-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2018 at 01:02 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.12-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `election`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(8) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `admin`:
--

--
-- Truncate table before insert `admin`
--

TRUNCATE TABLE `admin`;
--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `type`, `status`) VALUES
(1, 'janak v', 'vaghelajanak97145@gmail.com', '123123', '2.jpg', 0, 1),
(2, 'sudeep', 'sudeepyadav5@gmail.com', '123123', 'p2.jpg', 0, 1),
(3, 'dhaval', 'variyadhaval758@gmail.com', '123123', 'p3.jpg', 0, 1),
(4, 'smita', 'smitapatel9537@gmail.com', 'IB8225', 'IMG-20160229-WA00172.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `blogs`:
--   `cid`
--       `candidate` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `id` int(11) NOT NULL,
  `stud_id` int(11) NOT NULL,
  `proposer_id` int(11) NOT NULL,
  `supporter_id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `position` int(11) NOT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  `g_link` varchar(255) DEFAULT NULL,
  `t_link` varchar(255) DEFAULT NULL,
  `i_link` varchar(255) DEFAULT NULL,
  `about` text,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `candidate`:
--   `stud_id`
--       `student` -> `id`
--   `proposer_id`
--       `student` -> `id`
--   `supporter_id`
--       `student` -> `id`
--   `eid`
--       `election` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `r_id` int(11) NOT NULL,
  `msg` text NOT NULL,
  `read_status` int(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `chat`:
--   `s_id`
--       `student` -> `id`
--   `r_id`
--       `student` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `clg_info`
--

CREATE TABLE `clg_info` (
  `id` int(11) NOT NULL,
  `clg_name` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `clg_logo` varchar(80) NOT NULL,
  `clg_contact_no` varchar(43) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` double(10,6) DEFAULT NULL,
  `lng` double(10,6) DEFAULT NULL,
  `about_clg` text NOT NULL,
  `principal` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `clg_info`:
--

--
-- Truncate table before insert `clg_info`
--

TRUNCATE TABLE `clg_info`;
--
-- Dumping data for table `clg_info`
--

INSERT INTO `clg_info` (`id`, `clg_name`, `email`, `clg_logo`, `clg_contact_no`, `address`, `lat`, `lng`, `about_clg`, `principal`, `website`, `status`) VALUES
(1, 'scet', 'scet@gmail.com', 'clg_logo.jpeg', '9876543210,9988775566', 'athvaline', 21.181796, 72.808966, '<p>Single-minded devotion of the pioneers of the society, inspired a series of selfless workers and public spirited men to volunteer to dedicated themselves to the service of Sarvajanik Education Society, which is now the largest philanthropic society in the whole of country.</p>\r\n\r\n<p>Established in 1912, and registered under Society&rsquo;s Registration Act XXI OF 1860, Sarvajanik Education Society imparts high quality education in multiple disciplines to 33,000 students in 33 institutions from pre primary to post graduate level.</p>\r\n\r\n<p>SES, managed by eminent citizens of surat from different cross sections of the society, is purely a philanthropic society sustained only through public donations.</p>\r\n\r\n<p>SES runs in a fully democratic manner, functioning in accordance with the guidelines set up unanimously by the founders, the government, university and the All India Council of technical Education (AICTE).</p>\r\n\r\n<p>The organizational structure of SES comprises of the Managing Committee and the Executive Committee. The latter looks after the Finance, Building, Academic, legal, Purchase, Project &amp; Administration matters as per the policies laid down by the Managing Committee.</p>\r\n', 'prof. jayatri kapadiya', 'http://www.scet.ac.in', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `replay_id` int(11) DEFAULT '0',
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `comment`:
--   `bid`
--       `blogs` -> `id`
--   `uid`
--       `student` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `department`:
--

--
-- Truncate table before insert `department`
--

TRUNCATE TABLE `department`;
--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_name`, `status`) VALUES
(1, 'MCA', 1),
(2, 'MBA', 1),
(4, 'MSC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `no_of_div` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `division`:
--   `dept_no`
--       `department` -> `id`
--

--
-- Truncate table before insert `division`
--

TRUNCATE TABLE `division`;
--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `dept_no`, `year`, `no_of_div`, `status`) VALUES
(2, 1, 1, 3, 1),
(1, 1, 2, 2, 1),
(5, 2, 1, 2, 1),
(4, 4, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `election`
--

CREATE TABLE `election` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `dept_no` int(11) NOT NULL,
  `edate` datetime NOT NULL,
  `registration_end_date` datetime NOT NULL,
  `ads_end_date` datetime NOT NULL,
  `hours` int(11) NOT NULL DEFAULT '1',
  `level` varchar(20) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `election`:
--

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `tbl` varchar(30) NOT NULL,
  `tbl_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `likes`:
--   `uid`
--       `student` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `position_chart`
--

CREATE TABLE `position_chart` (
  `id` int(11) NOT NULL,
  `position_name` varchar(50) NOT NULL,
  `position_no` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `position_chart`:
--

--
-- Truncate table before insert `position_chart`
--

TRUNCATE TABLE `position_chart`;
--
-- Dumping data for table `position_chart`
--

INSERT INTO `position_chart` (`id`, `position_name`, `position_no`, `status`) VALUES
(1, 'student', 2, 1),
(2, 'Class LR', 5, 1),
(3, 'Class CR', 7, 1),
(4, 'College LR', 10, 1),
(5, 'College GS', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `slider`:
--

--
-- Truncate table before insert `slider`
--

TRUNCATE TABLE `slider`;
--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `image`, `status`) VALUES
(1, 'HEY! WE ARE POLITE', 'Election Day Is Coming\r\nUnited for Progress', 'def_31.jpg', 1),
(2, 'HEY! WE ARE POLITE', 'Your Vote for Progress\r\nWe Make History', 'def_21.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `rno` int(11) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(250) NOT NULL,
  `image` varchar(60) NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `student`:
--

--
-- Truncate table before insert `student`
--

TRUNCATE TABLE `student`;
--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dept_no`, `year`, `division`, `semester`, `rno`, `mobile_no`, `email`, `password`, `dob`, `gender`, `address`, `image`, `position`, `status`) VALUES
(4, 'smita', 1, 2, 1, 3, 1, 9876543210, 'smita@gmail.com', 'SS123', '1998-01-27', 'female', 'uyuyguh ihh ih iuh', 'g5.jpg', 4, 1),
(7, 'venish', 1, 2, 1, 3, 3, 7894561232, 'venishsiyani@gmail.com', 'EJ6090', '1997-03-07', 'male', 'sdas as321d as1d 32s ', 'polish-words-v.jpg', 1, 1),
(53, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 2, 9427157100, 'abk123@gmail.com', 'HM2594', '0000-00-00', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(54, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 1, 4, 5, 9712473353, 'hamita97@gmail.com', 'LU3728', '0000-00-00', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(55, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 1, 4, 8, 7226057145, 'ranjan3544@gmail.com', 'OD8693', '0000-00-00', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(56, 'DESAI RUHI MUKESHKUMAR', 1, 2, 1, 4, 10, 9624059581, 'rushi@gmail.com', 'UT8684', '0000-00-00', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(57, 'PAWAR ROHAN RAJENDRA', 1, 2, 1, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', 'WC7688', '0000-00-00', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(58, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 1, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'TK6378', '0000-00-00', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(59, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 1, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'PI1675', '0000-00-00', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(60, 'VAGHELA JANAK KANUBHAI', 1, 2, 1, 4, 20, 9662727082, 'vaghelajanak97145@gmail.com', '123123', '0000-00-00', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '2.jpg', 1, 1),
(61, 'LAD HARDIK JAGDISHBHAI', 1, 2, 1, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'KZ3195', '0000-00-00', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(62, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 1, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'NE5631', '0000-00-00', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(63, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'CE6726', '0000-00-00', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(64, 'BHATT RUCHA JAYESHBHAI', 1, 2, 1, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'ZE3956', '0000-00-00', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(65, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 1, 4, 32, 8128603405, 'httgabk123@gmail.com', 'DC7851', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(66, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 1, 4, 34, 7046118204, 'mirhystal45@gmail.com', 'CL2992', '0000-00-00', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(67, 'GUPTA MANISH KRISHNA', 1, 2, 1, 4, 37, 7096869810, 'abk12457453@gmail.com', 'VV6882', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(68, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 1, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'TY3651', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(69, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 1, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'SH8283', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(70, 'ATLANI TARUNA MUKESH', 1, 2, 1, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'SX9134', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(71, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 1, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'AJ6572', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(5, 'gaurav', 1, 2, 2, 3, 2, 9966332255, 'gaurav.vanani@gmail.com', 'GV123', '1997-12-24', 'male', '25,jjndjnj jnfjdnfjndj ,f j ', 'crop.jpg', 5, 1),
(8, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 2, 9427157100, 'abk123@gmail.com', '123123', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(9, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 2, 4, 5, 9712473353, 'hamita97@gmail.com', '123123', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(10, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 2, 4, 8, 7226057145, 'ranjan3544@gmail.com', '123123', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(11, 'DESAI RUHI MUKESHKUMAR', 1, 2, 2, 4, 10, 9624059581, 'rushi@gmail.com', '123123', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(12, 'PAWAR ROHAN RAJENDRA', 1, 2, 2, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', '123123', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(13, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 2, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', '123123', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(14, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 2, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', '123123', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(15, 'VAGHELA JANAK KANUBHAI', 1, 2, 2, 4, 20, 9662727082, 'rushvhzsvbvzi@gmail.com', '123123', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(16, 'LAD HARDIK JAGDISHBHAI', 1, 2, 2, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', '123123', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(17, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 2, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', '123123', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(18, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', '123123', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(19, 'BHATT RUCHA JAYESHBHAI', 1, 2, 2, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', '123123', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(92, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', 'BM7949', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(21, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 2, 4, 34, 7046118204, 'mirhystal45@gmail.com', '123123', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(93, 'GUPTA MANISH KRISHNA', 1, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', 'UF3846', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(94, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'CE3464', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(95, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'FV2625', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(96, 'ATLANI TARUNA MUKESH', 1, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'RW2730', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(97, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'BJ8861', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(98, 'JOSHI JAYKUMAR KETANBHAI', 1, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'VL4347', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(72, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 2, 9427157100, 'abk123@gmail.com', 'PI5128', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(73, 'BARAIYA HASMITA DANJIBHAI', 2, 1, 2, 2, 5, 9712473353, 'hamita97@gmail.com', 'CC5088', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(74, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 2, 1, 2, 2, 8, 7226057145, 'ranjan3544@gmail.com', 'OQ8146', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(75, 'DESAI RUHI MUKESHKUMAR', 2, 1, 2, 2, 10, 9624059581, 'rushi@gmail.com', 'VR9179', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(76, 'PAWAR ROHAN RAJENDRA', 2, 1, 2, 2, 13, 9824914133, 'ranjahsvn34@gmail.com', 'CN4783', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(77, 'PATEL DRASHTIBEN RASIKBHAI', 2, 1, 2, 2, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'BP9024', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(78, 'PRAJAPATI NEHA HARKISHAN', 2, 1, 2, 2, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'KY4966', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(79, 'VAGHELA JANAK KANUBHAI', 2, 1, 2, 2, 20, 9662727082, 'rushvhzsvbvzi@gmail.com', 'KW4951', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(80, 'LAD HARDIK JAGDISHBHAI', 2, 1, 2, 2, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'XV1485', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(81, 'PATEL AYUSH VIJAYKUMAR', 2, 1, 2, 2, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'FV2613', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(82, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'JU2168', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(83, 'BHATT RUCHA JAYESHBHAI', 2, 1, 2, 2, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'WZ9302', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(84, 'PARAJAPATI DARSHANA BHARATKUMAR', 2, 1, 2, 2, 32, 8128603405, 'httgabk123@gmail.com', 'YY3381', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(85, 'PIPALIYA MIRAL KISHORBHAI', 2, 1, 2, 2, 34, 7046118204, 'mirhystal45@gmail.com', 'ZF5616', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(86, 'GUPTA MANISH KRISHNA', 2, 1, 2, 2, 37, 7096869810, 'abk12457453@gmail.com', 'HK3500', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(87, 'PATEL SUNNYKUMAR ARUNBHAI', 2, 1, 2, 2, 39, 7405859953, 'ruchggha441221@gmail.com', 'QD4079', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(88, 'AMIPARA NIRALIBEN RAMESHBHAI', 2, 1, 2, 2, 42, 8238165078, 'httgabk1234544@gmail.com', 'PF2719', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(89, 'ATLANI TARUNA MUKESH', 2, 1, 2, 2, 44, 7567699564, 'mirhystal42145@gmail.com', 'FF2277', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(90, 'GOLWALA MONIKA PRAKASHCHANDRA', 2, 1, 2, 2, 47, 9724373235, 'kanjothgghxi@gmail.com', 'EX6165', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(91, 'JOSHI JAYKUMAR KETANBHAI', 2, 1, 2, 2, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'MS9670', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(6, 'nirti', 4, 2, 2, 4, 1, 9856321478, 'nirtipatel@gmail.com', 'NP123', '1998-12-28', 'female', 'asdsadd sd,sdaa f,rrrt,yr', 'the_letter_n_by_nvn5719-d46l9ld.jpg', 3, 1),
(20, 'PARAJAPATI DARSHANA BHARATKUMAR', 4, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', '123123', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(22, 'GUPTA MANISH KRISHNA', 4, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', '123123', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(48, 'PATEL SUNNYKUMAR ARUNBHAI', 4, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'LA2110', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(49, 'AMIPARA NIRALIBEN RAMESHBHAI', 4, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'WK5961', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(50, 'ATLANI TARUNA MUKESH', 4, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'JD6080', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(51, 'GOLWALA MONIKA PRAKASHCHANDRA', 4, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'JW2041', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(52, 'JOSHI JAYKUMAR KETANBHAI', 4, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'YR7914', '0000-00-00', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `vote`:
--   `eid`
--       `election` -> `id`
--   `cid`
--       `candidate` -> `id`
--   `sid`
--       `student` -> `id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stud_id` (`stud_id`),
  ADD KEY `proposer_id` (`proposer_id`),
  ADD KEY `supporter_id` (`supporter_id`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `s_id` (`s_id`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `clg_info`
--
ALTER TABLE `clg_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bid` (`bid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`dept_no`,`year`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `election`
--
ALTER TABLE `election`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_no` (`dept_no`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`tbl`,`tbl_id`,`uid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `unique_id` (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `position_chart`
--
ALTER TABLE `position_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`dept_no`,`year`,`division`,`semester`,`rno`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`,`cid`,`sid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `sid` (`sid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `clg_info`
--
ALTER TABLE `clg_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `election`
--
ALTER TABLE `election`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `position_chart`
--
ALTER TABLE `position_chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `candidate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `candidate`
--
ALTER TABLE `candidate`
  ADD CONSTRAINT `candidate_ibfk_1` FOREIGN KEY (`stud_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_2` FOREIGN KEY (`proposer_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_3` FOREIGN KEY (`supporter_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_4` FOREIGN KEY (`eid`) REFERENCES `election` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`s_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`r_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `division`
--
ALTER TABLE `division`
  ADD CONSTRAINT `division_ibfk_1` FOREIGN KEY (`dept_no`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `election` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vote_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `candidate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vote_ibfk_3` FOREIGN KEY (`sid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
