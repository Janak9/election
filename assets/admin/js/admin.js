var view_pic=document.getElementsByClassName('view_pic');
for(var j=0;j<view_pic.length;j++){
 view_pic[j].setAttribute("alt","Image Dose Not Exists");
}

function del() {
  //alert("hello");
  if (confirm("are you sure?")) {
    return true;
  } else {
    return false;
  }
}

function chkall() {
  //alert("hello");
  var chk_all = document.getElementById('checkall').checked;
  //alert(chk_all);
  var chk = document.getElementsByName('chk[]');
  var chk_len = chk.length;
  console.log(chk);
  for (var i = 0; i < chk_len; i++) {
    if (chk_all == true) {
      chk[i].checked = true;
    } else {
      chk[i].checked = false;
    }
  }
}

function select_chk() {
  //alert("hi");
  var chk = document.getElementsByName('chk[]');
  var chk_len = chk.length;
  var c = 0;
  for (var i = 0; i < chk_len; i++) {
    if (chk[i].checked == true) {
      c++;
    }
  }
  if (chk_len == c) {
    document.getElementById('checkall').checked = true;
  } else {
    document.getElementById('checkall').checked = false;
  }
}
/* end of script of multiple delete*/

function status_toggle(id, tbl, tbl_id) {
  var s = document.getElementById(id).checked;
  var val;
  if (s == true) {
    val = 0;
  } else {
    val = 1;
  }
  $("#" + id).attr("checked", !$("#" + id).attr("checked"));
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/admin/Dashboard/fn_status_changer/",
    data: {
      tbl: tbl,
      tbl_id: tbl_id,
      val: val
    },
    success: function (response) {

    }
  });
}

function get_image(cnt) {
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/admin/Dashboard/fn_add_more_img",
    data: {
      cnt: cnt
    },
    success: function (html) {
      $("#img" + cnt).html(html);
      cnt = cnt + 1;
      $("#more_img")[0].setAttribute("onclick", "get_image(" + cnt + ")");
    }
  });
}

function remove_img(id) {
  $(id).remove();
}
function applied_candidate_count(){
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/admin/Candidate/count_applied_candidate",
    success: function (html) {
      $("#applied_candidate_count").html(html);
    }
  });
}
function loadfile(event, obj) {
  var output = $(obj).parent('div').find('img');
  //  var output = document.getElementById("pic");
  output[0].src = URL.createObjectURL(event.target.files[0]);
}
//------------loadfile1 for multifile upload
function loadfile1(event, pic, span) {
  var output = document.getElementById(pic);
  output.src = URL.createObjectURL(event.target.files[0]);
  var sp = document.getElementById(span);
  sp.innerHTML = event.target.value.split('\\').pop();
}

function go_back(page) {
  window.history.back(page);
}

function calc_age(e, id) {
  var today = new Date();
  var birthDate = new Date(e.value);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  document.getElementById(id).value = age;
}

function get_division(dept,year,division){
  dept_no = document.getElementById(dept).value;
  year_no = document.getElementById(year).value;
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/admin/Dashboard/fn_get_division",
    data: {
      dept_no : dept_no,
      year : year_no
    },
    success: function (html) {
      $("#" + division).html(html);
    }
  });

  /*--------for semester-------*/
  if(year_no>0){
    $("#semester").html("<option value='-1'>Select Semester</option><option value='"+ (year_no*2-1) +"'>"+ (year_no*2-1) +"</option><option value='"+ (year_no*2) +"'>"+ (year_no*2) +"</option>");
  }
  else{
    $("#semester").html("<option value='-1'>Select Semester</option>");
  }
}

/*-------------add location map-------*/
var map;
var marker;
var markers = [];
var infowindow;

function initAutocomplete() {
  initMap();

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function () {
    searchBox.setBounds(map.getBounds());
  });

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    delete_marker();
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function (place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

function initMap() {
  var india = {lat: 22.14739, lng: 78.59489};
  map = new google.maps.Map(document.getElementById('map'), {
    center: india,
    zoom: 5
  });
  set_co_ords();
  infowindow = new google.maps.InfoWindow({
    content: document.getElementById('map_form')
  });

  google.maps.event.addListener(map, 'click', function(event) {
    //marker.setMap(null);
    marker = new google.maps.Marker({
      position: event.latLng,
      map: map
    });
    markers.push(marker);
  });
}

function delete_marker() {
  markers.forEach(function (marker) {
    marker.setMap(null);
  });
  //marker.setMap(null);
}

function saveData() {
  var latlng = marker.getPosition();
  document.getElementById('lat').value = latlng.lat();
  document.getElementById('lng').value = latlng.lng();
  alert("College location Saved Successfully.");
}

function set_co_ords() {
  var lat = document.getElementById('lat').value;
  var lng = document.getElementById('lng').value;
  //  console.log(lat);
  //  console.log(lng);
  if (lat != "" && lng != "") {
    var myLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map
    });
    map.setZoom(15);
    map.panTo(myLatlng);

  }
}