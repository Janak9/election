function loadfile(event, id) {
  var output = document.getElementById(id);
  output.src = URL.createObjectURL(event.target.files[0]);
}

function start_timer(id,hour){
  setInterval(function(){
    var deadline=new Date($('#'+id).attr('value')).getTime();
		var now=new Date().getTime();
		var dis=deadline-now;
		var d=Math.floor(dis/(1000*60*60*24));
		var h=Math.floor((dis%(1000*60*60*24))/(1000*60*60));
		var m=Math.floor((dis%(1000*60*60))/(1000*60));
    var s=Math.floor((dis%(1000*60))/(1000));
    if(dis<0){
      dis=deadline+(hour*60*60*1000)-now;
      h=Math.floor((dis%(1000*60*60*24))/(1000*60*60));
      m=Math.floor((dis%(1000*60*60))/(1000*60));
      s=Math.floor((dis%(1000*60))/(1000));  
      if(dis<0){
        $('#'+id).text("Election Is Over..!");
        $('#'+id).closest('.election_div').addClass("election_close");
        $('#'+id).closest('.two-hover').find('a').css('display','block');
        $('#'+id).closest('.two-hover').find('a').attr('href',"http://localhost/election/index.php/user/Election/fn_result/"+id.match(/\d+$/)[0]);
      }
      else{
        $('#'+id).html("Election Is started <br>"+h+"h "+m+"m "+s+"s ");
        $('#'+id).closest('.election_div').find('a').attr('href',"http://localhost/election/index.php/user/Election/fn_vote_list/"+id.match(/\d+$/)[0]);
      }
    }else{
      $('#'+id).text(d+"d "+h+"h "+m+"m "+s+"s ");
    }
	},1000);

}

function manage_like(ele,tbl,id){
  var cls_active = $(ele).hasClass('active');
  var task;
  if(cls_active === undefined || cls_active === false){
    task = 0;  
  }
  else{
    task = 1;
  }
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/user/Candidate/fn_manage_like/"+task,
    data: {
      tbl:tbl,
      id:id
    },
    success: function (response) {
      $(ele).toggleClass('active');
      var t = $(ele).find('.total_like').html();
      if($(ele).hasClass('active')){
        $(ele).find('.total_like').html(parseInt(t)+1);
      }else{
        $(ele).find('.total_like').html(parseInt(t)-1);
      }
    }
  });
}

function get_student_card(id,rno,name,detail_id){
  var rno = document.getElementById(rno).value;
  var name = document.getElementById(name).value;
  var pid = document.getElementById("proposer_id");
  if(pid==null)
    pid="";
  else
    pid=pid.value;
    
  var sid = document.getElementById("supporter_id");
  if(sid==null)
    sid="";
  else
    sid=sid.value;
  
  if(rno != "" || name!=""){
    $.ajax({
      type: "post",
      url: "http://localhost/election/index.php/user/Dashboard/fn_get_student_card",
      data: {
        rno:rno,
        name:name,
        detail_id:detail_id,
        pid:pid,
        sid:sid
      },
      success: function (response) {
        $("#" +id).html(response);
      }
    });
  }
}

function set_student_card_details(detail_id,stud_id){
  $.ajax({
    type: "post",
    url: "http://localhost/election/index.php/user/Dashboard/fn_get_student_detail",
    data: {
      detail_id:detail_id,
      stud_id:stud_id
    },
    success: function (response) {
      $("#"+detail_id).html(response);
    }
  });
}
/*-------------add location map-------*/
var map;
var marker;
var markers = [];
var infowindow;

function initAutocomplete() {
  initMap();

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function () {
    searchBox.setBounds(map.getBounds());
  });

  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();
    if (places.length == 0) {
      return;
    }
    delete_marker();
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function (place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

function initMap() {
  var india = {
    lat: 22.14739,
    lng: 78.59489
  };
  map = new google.maps.Map(document.getElementById('map'), {
    center: india,
    zoom: 5
  });
  set_co_ords();
}

function delete_marker() {
  markers.forEach(function (marker) {
    marker.setMap(null);
  });
  //marker.setMap(null);
}

function saveData() {
  var latlng = marker.getPosition();
  document.getElementById('lat').value = latlng.lat();
  document.getElementById('lng').value = latlng.lng();
  alert("College location Saved Successfully.");
}

function set_co_ords() {
  var lat = document.getElementById('lat').value;
  var lng = document.getElementById('lng').value;
  //  console.log(lat);
  //  console.log(lng);
  if (lat != "" && lng != "") {
    var myLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map
    });
    map.setZoom(15);
    map.panTo(myLatlng);
  }
}

// chat 
function send_msg(s_id,r_id,str){
  if(str==""){
    return;
  }
  var data="&s_id="+s_id+"&r_id="+r_id+"&msg="+str;
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("POST","http://localhost/election/index.php/user/Chat/fn_send_msg?"+data,true);
  xmlhttp.send();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $('#mess').val('');
      chat_box();
    }
  }   
}
function chat_box(){
  var s_id=$('#s_id').val();
  var r_id=$('#r_id').val();
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("POST","http://localhost/election/index.php/user/Chat/fn_chat_box?s_id="+s_id+"&r_id="+r_id,true);
  xmlhttp.send();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $('#messages').html(xmlhttp.responseText);
      var elem = document.getElementById('messages');
      elem.scrollTop = elem.scrollHeight;
    }
  }   
}

function chat_list(){
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("POST","http://localhost/election/index.php/user/Chat/fn_chat_list_data",true);
  xmlhttp.send();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $('#main_chat_list').html(xmlhttp.responseText);
    }
  }   
}

function chat_count(){
  xmlhttp=new XMLHttpRequest();
  xmlhttp.open("POST","http://localhost/election/index.php/user/Chat/fn_chat_count",true);
  xmlhttp.send();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $('#chat_count').html(xmlhttp.responseText);
    }
  }   
}

function confirmation(){
  if (confirm("are you sure?")) {
    return true;
  } else {
    return false;
  }
}