<?php
class Main_model extends CI_Model
{
	function insert_rec($arr = array(), $tbl = ""){
		if(count($arr) == count($arr, COUNT_RECURSIVE)) // check for multiple row insert
			$this->db->insert($tbl, $arr);
		else
			$this->db->insert_batch($tbl, $arr);
		//echo $this->db->last_query();
	}

	function update_rec($arr = array(), $id = "", $tbl = "", $conditions = array(),$qry=""){
		if ($id > 0) {
			$this->db->where("id", $id);
		}
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		if($qry!=""){
			$this->db->query($qry);
		}else{
			$this->db->update($tbl, $arr);
		}
		// echo $this->db->last_query();	
	}
	
	function delete_rec($id = "", $tbl = "", $conditions = array(),$qry=""){
		if ($id > 0) {
			$this->db->where("id", $id);
		} 
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		if($qry!=""){
			$this->db->query($qry);
		}else{
			$this->db->delete($tbl);
		}
		//echo $this->db->last_query();
	}
	
	function select_record($id = "", $tbl = ""){
		$this->db->where("id", $id);
		$qry = $this->db->get($tbl);
		$arr = $qry->row_array();
		return $arr;
	}
	
	function last_record($tbl = ""){
		$this->db->order_by("id", 'DESC');
		$qry = $this->db->get($tbl, 1);
		$arr = $qry->row_array();
			//echo $this->db->last_query();
		return $arr;
	}
	
	function row_count($tbl = "", $field = "", $id = "", $conditions = array(), $qry_str = ""){
		if ($id > 0 || $id == -1) {
			$this->db->where($field, $id);
		} 
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		if ($qry_str != "") {
			$qry = $this->db->query($qry_str);
		} else {
			$qry = $this->db->get($tbl);
		}
		// echo $this->db->last_query();
		$num = $qry->num_rows();
		return $num;
	}
	
	function view_rec($per_page = 3, $start = 0, $tbl = "", $field = "", $id = "", $conditions = array(), $qry_str = ""){
		if ($id != "") { 
			$this->db->where($field, $id);
		} 
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		$this->db->limit($per_page, $start);
		
		if ($qry_str != "") {
			$qry = $this->db->query($qry_str);
		} else {
			$qry = $this->db->get($tbl);
		}
		//echo $this->db->last_query();
		$arr = $qry->result_array();
		if ($qry->num_rows() == 0) {
			$arr['blank'] = "Record Does Not Found";
		}
		return $arr;
	}
	
	function get_rec($tbl = "", $field = "", $id = "", $conditions = array(), $orderby = array()){
		if ($id != "" ) {
			$this->db->where($field, $id);
		} 
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		if (!empty($orderby)) {
			foreach($orderby as $k => $v){
				$this->db->order_by($k, $v);
			}
		}
		$qry = $this->db->get($tbl);
		// echo $this->db->last_query();
		$arr = $qry->result_array();
		if ($qry->num_rows() == 0) {
			$arr['blank'] = "Record Does Not Found";
		}
		return $arr;
	}
	
	function like_rec($tbl = "", $field = "", $txt = "",$mode="after",$like_arr=array(),$conditions=array()){
		if (!empty($conditions)) {
			$this->db->where($conditions);
		}
		if (!empty($like_arr)) {
			foreach($like_arr as $key => $value) {
				$this->db->like($key,$value,$mode);
			}
		}
		else{
			$this->db->like($field, $txt,$mode);//$mode can be a after,before,both
		}
		$qry = $this->db->get($tbl);
		// echo $this->db->last_query();
		$arr = $qry->result_array();
		if ($qry->num_rows() == 0) {
			$arr['blank'] = "Record Does Not Found";
		}
		return $arr;
	}
	
	public function my_query($qry = ''){
		$res = $this->db->query($qry);
			//echo $this->db->last_query();
		$arr = $res->result_array();
		if ($num = $res->num_rows() == 0) {
			$arr['blank'] = "Record Does Not Found";
		}
		return $arr;
	}
	
	public function multitask($select = '', $from = "", $conditions = array(), $orderby = "", $groupby = array(), $limit = ""){
		$this->db->distinct();
		if ($select != "")
			$this->db->select($select);
		$this->db->from($from);
		$this->db->where($conditions);
		if ($orderby != "")
			$this->db->order_by($orderby);
		if (!empty($groupby))
			$this->db->group_by($groupby);
		if ($limit != "")
			$this->db->limit($limit);
		$qry = $this->db->get();
		$arr = $qry->result_array();
		//echo $this->db->last_query();
		if ($qry->num_rows() == 0) {
			$arr['blank'] = "Record Does Not Found";
		}
		return $arr;
	}
	
	public function page_rec($per_page = "3", $start = "0", $data = array()){
		$arr = array();
		$j = 0;
		$i = 0;
		foreach ($data as $key => $value) {
			if ($i < $start) {
				$i++;
			} elseif ($j < $per_page) {
				$arr[$key] = $value;
				$j++;
			} else {
				break;
			}
		}
		return $arr;
	}
}
?>