<?php
class Clg_info extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}
	public function fn_edit_clg_info()
	{
		$data = array();
		$data['menu'] = 'clg_info';
        if ($this->input->post('btn_submit')) {
            $this->form_validation->set_rules('name', 'College Name', 'required', array('%s is required'));
            $this->form_validation->set_rules('email', 'College Email', 'required', array('%s is required'));
            $this->form_validation->set_rules('contact1', 'Contact Number 1', 'required|regex_match[/^[0-9]{10}$/]', array('required' => '%s field is required',
            'regex_match' => 'The %s must contain 10 digit.'));
            $this->form_validation->set_rules('contact2', 'Contact Number 2', 'regex_match[/^[0-9]{10}$/]', array('regex_match' => 'The %s must contain 10 digit.'));
            $this->form_validation->set_rules('contact3', 'Contact Number 3', 'regex_match[/^[0-9]{10}$/]', array('regex_match' => 'The %s must contain 10 digit.'));
            $this->form_validation->set_rules('contact4', 'Contact Number 4', 'regex_match[/^[0-9]{10}$/]', array('regex_match' => 'The %s must contain 10 digit.'));
            $this->form_validation->set_rules('addr', 'Address', 'required', array('%s is required'));
            $this->form_validation->set_rules('lat', 'Location', 'required', array('required'=>'Please Select College %s On Map'));
			$this->form_validation->set_rules('about', 'About College', 'required', array('%s is required'));
			$this->form_validation->set_rules('website', 'Website', 'required|regex_match[/^http(s?)\:\/\/([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?$/]', array('required' => '%s field is required.',
            'regex_match' => 'Enter valid Website Link.'));
			if ($this->form_validation->run() == true) {
				$contacts=array($this->input->post('contact1'),$this->input->post('contact2'),$this->input->post('contact3'),$this->input->post('contact4'));
				
				$name=$this->input->post('name');
                $email=$this->input->post('email');
                $contact=implode(",",array_filter($contacts));
                $addr=$this->input->post('addr');
                $lat=$this->input->post('lat');
                $lng=$this->input->post('lng');
				$about=$this->input->post('about');
				$principal=$this->input->post('principal');
				$website=$this->input->post('website');
                $status=$this->input->post('status');
           
				$arr=array('clg_name'=>$name,
                           'email'=>$email,
                           'clg_contact_no'=>$contact,
                           'address'=>$addr,
                           'lat'=>$lat,
                           'lng'=>$lng,
						   'about_clg'=>$about,
						   'principal'=>$principal,
						   'website'=>$website,
                           'status'=>$status
                           );
                $config['upload_path']="assets/photos/";
                $config['allowed_types']="gif|jpg|png|jpeg";
                $this->load->library("upload",$config);
                if($this->upload->do_upload('img_file')){
                    $file=array("up_img"=>$this->upload->data());
					$arr['clg_logo']=$file['up_img']['file_name'];
					
					$data['data'] = $this->Main_model->select_record(1,"clg_info");
					if($data['data']['clg_logo']!="def_clg_logo"){
						if($data['data']['clg_logo'] !="" && is_file("assets/photos/".$data['data']['clg_logo'])){
							unlink("assets/photos/".$data['data']['clg_logo']);
						}
					}
				}
				else{
					$data['error']=$this->upload->display_errors();
				}
				$this->Main_model->update_rec($arr,1,"clg_info");
				$this->form_validation->clear_rules();
				unset($data['error']);
                echo "<script>alert('College data successfull saved.');</script>";
			}//if enter data is proper validation
		}
		$data['data'] = $this->Main_model->select_record(1,"clg_info");
		$this->load->view('admin/edit_clg_info', $data);
    }
    
    public function fn_add_slider_img($id = '')
	{
		$data = array();
		$data['menu'] = 'clg_info';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "slider");
		}
		if ($this->input->post('btn_submit')) {
            $title=$this->input->post('title');
            $des=$this->input->post('description');
			$status=$this->input->post('status');
			$arr=array('title'=>$title,
						'description'=>$des,
						'status'=>$status
						);
			$config['upload_path']="assets/photos/slider/";
			$config['allowed_types']="gif|jpg|png|jpeg";
			$this->load->library("upload",$config);
			if($this->upload->do_upload('img_file')){
				if(!empty($id)){
					$rec=$this->Main_model->select_record($id,"slider");
					if(!in_array(@$rec['image'],array("def_1.jpg","def_2.jpg","def_3.jpg","def_4.jpg"))){
						if(@$rec['image'] !="" && is_file("assets/photos/slider/".@$rec['image'])){
							unlink("assets/photos/slider/".@$rec['image']);
						}
					}
				}
				$file=array("up_img"=>$this->upload->data());
				$arr['image']=$file['up_img']['file_name'];
				if(!empty($id))
					$this->Main_model->update_rec($arr,$id,"slider");
				else
					$this->Main_model->insert_rec($arr,"slider");
				redirect("admin/Clg_info/fn_view_slider_img");
			}else if (!empty($id)) {
				$this->Main_model->update_rec($arr, $id, "slider");
			} else {
				$data['error']=$this->upload->display_errors();
			}
		}
		$this->load->view('admin/add_slider_img', $data);
	}

	public function fn_view_slider_img()
	{
		$data = array();
		$data['menu'] = 'clg_info';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$rec=$this->Main_model->select_record($checked[$i],"slider");
					if(!in_array(@$rec['image'],array("def_1.jpg","def_2.jpg","def_3.jpg","def_4.jpg"))){
						if(@$rec['image'] !="" && is_file("assets/photos/slider/".@$rec['image'])){
							unlink("assets/photos/slider/".@$rec['image']);
						}
					}
                    $this->Main_model->delete_rec($checked[$i], "slider");
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("slider");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Clg_info/fn_view_slider_img'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "slider");
		$this->load->view('admin/view_slider_img', $data);

	}
	public function fn_delete_slider_img($id = '')
	{
		$rec = $this->Main_model->select_record($id, "slider");
		if(!in_array(@$rec['image'],array("def_1.jpg","def_2.jpg","def_3.jpg","def_4.jpg"))){
			if(@$rec['image'] !="" && is_file("assets/photos/candidate_docs/".@$rec['image'])){
				unlink("assets/photos/slider/".@$rec['image']);
			}
		}
		$this->Main_model->delete_rec($id, "slider");
		redirect("admin/Clg_info/fn_view_slider_img");
	}

}
?>
