<?php
class Candidate extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
    }
    
	public function fn_applied_candidate(){
		$data = array();
		$data['menu'] = 'candidate';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->fn_delete_candidate($checked[$i],false);
				}
			}
		}
		$per_page = 15;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("candidate");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Candidate/fn_applied_candidate'));
		$this->db->order_by('id', 'DESC');
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "candidate");
		$this->load->view('admin/applied_candidate', $data);
	}
	
	public function count_applied_candidate(){
		echo $this->Main_model->row_count("candidate","","",array("status"=>0));
	}

    public function fn_notify($sid,$msg){
        $msg=urldecode($msg);
        $rec = $this->Main_model->select_record($sid, "student");
        if(mail_send($rec['email'],"Status Change",$msg)){
            echo "send mail successfully";
        }else{
            echo "Email Does Not Send Please Try Again...!";
        }
    }

	public function fn_delete_candidate($id = '',$redirect=true)
	{
        $rec = $this->Main_model->select_record($id, "candidate");
        $stud = $this->Main_model->select_record($rec['stud_id'], "student");
        if(mail_send($stud['email'],"Removed From Candidate","You are removed from candidate")){}
		if($rec['image'] !="" && is_file("assets/photos/candidate_docs/".$rec['image'])){
			unlink("assets/photos/candidate_docs/".$rec['image']);
		}
		$blogs_of_cand = "select id from blogs where cid=".$id;
		$blogs = $this->Main_model->my_query($blogs_of_cand);
		if(!@$blogs['blank']){
			$blogs_list=array_map(function($index){
				$blog = $this->Main_model->select_record($index['id'], "blogs");
				if($blog['image'] !="" && is_file("assets/photos/blogs/".$blog['image'])){
					unlink("assets/photos/blogs/".$blog['image']);
				}
				return $index['id'];
			},$blogs);
			$this->Main_model->delete_rec("","",array(),"delete from likes where (tbl='blogs' and tbl_id IN(".implode(",",$blogs_list).")) or (tbl='candidate' and tbl_id IN(".implode(",",$cand_list)."))");
		}
		$pos = $this->Main_model->like_rec("position_chart","position_name","tudent","both");
		$arr = array('position'=>$pos[0]['id']);
		$this->Main_model->update_rec($arr,$stud['id'],"student");
		$this->Main_model->delete_rec($id, "candidate");
		if($redirect)
			redirect("admin/Candidate/fn_applied_candidate");
	}
}
?>
