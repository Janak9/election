<?php
class Student extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		$this->load->library('Spreadsheet_Excel_Reader');
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}

	public function fn_add_student($id = ''){
		$data = array();
		$data['menu'] = 'student';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "student");
		}
		if($this->input->post('btn_submit') && !empty($_FILES['excel_file']['name'])) {
			$config['upload_path'] = "assets/excel_files/students/";
			$config['allowed_types'] = "xls|xlsx|xlm|xlsm";
			$this->load->library("upload", $config);
			if ($this->upload->do_upload('excel_file')) {
				$file = array("up_excel" => $this->upload->data());
				$file_name = $file['up_excel']['file_name'];
				$excel = new Spreadsheet_Excel_Reader();	
				$excel->read('assets/excel_files/students/'.$file_name);
				$data['data_excel']=$excel->sheets[0]['cells'];
				// pre($data['data_excel']);
				$arr=array();
				$up_arr=array();
				$insert_cnt=0;
				$dept = $this->input->post('dept');
				$year = $this->input->post('year');
				$div = $this->input->post('division');
				$position = $this->input->post('position');
				$sem = $this->input->post('semester');
				$data['error']="";
				for($i=2;$i<=count($data['data_excel']);$i++){
					if(preg_match("/^[A-Za-z ]+$/",$data['data_excel'][$i][2])) {
						if(preg_match("/^[0-9]+$/",$data['data_excel'][$i][1])) {
							if (preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/", $data['data_excel'][$i][4])) {
								if (preg_match("/^[0-9]{10,10}$/", $data['data_excel'][$i][3])) {
									$tempDate = explode('/', $data['data_excel'][$i][5]);
									// echo $data['data_excel'][$i][5]."<br>";
									// echo $tempDate[1]."-";
									// echo $tempDate[0]."-";
									// echo $tempDate[2]."<br>";
									
									if (is_numeric($tempDate[0]) && is_numeric($tempDate[1]) && is_numeric($tempDate[2]) && checkdate($tempDate[1], $tempDate[0], $tempDate[2])) {
										if (preg_match("/^[mfMF]{1}$/", $data['data_excel'][$i][6])) {
											$count_con=array(
												'dept_no'=>$dept,
												'year'=>$year,
												'division'=>$div,
												'semester'=>$sem,
												'rno'=>$data['data_excel'][$i][1]
											);
											$rec = $this->Main_model->get_rec('student',"","",$count_con);
											if(!@$rec['blank']){
												$index=$rec[0]['id'];
												$up_arr[$index]=array(
													'name' => $data['data_excel'][$i][2],
													'dept_no'=> $dept,
													'year'=> $year,
													'division'=> $div,
													'semester'=> $sem,
													'rno'=> $data['data_excel'][$i][1],
													'mobile_no' => $data['data_excel'][$i][3],
													'dob'=> $tempDate[2]."-".$tempDate[1]."-".$tempDate[0],
													'gender'=> ($data['data_excel'][$i][6] == 'M') ? "male" : "female",
													'address' => $data['data_excel'][$i][7],
													'position' => $position
												);
											}else{
												$password = chr(rand(65,90)).chr(rand(65,90)).rand(1111,9999);
												if(mail_send($data['data_excel'][$i][4],'Password For Student Login','Your Login Password is : - '.$password)){
													$arr[$insert_cnt]=array(
														'name' => $data['data_excel'][$i][2],
														'dept_no'=> $dept,
														'year'=> $year,
														'division'=> $div,
														'semester'=> $sem,
														'rno'=> $data['data_excel'][$i][1],
														'mobile_no' => $data['data_excel'][$i][3],
														'email' => $data['data_excel'][$i][4],
														'password'=>$password,
														'dob'=> $tempDate[2]."-".$tempDate[1]."-".$tempDate[0],
														'gender'=> ($data['data_excel'][$i][6] == 'M') ? "male" : "female",
														'address' => $data['data_excel'][$i][7],
														'position' => $position
													);
													$insert_cnt++;
												}else
													$data['error']="Email Does Not Send Please Try Again...!<br>";
											}
										}else
											$data['error'].="Gender is not valid. It should contain only one charchter('M' or 'F') in row number $i<br>";
									}else
										$data['error'].="Date is not valid. Please use dd/mm/yyyy format in row number $i<br>";
								}else
									$data['error'].="Mobile Number is not valid in row number $i<br>";
							}else
								$data['error'].="Email is not valid in row number $i<br>";
						}else
							$data['error'].="Roll number should contain only number in row number $i<br>";
					}else
						$data['error'].="Student name should contain only character in row number $i<br>";
				}
				// pre($arr);
				// pre($up_arr);
				// die;
				if($insert_cnt>0)
					$this->Main_model->insert_rec($arr,"student");
				foreach($up_arr as $k =>$v){
					$this->Main_model->update_rec($v,$k,"student");
				}
				if(empty($data['error']))
					echo "<script>alert('Successfull Inserted All Recored')</script>";
				else
					$data['error'].="<br><br>*Except Above All Other Record Are Inserted Successfully.<br>";
			} else {
				$data['error'] = $this->upload->display_errors();
			}
		}
		if ($this->input->post('btn_submit') && !empty($_FILES['img_file']['name'])) {
			$this->form_validation->set_rules('sname', 'Name', 'required|regex_match[/^[A-Za-z ]+$/]', array(
				'required' => '%s field is required',
				'regex_match' => 'The %s must contain only letter.'
			));
			$this->form_validation->set_rules('phno', 'Mobile Number', 'required|regex_match[/^[0-9]{10}$/]', array(
				'required' => '%s field is required',
				'regex_match' => 'The %s must contain 10 digit.'
			));

			if ($this->form_validation->run() == true) {
				$name = $this->input->post('sname');
				$dept = $this->input->post('dept');
				$year = $this->input->post('year');
				$div = $this->input->post('division');
				$sem = $this->input->post('semester');
				$rno = $this->input->post('rno');
				$phno = $this->input->post('phno');
				$email = $this->input->post('email');
				$dob = $this->input->post('dob');
				$gender = $this->input->post('gender');
				$addr = $this->input->post('addr');
				$position = $this->input->post('position');
				$status = $this->input->post('status');
				if(empty($id))
					$password = chr(rand(65,90)).chr(rand(65,90)).rand(1111,9999);

				$arr = array(
					'name' => $name,
					'dept_no'=> $dept,
					'year'=> $year,
					'division'=> $div,
					'semester'=> $sem,
					'rno'=> $rno,
					'mobile_no' => $phno,
					'email' => $email,
					'dob'=> $dob,
					'gender'=> $gender,
					'address' => $addr,
					'position' => $position,
					'status' => $status
				);
				if(empty($id))
					$arr['password']=$password;
					
				$config['upload_path'] = "assets/photos/students/";
				$config['allowed_types'] = "gif|jpg|png|jpeg";
				$this->load->library("upload", $config);
				if ($this->upload->do_upload('img_file')) {
					$file = array("up_img" => $this->upload->data());
					$arr['image'] = $file['up_img']['file_name'];
					if (!empty($id) && $data['data']['image'] != "avatar.png") {
						if (unlink("assets/photos/students/" . $data['data']['image'])) {}
					}

					if(empty($id)){
						if(mail_send($email,'Password For Student Login','Your Login Password is : - '.$password)){
							$this->Main_model->insert_rec($arr,"student");
							redirect("admin/Student/fn_view_student");
						}else{
							$data['error']="Email Does Not Send Please Try Again...!<br>";
						}
					}
					else{
						$this->Main_model->update_rec($arr, $id, "student");
						redirect("admin/Student/fn_view_student");	
					}
				}else if (!empty($id)) {
					$this->Main_model->update_rec($arr, $id, "student");
					redirect("admin/Student/fn_view_student");
				} else {
					$data['error'] = $this->upload->display_errors();
				}
				redirect("admin/Student/fn_view_student");
			}//if enter data is proper validation
		}
		$data['dept'] = $this->Main_model->get_rec("department","status",1);
		$data['division'] = $this->Main_model->get_rec("division","status",1);
		$data['position'] = $this->Main_model->get_rec("position_chart","status",1);
		$this->load->view('admin/add_student', $data);
	}

	public function fn_view_student(){
		$data = array();
		$data['menu'] = 'student';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->fn_delete_student($checked[$i],false);
				}
			}
		}
		$per_page = 15;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("student");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Student/fn_view_student'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "student");
		$this->load->view('admin/view_student', $data);

	}
	
	public function fn_delete_student($id = '',$redirect=true){
		$rec = $this->Main_model->select_record($id, "student");
		if(@$rec['image'] !="" && is_file("assets/photos/students/".@$rec['image'])){
			unlink("assets/photos/students/".$rec['image']);
		}
		$this->Main_model->delete_rec($id, "student");
		if($redirect)
			redirect("admin/Student/fn_view_student");
	}
}
?>
