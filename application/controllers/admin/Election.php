<?php
class Election extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}
	public function fn_add_election($id = ''){
		$data = array();
		$data['menu'] = 'election';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "election");
		}
		if ($this->input->post('btn_submit')) {
			$this->form_validation->set_rules('name', 'Name', 'required', array('%s field is required'));
			$this->form_validation->set_rules('hours', 'Election Hours', 'required', array('%s field is required'));
			$this->form_validation->set_rules('description', 'Description', 'required', array('%s field is required'));
			$this->form_validation->set_rules('edate', 'Election Date', 'required', array('%s field is required'));
			$this->form_validation->set_rules('registration_end_date', 'Registration End Date', 'required', array('%s field is required'));
			$this->form_validation->set_rules('ads_end_date', 'Advertisment End Date', 'required', array('%s field is required'));
			if ($this->form_validation->run() == true) {
				$level = $this->input->post('level');
				if($level != "college")
					$dno = $this->input->post('dept');
				else{
					$dno = 0;
				}
				$name = $this->input->post('name');
				$description = $this->input->post('description');
				$edate = $this->input->post('edate');
				$redate = $this->input->post('registration_end_date');
				$aedate = $this->input->post('ads_end_date');
				$hours = $this->input->post('hours');
				$status = $this->input->post('status');
				if($redate < $edate && $aedate < $edate){
					$arr = array(
						'dept_no'=>$dno,
						'name'=>$name,
						'description'=>$description,
						'edate'=>$edate,
						'registration_end_date'=>$redate,
						'ads_end_date'=>$aedate,
						'hours'=>$hours,
						'level'=>$level,
						'status' => $status
					);
					// pre($arr);die;
					if (!empty($id)) {
						$this->Main_model->update_rec($arr, $id, "election");
					} else {
						$this->Main_model->insert_rec($arr, "election");
					}
					redirect("admin/Election/fn_view_election");
				}else{
					$data['error']="Registration End Date or Advertisment End Date must be less than Election Date.";
				}
			}//if enter data is proper validation
		}
		$data['dept'] = $this->Main_model->get_rec("department","status",1);
		$this->load->view('admin/add_election', $data);
	}

	public function fn_view_election(){
		$data = array();
		$data['menu'] = 'election';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->fn_delete_election($checked[$i],false);
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("election");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Election/fn_view_election'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "election");
		$this->load->view('admin/view_election', $data);
	}
	public function fn_delete_election($id = '',$redirection=true){
		$cand_qry = "select * from candidate where eid=".$id;
		$cand = $this->Main_model->my_query($cand_qry);
		if(!@$cand['blank']){
			//when delete set it's position again to student
			$pos = $this->Main_model->like_rec("position_chart","position_name","tudent","both");
			$arr = array('position'=>$pos[0]['id']);	
			//delete related blogs and other things
			foreach($cand as $index){
				$rec = $this->Main_model->select_record($index['id'], "candidate");
				$stud = $this->Main_model->select_record($rec['stud_id'], "student");
				if(mail_send($stud['email'],"Removed From Candidate","You are removed from candidate")){}
				$this->Main_model->update_rec($arr,$stud['id'],"student");
				if($rec['image'] !="" && is_file("assets/photos/candidate_docs/".$rec['image'])){
					unlink("assets/photos/candidate_docs/".$rec['image']);
				}
				$blogs_of_cand = "select id from blogs where cid=".$index['id'];
				$blogs = $this->Main_model->my_query($blogs_of_cand);
				if(!@$blogs['blank']){
					$blogs_list=array_map(function($blog_index){
						$blog = $this->Main_model->select_record($blog_index['id'], "blogs");
						if($blog['image'] !="" && is_file("assets/photos/blogs/".$blog['image'])){
							unlink("assets/photos/blogs/".$blog['image']);
						}
						return $blog_index['id'];
					},$blogs);
					$this->Main_model->delete_rec("","",array(),"delete from likes where (tbl='blogs' and tbl_id IN(".implode(",",$blogs_list).")) or (tbl='candidate' and tbl_id=".$index['id'].")");
				}
			}
		}
		$this->Main_model->delete_rec($id, "election");
		if($redirection)
			redirect("admin/Election/fn_view_election");
	}

	public function fn_all_results(){
		$data=array();
		$data['menu'] = "ele_result";
		$this->load->helper('directory');
		$data['files'] = directory_map("assets/result_pdf/");
		$this->load->view('admin/all_result_list', $data);
	}

	public function delete_result_pdf($file){
		$file=urldecode($file);
		if($file !="" && is_file("assets/result_pdf/".$file)){
			unlink("assets/result_pdf/".$file);
		}
		redirect('admin/Election/fn_all_results');
	}
}
?>
