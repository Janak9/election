<?php
class Sub_admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
        }
	}
	public function fn_add_sub_admin($id = '')
	{
		$data = array();
		$data['menu'] = 'sub_admin';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "admin");
		}
		if ($this->input->post('btn_submit')) {
            $name=$this->input->post('name');
			$email=$this->input->post('email');
			if(empty($id))
				$password = chr(rand(65,90)).chr(rand(65,90)).rand(1111,9999);
			$status=$this->input->post('status');
			$this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[A-Za-z ]+$/]',array(
					'required'=>'%s field is required',
					'regex_match'=>'The %s must contain only letter.'));
			if(@$data['data']['email']!=$email){
				$this->form_validation->set_rules('email', 'Email', 'required|is_unique[admin.Email]');
			}
            if ($this->form_validation->run() == true) {
				$arr=array('name'=>$name,
                           'email'=>$email,
                           'type'=>1,
						   'status'=>$status
						   );
				if(empty($id))
					$arr['password']=$password;
                $config['upload_path']="assets/photos/admin/";
                $config['allowed_types']="gif|jpg|png|jpeg";
                $this->load->library("upload",$config);
                if($this->upload->do_upload('img_file')){
                    if(!empty($id)){
						$rec=$this->Main_model->select_record($id,"admin");
						if(@$rec['image'] !="" && is_file("assets/photos/admin/".@$rec['image'])){
							unlink("assets/photos/admin/".$rec['image']);
						}
                    }
                    $file=array("up_img"=>$this->upload->data());
                    $arr['image']=$file['up_img']['file_name'];
					if(empty($id)){
						if(mail_send($email,'Password For Admin Login','Your Login Password is : - '.$password)){
							$this->Main_model->insert_rec($arr,"admin");
							redirect("admin/Sub_admin/fn_view_sub_admin");
						}else{
							$data['error']="Email Does Not Send Please Try Again";
						}
					}
					else{
						$this->Main_model->update_rec($arr, $id, "admin");
						redirect("admin/Sub_admin/fn_view_sub_admin");	
					}
                }else if (!empty($id)) {
					$this->Main_model->update_rec($arr, $id, "admin");
					redirect("admin/Sub_admin/fn_view_sub_admin");
				} else {
					$data['error']=$this->upload->display_errors();
				}
			}//if enter data is proper validation
		}
		$this->load->view('admin/add_sub_admin', $data);
	}

	public function fn_view_sub_admin()
	{
		$data = array();
		$data['menu'] = 'sub_admin';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
                    $this->fn_delete_sub_admin($checked[$i],false);
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("admin","type",1);
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Sub_admin/fn_view_sub_admin'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "admin","type",1);
		$this->load->view('admin/view_sub_admin', $data);

	}
	public function fn_delete_sub_admin($id = '',$redirect=true)
	{
		$rec = $this->Main_model->select_record($id, "admin");
		if(@$rec['image'] !="" && is_file("assets/photos/admin/".@$rec['image'])){
			unlink("assets/photos/admin/".$rec['image']);
		}
		$this->Main_model->delete_rec($id, "admin");
		if($redirect)
			redirect("admin/Sub_admin/fn_view_sub_admin");
	}
}
?>
