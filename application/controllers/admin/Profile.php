<?php
class Profile extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model('admin/Login_model');
    $this->load->model('Main_model');
    $this->load->library("form_validation");
  }

  public function fn_edit_profile(){
    if (!$this->session->userdata('admin_id')) {
      redirect('admin/Login');
    }
    $data = array();
    $data['menu'] = 'dashboard';
    $id = $this->session->userdata('admin_id');
    if ($this->input->post('btn_submit')) {
      $name = $this->input->post('name');
      $this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[A-Za-z ]+$/]', array(
        'required' => '%s field is required',
        'regex_match' => 'The %s must contain only letter.'
      ));
      if ($this->form_validation->run() == true) {
        $arr = array('name' => $name);
        $config['upload_path'] = "assets/photos/admin/";
        $config['allowed_types'] = "gif|jpg|png|jpeg";
        $this->load->library("upload", $config);
        if ($this->upload->do_upload('img_file')) {
          if (!empty($id)) {
            $rec = $this->Main_model->select_record($id, "admin");
            if (@$rec['image'] != "" && is_file("assets/photos/admin/" . @$rec['image'])) {
              unlink("assets/photos/admin/" . $rec['image']);
            }
          }
          $file = array("up_img" => $this->upload->data());
          $arr['image'] = $file['up_img']['file_name'];
        }
        $this->Main_model->update_rec($arr, $id, "admin");
        redirect("admin/Profile/fn_edit_profile");
      } //if enter data is proper validation
    }
    $data['data'] = $this->Dashboard_model->get_admin_info();
    $this->load->view('admin/profile', $data);
  }

  public function fn_change_email()
  {
    if (!$this->session->userdata('admin_id')) {
      redirect('admin/Login');
    }
    $data = array();
    $data['menu'] = "dashboard";
    $data['data'] = $this->Dashboard_model->get_admin_info();
    if ($this->input->post("btn_submit")) {
      $new_email = $this->input->post('email');
      $old_email = $data['data']['email'];
      $encryption_key = "radom_32_bit_string_1234567_abcd";
      $cipher_id = safeEncrypt($data['data']['id'], $encryption_key);
      $cipher_old_email = safeEncrypt($old_email, $encryption_key);
      $cipher_new_email = safeEncrypt($new_email, $encryption_key);
      $old_link = "http://localhost/election/index.php/admin/Profile/fn_verify_email/recover_scam/" . $cipher_id . "/" . $cipher_old_email;
      $new_link = "http://localhost/election/index.php/admin/Profile/fn_verify_email/verify/" . $cipher_id . "/" . $cipher_new_email;
      $old_msg = "You will send a request to change the email to " . $new_email . ".<br><p>if you don't apply for this then please click below link and change password.</p><br>" . $old_link;
      $new_msg = "You will send a request to change the email to " . $new_email . ".<br><p>for verify please click below link.</p><br>" . $new_link;
      if (mail_send($old_email, 'Email Verification', $old_msg)) {
        if (mail_send($new_email, 'Email Verification', $new_msg)) {
          redirect("admin/Login/fn_logout");
        } else
          $data['error'] = "Email Does Not Send Please Try Again";
      } else
        $data['error'] = "Email Does Not Send Please Try Again";
    }
    $this->load->view("admin/change_email", $data);
  }

  public function fn_verify_email($type = "", $cipher_id = "", $cipher_email)
  {
    $data = array();
    $data['type'] = $type;
    $encryption_key = "radom_32_bit_string_1234567_abcd";
    $id = safeDecrypt($cipher_id, $encryption_key);
    $email = safeDecrypt($cipher_email, $encryption_key);
    if ($this->input->post("btn_submit")) {
      if ($type == "verify") {
        $pwd = $this->input->post("pwd");
        $admin_info = $this->Main_model->select_record($id, "admin");
        if ($pwd == $admin_info['password']) {
          $this->Main_model->update_rec(array("email" => $email), $id, "admin");
          redirect("admin/Login");
        } else
          $data['error'] = "Password is wrong.";
      } elseif ($type == "recover_scam") {
        $pwd = $this->input->post("pwd");
        $cpwd = $this->input->post("cpwd");
        if ($pwd == $cpwd) {
          $arr = array('email' => $email, 'password' => $pwd);
          $this->Main_model->update_rec($arr, $id, "admin");
          redirect("admin/Login");
        } else {
          $data['error'] = "Confirm password not match";
        }
      }
    }
    $this->load->view("admin/recover_scam", $data);
  }

  public function fn_change_password()
  {
    if (!$this->session->userdata('admin_id')) {
      redirect('admin/Login');
    }
    $data = array();
    $data['menu'] = "dashboard";
    if ($this->input->post("btn_submit")) {
      $old_pwd = $this->input->post("old_pwd");
      $pwd = $this->input->post("pwd");
      $cpwd = $this->input->post("cpwd");
      $admin_info = $this->Dashboard_model->get_admin_info();
      if ($admin_info['password'] == $old_pwd) {
        if ($pwd == $cpwd) {
          $this->Main_model->update_rec(array("password" => $pwd), $admin_info['id'], "admin");
          redirect("admin/Login/fn_logout");
        } else
          $data['error'] = "Confirm password not match...!";
      } else
        $data['error'] = "Password is wrong. Try Again!";
    }
    $this->load->view("admin/change_password", $data);
  }

  public function fn_forgot_password()
  {
    $data = array();
    if ($this->input->post("btn_submit")) {
      $email = $this->input->post("email");
      $tmp = $this->Main_model->row_count("admin", "", "", array("email" => $email));
      if ($tmp > 0) {
        $password = chr(rand(65, 90)) . chr(rand(65, 90)) . rand(1111, 9999);
        $admin = $this->Main_model->get_rec("admin", "email", $email);
        if (mail_send($email, 'Password For Admin Login', 'Your Login Password is : - ' . $password)) {
          $this->Main_model->update_rec(array("password" => $password), $admin[0]['id'], "admin");
          redirect("admin/Login/fn_logout");
        } else {
          $data['error'] = "Email Does Not Send Please Try Again";
        }
      } else
        $data['error'] = "Email not exists";
    }
    $this->load->view("admin/forgot_password", $data);
  }
}
 