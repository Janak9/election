<?php
class Division extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}
	public function fn_add_division($id = '')
	{
		$data = array();
		$data['menu'] = 'division';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "division");
		}
		if ($this->input->post('btn_submit')) {
			$this->form_validation->set_rules('no_of_div', 'Number Of Divsion', 'required', array('%s field is required'));
			if ($this->form_validation->run() == true) {
				$dno = $this->input->post('dept');
				$year = $this->input->post('year');
				$no_of_div = $this->input->post('no_of_div');
				$status = $this->input->post('status');
				$arr = array(
					'dept_no'=>$dno,
					'year'=>$year,
					'no_of_div'=>$no_of_div,
					'status' => $status
				);
				if (!empty($id)) {
					$this->Main_model->update_rec($arr, $id, "division");
				} else {
					$this->Main_model->insert_rec($arr, "division");
				}
				redirect("admin/Division/fn_view_division");
			}//if enter data is proper validation
		}
		$data['dept'] = $this->Main_model->get_rec("department","status",1);
		$this->load->view('admin/add_division', $data);
	}

	public function fn_view_division()
	{
		$data = array();
		$data['menu'] = 'division';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->Main_model->delete_rec($checked[$i], "division");
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("division");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Division/fn_view_division'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "division");
		$this->load->view('admin/view_division', $data);
	}
	public function fn_delete_division($id = '')
	{
		$this->Main_model->delete_rec($id, "division");
		redirect("admin/Division/fn_view_division");
	}
}
?>
