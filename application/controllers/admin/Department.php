<?php
class Department extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}
	public function fn_add_department($id = '')
	{
		$data = array();
		$data['menu'] = 'department';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "department");
		}
		if ($this->input->post('btn_submit')) {
			$this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[A-Za-z ]+$/]', array(
				'required' => '%s field is required',
				'regex_match' => 'The %s must contain only letter.'
			));
			if ($this->form_validation->run() == true) {
				$name = $this->input->post('name');
				$status = $this->input->post('status');
				$arr = array(
					'dept_name' => $name,
					'status' => $status
				);

				if (!empty($id)) {
					$this->Main_model->update_rec($arr, $id, "department");
				} else {
					$this->Main_model->insert_rec($arr, "department");
				}
				redirect("admin/Department/fn_view_department");
			}//if enter data is proper validation
		}
		$this->load->view('admin/add_department', $data);
	}

	public function fn_view_department()
	{
		$data = array();
		$data['menu'] = 'department';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->Main_model->delete_rec($checked[$i], "department");
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("department");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Department/fn_view_department'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "department");
		$this->load->view('admin/view_department', $data);

	}
	public function fn_delete_department($id = '')
	{
		$this->Main_model->delete_rec($id, "department");
		redirect("admin/Division/fn_view_department");
	}
}
?>
