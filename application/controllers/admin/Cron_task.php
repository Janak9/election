<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_task extends CI_Controller {	
    function __construct(){
        parent::__construct();
        $this->load->model("Main_model");
    }

	public function election_notifier()
	{
        $election = $this->Main_model->get_rec("election","status","1");
        if(!@$election['blank']){
            foreach($election as $ele){
                if((strtotime($ele['registration_end_date']) > strtotime(date('Y-m-d H:i:s'))) && (strtotime($ele['registration_end_date']) < (strtotime(date('Y-m-d H:i:s')) + 7200))){ // 7200 = 2 hour(60*60*2)
                    if($ele['dept_no']!=0){
                        $email_arr=$this->Main_model->multitask("email","student",array("dept_no"=>$ele['dept_no']));
                    }else{
                        $email_arr=$this->Main_model->multitask("email","student");
                    }
                    if(!@$email_arr['blank']){
                        $email_list=array_map(function($index){
                            return $index['email'];
                        },$email_arr);        
                        $subject="Election Registration";
                        $msg="Election Registration will be end in <b>".$ele['registration_end_date']."</b> so if you want to apply for it then click below link.<br><br><a href='http://localhost/election/index.php/user/Dashboard/fn_apply_for_election'>Click Here For Apply</a><br><br><p style='color:red;'>*Note: If you already applied then ignore this email.</p>";
                        if(mail_send(implode(",",$email_list),$subject,$msg)){
                            echo "Email sent<br>";
                        }else{
                            echo "Email Does Not Send Please Try Again...!<br>";
                        }
                    }
                }

                if((strtotime($ele['ads_end_date']) > strtotime(date('Y-m-d H:i:s'))) && (strtotime($ele['ads_end_date']) < (strtotime(date('Y-m-d H:i:s')) + 7200))){
                    if($ele['dept_no']!=0){
                        $stud_list = $this->Main_model->get_rec("candidate","eid",$ele['id']);
                    }
                    if(!@$stud_list['blank']){
                        $email_arr=array();
                        for($i=0;$i<count($stud_list);$i++){
                            $email_arr[$i]=$this->Main_model->multitask("email","student",array("id"=>$stud_list[$i]['stud_id']));
                            $email_arr[$i] = $email_arr[$i][0];
                        }
                        $email_list=array_map(function($index){
                            return $index['email'];
                        },$email_arr);        
                        $subject="Election Advertisment";
                        $msg="Election Advertisment will be end in <b>".$ele['ads_end_date']."</b> so be prepared for it. Click below link to add blogs.<br><br><a href='http://localhost/election/index.php/user/Candidate/fn_add_blogs'>Click Here For Add Blogs</a>";
                        if(mail_send(implode(",",$email_list),$subject,$msg)){
                            echo "Email sent<br>";
                        }else{
                            echo "Email Does Not Send Please Try Again...!<br>";
                        }
                    }
                }

                if((strtotime($ele['edate']) > strtotime(date('Y-m-d H:i:s'))) && (strtotime($ele['edate']) < (strtotime(date('Y-m-d H:i:s')) + 7200))){
                    if($ele['dept_no']!=0){
                        $email_arr=$this->Main_model->multitask("email","student",array("dept_no"=>$ele['dept_no']));
                    }else{
                        $email_arr=$this->Main_model->multitask("email","student");
                    }
                    if(!@$email_arr['blank']){
                        $email_list=array_map(function($index){
                            return $index['email'];
                        },$email_arr);        
                        $subject="Election Is Starting";
                        $msg="Election will be starting in <b>".$ele['edate']."</b> so go through below link and elect your favorite candidate.<br><br><a href='http://localhost/election/index.php/user/Home'>Click Here For Vote</a><br><br><p style='color:red;'>*Note: If you already given vote then ignore this email.</p>";
                        if(mail_send(implode(",",$email_list),$subject,$msg)){
                            echo "Email sent<br>";
                        }else{
                            echo "Email Does Not Send Please Try Again...!<br>";
                        }
                    }
                }

                if((strtotime($ele['edate']) < strtotime(date('Y-m-d H:i:s'))) && ((strtotime($ele['edate']) + 3600) > strtotime(date('Y-m-d H:i:s')))){
                    if($ele['dept_no']!=0){
                        $email_arr=$this->Main_model->multitask("email","student",array("dept_no"=>$ele['dept_no']));
                    }else{
                        $email_arr=$this->Main_model->multitask("email","student");
                    }
                    if(!@$email_arr['blank']){
                        $email_list=array_map(function($index){
                            return $index['email'];
                        },$email_arr);        
                        $subject="Election Is Started";
                        $msg="Election was started from <b>".$ele['edate']."</b> so go through below link and elect your favorite candidate.<br><br><a href='http://localhost/election/index.php/user/Home'>Click Here For Vote</a><br><br><p style='color:red;'>*Note: If you already given vote then ignore this email.</p>";
                        if(mail_send(implode(",",$email_list),$subject,$msg)){
                            echo "Email sent<br>";
                        }else{
                            echo "Email Does Not Send Please Try Again...!<br>";
                        }
                    }
                }
                
                if(((strtotime($ele['edate'])+($ele['hours']*60*60)) < strtotime(date('Y-m-d H:i:s'))) && ((strtotime($ele['edate'])+($ele['hours']*60*60) + 1800) > strtotime(date('Y-m-d H:i:s')))){ // 1800 = 30 min(60*30)
                    //create pdf
                    $this->pdf_code($ele['id']);
                    // remove candidates whose not win
                    if($ele['dept_no']!=0){
                        $cal_vote_sql = "select v.*,s.id as stud_id,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=".$ele['id']." group by v.cid order by total_vote desc";
                        $cal_vote = $this->Main_model->my_query($cal_vote_sql);
                        $winner = array();
                        if (!@$cal_vote['blank']) {
                            $k = 0;
                            $prev_pos = "";
                            for ($i = 0; $i < count($cal_vote); $i++) {
                                $cal_vote[$i]['position_name'];
                                if ($k == 0 || $cal_vote[$i]['position_name'] != $prev_pos) {
                                    $winner[$k] = $cal_vote[$i]['cid'];
                                    $prev_pos = $cal_vote[$i]['position_name'];
                                    $k++;
                                    if ($k == 2)
                                        break;
                                }
                            }
                        }
                        if(empty($winner)){
                            $losser_candidate_sql = "select c.id,c.stud_id,c.image from candidate c where c.eid=".$ele['id'];
                        }else{
                            $losser_candidate_sql = "select c.id,c.stud_id,c.image from candidate c where c.eid=".$ele['id']." and c.id NOT IN(".implode(",",$winner).")";
                        }
                        $losser_candidate = $this->Main_model->my_query($losser_candidate_sql);
                        if(!@$losser_candidate['blank']){
                            $pos = $this->Main_model->like_rec("position_chart","position_name","tudent","both");
                            $arr = array('position'=>$pos[0]['id']);
                            $cand_list=array_map(function($index) use ($arr){
                                $this->Main_model->update_rec($arr,$index['stud_id'],"student");
                                if($index['image'] !="" && is_file("assets/photos/candidate_docs/".$index['image'])){
                                    unlink("assets/photos/candidate_docs/".$index['image']);
                                }
                                return $index['id'];
                            },$losser_candidate);
                            $blogs_of_cand = "select id from blogs where cid IN(".implode(",",$cand_list).")";
                            $blogs = $this->Main_model->my_query($blogs_of_cand);
                            if(!@$blogs['blank']){
                                $blogs_list=array_map(function($index){
                                    $blog = $this->Main_model->select_record($index['id'], "blogs");
                                    if($blog['image'] !="" && is_file("assets/photos/blogs/".$blog['image'])){
                                        unlink("assets/photos/blogs/".$blog['image']);
                                    }
                                    return $index['id'];
                                },$blogs);
                                $this->Main_model->delete_rec("","",array(),"delete from likes where (tbl='blogs' and tbl_id IN(".implode(",",$blogs_list).")) or (tbl='candidate' and tbl_id IN(".implode(",",$cand_list)."))");
                            }
                            $this->Main_model->delete_rec("","",array(),"delete from candidate where id IN(".implode(",",$cand_list).")");
                        }
                    }
                    //email send
                    if($ele['dept_no']!=0){
                        $email_arr=$this->Main_model->multitask("email","student",array("dept_no"=>$ele['dept_no']));
                    }else{
                        $email_arr=$this->Main_model->multitask("email","student");
                    }
                    if(!@$email_arr['blank']){
                        $email_list=array_map(function($index){
                            return $index['email'];
                        },$email_arr);        
                        $subject="Election Result";
                        $msg="Election was end on <b>".$ele['edate'].".</b> Click below link for view result.<br><br><a href='http://localhost/election/index.php/user/Home'>Click Here For Result</a><br><br>";
                        if(mail_send(implode(",",$email_list),$subject,$msg)){
                            echo "Email sent<br>";
                        }else{
                            echo "Email Does Not Send Please Try Again...!<br>";
                        }
                    }
                }
            }
        }
    }

    public function pdf_code($eid){
        $data['election'] = $this->Main_model->select_record($eid, "election");
        if($data['election']['dept_no']!=0){
            $cal_vote_sql = "select v.*,s.id as stud_id,s.year,s.dept_no,s.division,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=".$data['election']['id']." group by v.cid order by s.dept_no,s.year,s.division,total_vote desc";
            $data['cal_vote'] = $this->Main_model->my_query($cal_vote_sql);
            if (!@$data['cal_vote']['blank']) {
                $k = 0;
                $prev_pos = "";
                $prev_dept = 0;
                $prev_year = 0;
                $prev_div = 0;
                $change_flag = false;
                $data['winner'] = array();
                for ($i = 0; $i < count($data['cal_vote']); $i++) {
                    $data['cal_vote'][$i]['position_name'];
                    if($k == 0 || ($prev_dept != $data['cal_vote'][$i]['dept_no'] || $prev_year != $data['cal_vote'][$i]['year'] || $prev_div != $data['cal_vote'][$i]['division']) ){
                        $prev_dept = $data['cal_vote'][$i]['dept_no'];
                        $prev_year = $data['cal_vote'][$i]['year'];
                        $prev_div = $data['cal_vote'][$i]['division'];
                        $change_flag = true;
                    }
                    if ($change_flag || ($data['cal_vote'][$i]['position_name'] != $prev_pos && $k%2 != 0)){
                        $change_flag = false;
                        $data['winner'][$k] = $data['cal_vote'][$i];
                        $prev_pos = $data['cal_vote'][$i]['position_name'];
                        $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                        $k++;
                    }
                }
            }
             /*
                $k = 0;
                $prev_pos = "";
                $prev_dept = 0;
                $prev_year = 0;
                $prev_div = 0;
                $change_flag = false;
                $data['winner'] = array();
                for ($i = 0; $i < count($data['cal_vote']); $i++) {
                    if($k == 0 || ($prev_dept != $data['cal_vote'][$i]['dept_no'] || $prev_year != $data['cal_vote'][$i]['year'] || $prev_div != $data['cal_vote'][$i]['division']) ){
                        $prev_dept = $data['cal_vote'][$i]['dept_no'];
                        $prev_year = $data['cal_vote'][$i]['year'];
                        $prev_div = $data['cal_vote'][$i]['division'];
                        $change_flag = true;
                    }
                    if ($change_flag || ($data['cal_vote'][$i]['position_name'] != $prev_pos && $k%2 != 0)){
                        $change_flag = false;
                        $data['winner'][$k] = $data['cal_vote'][$i];
                        $prev_pos = $data['cal_vote'][$i]['position_name'];
                        $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                        $k++;
                    }
                }
            */
        }else{
            $cal_vote_sql = "select v.*,s.id as stud_id,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=".$data['election']['id']." group by v.cid order by total_vote desc";
            $data['cal_vote'] = $this->Main_model->my_query($cal_vote_sql);

            $data['winner'] = array();
            if (!@$data['cal_vote']['blank']) {
                $k = 0;
                $prev_pos = "";
                for ($i = 0; $i < count($data['cal_vote']); $i++) {
                    $data['cal_vote'][$i]['position_name'];
                    if ($k == 0 || $data['cal_vote'][$i]['position_name'] != $prev_pos) {
                        $data['winner'][$k] = $data['cal_vote'][$i];
                        $prev_pos = $data['cal_vote'][$i]['position_name'];
                        $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                        $k++;
                        if ($k == 2)
                            break;
                    }
                }
            }

            // update position of not winned candidate
            $win_list = array_map(function($index){
                return $index['cid'];
            },$data['winner']);
            if(empty($win_list)){
                $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid order by s.dept_no,s.year,s.division,c.position DESC";
            }else{
                $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid and c.id NOT IN (".implode(",",$win_list).") order by s.dept_no,s.year,s.division,c.position DESC";
            }
            $not_win = $this->Main_model->my_query($not_win_sql);
            if(!@$not_win['blank']){
                $i=0;
                foreach($not_win as $n){
                    $this->Main_model->update_rec(array('position'=>$not_win[$i]['current_position']),$not_win[$i]['cand_id'],"candidate");
                    $i++;
                }
            }
        }
    
        $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid order by s.dept_no,s.year,s.division,c.position DESC";
        $data['candidate'] = $this->Main_model->my_query($candidate_sql);
        
        if(!@$data['candidate']['blank']){ // order by vote
            $cal_vote_arr = array();
            if(!@$data['cal_vote']){
                $cal_vote_arr = array_map(function ($w) {
                    return $w['cid'];
                }, $data['cal_vote']);
            }
            $i=0;
            foreach ($data['candidate'] as $cand) {
                if (in_array($cand['cand_id'], $cal_vote_arr) == 1) {
                    $k = array_search($cand['cand_id'], $cal_vote_arr);
                    $data['candidate'][$i]['total_vote'] = $data['cal_vote'][$k]['total_vote'];
                } else {
                    $data['candidate'][$i]['total_vote'] = 0;
                }
                $i++;
            }
            usort($data['candidate'], function($a, $b) {
                return $a['total_vote'] <=> $b['total_vote'];
            });
            $data['candidate'] = array_reverse($data['candidate']);
        }

        if($data['election']['dept_no']!=0){
            $dept=$this->Main_model->select_record($data['election']['dept_no'],"department");
            $dept_name = $dept['dept_name'];
        }else
            $dept_name = "College";
        
        $edt = $data['election']['edate'];
        $edt = str_replace(":","_",$edt);
        $edt = str_replace(" ","_",$edt);
        $fname = $_SERVER['DOCUMENT_ROOT'] .'/election/assets/result_pdf/'.$edt.'_'.$dept_name.'.pdf';
    
        // $fname = dirname(__DIR__).'/../../assets/result_pdf/'.$data['election']['edate'].'_'.$dept_name.'.pdf';
        if(!is_file($fname)){
            $clg_info=$this->Main_model->select_record(1,"clg_info");
            ob_start();
            $this->load->library('Pdf');
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetTitle('Election Result '.$data['election']['edate'].'_'.$dept_name);
            $pdf->SetHeaderData('assets/photos/'.$clg_info['clg_logo'],
                                20, // logo height
                                $clg_info['clg_name'],
                                $clg_info['website']."\nResult Of ".$data['election']['name'],
                                array(0,64,255), array(0,64,128));
            // $pdf->setFooterData(array(0,64,0), array(0,64,128));
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetTopMargin(30);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Janak');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->AddPage();
            $pdf->writeHTML($this->load->view('user/result_pdf',$data,TRUE), true, false, true, false, '');
            ob_end_clean();
            $pdf->Output($fname, 'F'); // I for open in browser, F for save in folder
        }
        return $edt.'_'.$dept_name.'.pdf';
    }
}
?>