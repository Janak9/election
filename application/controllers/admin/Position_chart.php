<?php
class Position_chart extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if (!$this->session->userdata('admin_id')) {
			redirect('admin/Login');
		}
	}
	public function fn_add_position_chart($id = '')
	{
		$data = array();
		$data['menu'] = 'position_chart';
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "position_chart");
		}
		if ($this->input->post('btn_submit')) {
			$this->form_validation->set_rules('name', 'Name', 'required|regex_match[/^[A-Za-z ]+$/]', array(
				'required' => '%s field is required',
				'regex_match' => 'The %s must contain only letter.'
			));
			if ($this->form_validation->run() == true) {
                $name = $this->input->post('name');
                $no = $this->input->post('no');
				$status = $this->input->post('status');
				$arr = array(
                    'position_name' => $name,
                    'position_no' => $no,
					'status' => $status
				);

				if (!empty($id)) {
					$this->Main_model->update_rec($arr, $id, "position_chart");
				} else {
					$this->Main_model->insert_rec($arr, "position_chart");
				}
				redirect("admin/Position_chart/fn_view_position_chart");
			}//if enter data is proper validation
		}
		$this->load->view('admin/add_position_chart', $data);
	}

	public function fn_view_position_chart()
	{
		$data = array();
		$data['menu'] = 'position_chart';
		if ($this->input->post()) {
			$checked = $this->input->post('chk');
			if ($checked != null) {
				for ($i = 0; $i < sizeof($checked); $i++) {
					$this->Main_model->delete_rec($checked[$i], "position_chart");
				}
			}
		}
		$per_page = 3;
		$start = $this->uri->segment(4);
		$total = $this->Main_model->row_count("position_chart");
		$data['pagination'] = pagination($total, $per_page, site_url('admin/Position_chart/fn_view_position_chart'));
		$data['data'] = $this->Main_model->view_rec($per_page, $start, "position_chart");
		$this->load->view('admin/view_position_chart', $data);

	}
	public function fn_delete_position_chart($id = '')
	{
		$this->Main_model->delete_rec($id, "position_chart");
		redirect("admin/Division/fn_view_position_chart");
	}
}
?>
