<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {	
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Login_model');
	}
	public function index()
	{
		$data=array();
		if($this->session->userdata('admin_id')){
			redirect('admin/Dashboard');	
		}
		if($this->input->post('btn_submit'))
		{
			$qry=$this->Login_model->check_user();
			$num=$qry->num_rows();
			if($num==1){
				$row=$qry->row_array();
				$this->session->set_userdata('admin_id',$row['id']);
				redirect('admin/Dashboard');		
			}else{
				$data['error']="invalid username or password";
			}
		}
		$this->load->view('admin/login',$data);
		
	}
	public function fn_logout(){
		$this->session->unset_userdata('admin_id');
		redirect("admin/Login");
	}
}
?>