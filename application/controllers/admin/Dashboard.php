<?php
class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Login_model');
		$this->load->model('Main_model');
		$this->load->library("form_validation");
		if(!$this->session->userdata('admin_id')){
			redirect('admin/Login');	
		}
	}
	public function index()
	{
		$data= array();
		$data['menu']='dashboard';
		$this->load->view('admin/index',$data);
	}
	public function fn_status_changer()
	{
		$tbl=$_REQUEST['tbl'];
		$tbl_id=$_REQUEST['tbl_id'];
		$val=$_REQUEST['val'];
		$arr=array('status'=>$val);
		$this->Main_model->update_rec($arr,$tbl_id,$tbl);
	}
	
	public function fn_add_more_img()
	{
		$cnt['cnt']=$this->input->post('cnt');  
		$this->load->view('admin/get_image',$cnt);
	}

	public function fn_get_division()
	{
		$dept_no=$_POST['dept_no'];
		$year=$_POST['year'];
		$data = $this->Main_model->get_rec("division","","",array('dept_no'=>$dept_no,'year'=>$year,"status"=>1));
		echo '<option value="">Select division</option>';
		if(!@$data['blank']){
			for($i=1;$i<=$data[0]['no_of_div'];$i++){
				echo "<option value='$i'>$i</option>";
			}
		}
	}
}
?>