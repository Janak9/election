<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
    }
    
    public function fn_profile(){
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
        $data = array();
        if($this->input->post('btn_submit')){
            $data['user_info']=$this->User_dashboard_model->get_user_info();
            $config['upload_path'] = "assets/photos/students/";
            $config['allowed_types'] = "gif|jpg|png|jpeg";
            $config['max_size'] = "2048";
            $this->load->library("upload", $config);
            if ($this->upload->do_upload('img_file')) {
                $file = array("up_img" => $this->upload->data());
                $arr =array('image' => $file['up_img']['file_name']);
                if(@$data['user_info']['image'] !="" && is_file("assets/photos/students/".@$data['user_info']['image'])){
                    unlink("assets/photos/students/".$data['user_info']['image']);
                }
                $this->Main_model->update_rec($arr, $data['user_info']['id'], "student");
                echo "<script>alert('Update Successfully.')</script>";
            } else{
                echo "<script>alert('".$this->upload->display_errors()."')</script>";
            }
        }
        $data['user_info']=$this->User_dashboard_model->get_user_info();
		$this->load->view("user/profile",$data);
    }
    
    public function fn_change_pwd(){
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
        $data = array();
        if($this->input->post('btn_submit')){
            $old=$this->input->post("old_pwd");
            $new=$this->input->post("new_pwd");
            $cnew=$this->input->post("cnew_pwd");
            if($cnew==$new){
                $user_info=$this->User_dashboard_model->get_user_info();
			    if($user_info['password']==$old){
                    $this->Main_model->update_rec(array("password"=>$new),$user_info['id'],"student");
					redirect("user/Login/fn_logout");
                }else
                    $data['error']="Password is wrong. Try Again!";
            }else
                $data['error']="Confirm password not match...!";
        }
		$this->load->view("user/change_password",$data);
    }

    public function fn_apply_for_election(){
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
        $data = array();
        $data['user_info']=$this->User_dashboard_model->get_user_info();
        $position = $this->Main_model->select_record($data['user_info']['position'],"position_chart");
        if($position['position_name']=="student"){
            $data['position'] = $this->Main_model->like_rec("position_chart",'position_name','class',"both","",array("status"=>1));
        }else{
            $data['position'] = $this->Main_model->get_rec("position_chart","status",1);
        }

        if($this->input->post('btn_submit')){
            $election_cond=array(
                'dept_no'=>$data['user_info']['dept_no'],
                'level'=>'class'
            );
            $election=$this->Main_model->get_rec("election","","",$election_cond);
            if(@$election['blank']){
                echo "<script>alert('Sorry Election Is Not Available For Your Department...!');</script>";
                $this->load->view("user/apply_for_election",$data);
                return;
            }else if((strtotime($election[0]['registration_end_date']) - strtotime(date('Y-m-d H:i:s'))) <= 0){
                echo "<script>alert('Sorry Election Registration Time Is Over... :(');</script>";
                $this->load->view("user/apply_for_election",$data);
                return;
            }
            
            $can_con=array(
                "stud_id"=>$data['user_info']['id'],
                "position > "=>$data['user_info']['position'],
            );
            $candidate = $this->Main_model->get_rec("candidate","","",$can_con);
            if(!@$candidate['blank']){
                echo "<script>alert('Already Applied!')</script>";
                $this->load->view("user/apply_for_election",$data);
                return;
            }

            $this->form_validation->set_rules('proposer_id', 'Proposer', 'required', array('Please Select %s'));
            $this->form_validation->set_rules('supporter_id', 'Supporter', 'required', array('Please Select %s'));
            if ($this->form_validation->run() == true) {
                $pid=$this->input->post("proposer_id");
                $sid=$this->input->post("supporter_id");
                $position=$this->input->post("position");
                $arr = array(
                    'stud_id'=>$data['user_info']['id'],
                    'proposer_id'=>$pid,
                    'supporter_id'=>$sid,
                    'position'=>$position,
                    'eid'=>$election[0]['id']
                );
                $config['upload_path'] = "assets/photos/candidate_docs/";
                $config['allowed_types'] = "gif|jpg|png|jpeg";
                $config['max_size'] = "2048";
                $this->load->library("upload", $config);
                if ($this->upload->do_upload('img_file')) {
                    $file = array("up_img" => $this->upload->data());
                    $arr['image']= $file['up_img']['file_name'];
                    $this->Main_model->insert_rec($arr, "candidate");
                    echo "<script>alert('Applied Successfully.')</script>";
                } else{
                    echo "<script>alert('".$this->upload->display_errors()."')</script>";
                }
            }else{
                $data['error']=validation_errors();
            }
        }
		$this->load->view("user/apply_for_election",$data);
    }
    
    public function fn_get_student_card(){
        $data=array();
        $user_info=$this->User_dashboard_model->get_user_info();
        $rno=$_POST['rno'];
        $name=$_POST['name'];
        $detail_id=$_POST['detail_id'];
        $data['detail_id']=$detail_id;
        
        $cand_id = $this->Main_model->multitask("stud_id","candidate");
        $pro_id = $this->Main_model->multitask("proposer_id","candidate");
        $sup_id = $this->Main_model->multitask("supporter_id","candidate");
        $cand_list=array();
        if(!@$cand_id['blank']){
            $cand_list = array_map(function($index){
                return $index['stud_id'];
            },$cand_id);
        }
        $pro_list=array();
        if(!@$pro_id['blank']){
            $pro_list = array_map(function($index){
                return $index['proposer_id'];
            },$pro_id);
        }
        $sup_list=array();
        if(!@$sup_id['blank']){
            $sup_list = array_map(function($index){
                return $index['supporter_id'];
            },$sup_id);
        }
        $current_id=array($_POST['sid'],$_POST['pid'],$user_info['id']);
        $all_id=array_merge($current_id,$cand_list,$pro_list,$sup_list);
        
        if($rno!="" && $name!=""){
            $like_arr = array("rno"=>$rno,"name"=>$name);
        }elseif($rno!=""){
            $like_arr = array("rno"=>$rno);
        }elseif($name!=""){
            $like_arr = array("name"=>$name);
        }else{
            $like_arr = array();
        }
        
        $con_arr = array(
            "dept_no"=>$user_info['dept_no'],
            "year"=>$user_info['year'],
            "division"=>$user_info['division']
        );
    
        $this->db->where_not_in("id",$all_id);
        $data['student']=$this->Main_model->like_rec("student","","","both",$like_arr,$con_arr);
        $this->load->view("user/get_student_card",$data);
    }

    public function fn_get_student_detail(){
        $data=array();        
        $id=$_POST['stud_id'];
        $detail_id=$_POST['detail_id'];
        if(strpos($detail_id,"proposer")!== false)
            $type="proposer";
        else
            $type="supporter";
        $data['type']=$type;
        $data['student']=$this->Main_model->select_record($id,'student');
        $this->load->view("user/get_student_details",$data);
    }
}
?>