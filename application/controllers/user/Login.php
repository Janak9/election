<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {	
    function __construct()
	{
		parent::__construct();
		$this->load->model('user/Login_model');
		$this->load->model('Main_model');
	}
	public function index()
	{
		$data=array();
		if($this->session->userdata('user_id')){
			redirect('user/Home');	
		}
		if($this->input->post('btn_submit'))
		{
			$qry=$this->Login_model->check_user();
			$num=$qry->num_rows();
			if($num==1){
				$row=$qry->row_array();
				$this->session->set_userdata('user_id',$row['id']);
				redirect('user/Home');		
			}else{
				$data['error']="invalid username or password";
			}
		}
		$this->load->view('user/login',$data);
	}
	public function fn_logout(){
		$this->session->unset_userdata('user_id');
		redirect("user/Login");
	}
	public function fn_forgot_password(){
		$data=array();
		if($this->input->post("btn_submit")){
			$email=$this->input->post("email");
			$tmp=$this->Main_model->row_count("student","","",array("email"=>$email));
			if($tmp>0){
				$password = chr(rand(65,90)).chr(rand(65,90)).rand(1111,9999);
				$user=$this->Main_model->get_rec("student","email",$email);
				if(mail_send($email,'Password For Admin Login','Your Login Password is : - '.$password)){
					$this->Main_model->update_rec(array("password"=>$password),$user[0]['id'],"student");
					redirect("user/Login/fn_logout");
				}else{
					$data['error']="Email Does Not Send Please Try Again";
				}
			}else
				$data['error']="Email not exists";
		}
		$this->load->view("user/forgot_password",$data);
	}
}
?>