<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
    }

	public function index(){
        $data = array();
        $data['slider']=$this->Main_model->get_rec("slider","status",1);
        if(@$this->session->userdata('user_id')){
            $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
            
            if(@$this->input->post('apply_for_college_lvl')){
                $up_arr=array(
                    'eid'=>$this->input->post('eid'),
                    'position'=>$this->input->post('apply_for_college_lvl')
                );
                $this->Main_model->update_rec($up_arr,"","candidate",array('stud_id'=>$data['user_info']['id']));
                echo "<script>alert('Successfully Applied... :) ')</script>";
            }

            $data['election']=$this->Main_model->get_rec("election","status","1",array(),array("edate"=>"ASC"));

            $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and (s.dept_no=".$data['user_info']['dept_no']." or p.position_name like '%College%')";
            $data['candidate']=$this->Main_model->my_query($candidate_sql);

            $blogs_query = "SELECT b.*,s.name as 'cand_name',s.image as 'cand_pic' FROM `blogs` b,student s,candidate c WHERE b.cid=c.id and c.stud_id=s.id and b.status=1 and s.dept_no=".$data['user_info']['dept_no']." order by created_at DESC limit 3";
            $data['blogs'] = $this->Main_model->my_query($blogs_query);
            
        }
		$this->load->view("user/index",$data);
    }
    
    public function fn_about(){
        $data = array();
        $data['clg_info']=$this->Main_model->select_record(1,"clg_info");
		$this->load->view("user/about",$data);
    }
}
?>