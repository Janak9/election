<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
    }

	public function fn_chat_list(){
        $this->load->view("user/chat_list");
    }

    public function fn_chat_list_data(){
        $data = array();
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");

        $receive_chat_qry="select * from (select * from chat where r_id=".$data['user_info']['id'].") AS tmp group by s_id order by id DESC";
        $receive_chat = $this->Main_model->my_query($receive_chat_qry);

        $receive_chat_id_qry="select s_id from (select * from chat where r_id=".$data['user_info']['id'].") AS tmp group by s_id order by id DESC";
        $receive_chat_id = $this->Main_model->my_query($receive_chat_id_qry);
        $receive_chat_id_str="";
        if(!@$receive_chat_id['blank']){
            $receive_chat_id=array_map(function($index){
                return $index['s_id'];
            },$receive_chat_id);
            $receive_chat_id_str=implode(",",$receive_chat_id);
        }
        $send_chat_qry="select * from (select * from chat where s_id=".$data['user_info']['id'].") AS tmp where r_id NOT IN($receive_chat_id_str) group by r_id order by id DESC";
        $send_chat = $this->Main_model->my_query($send_chat_qry);
        if (!@$send_chat['blank']) {
            $mrg_arr=array_merge($receive_chat,$send_chat);
            $mrg_arr_sort = array();
            foreach ($mrg_arr as $key => $row)
            {
                $last_msg_qry="select * from chat where (r_id=".$row['r_id']." and s_id=".$row['s_id'].") or (r_id=".$row['s_id']." and s_id=".$row['r_id'].") ORDER BY id DESC limit 1";
                $last_msg = $this->Main_model->my_query($last_msg_qry);
                $mrg_arr[$key] = $last_msg[0];
                $mrg_arr_sort[$key] = $mrg_arr[$key]['id'];
            }
            array_multisort($mrg_arr_sort, SORT_DESC, $mrg_arr);
        }else{
            foreach ($receive_chat as $key => $row)
            {
                $last_msg_qry="select * from chat where (r_id=".$row['r_id']." and s_id=".$row['s_id'].") or (r_id=".$row['s_id']." and s_id=".$row['r_id'].") ORDER BY id DESC limit 1";
                $last_msg = $this->Main_model->my_query($last_msg_qry);
                $receive_chat[$key] = $last_msg[0];
            }
            $mrg_arr=$receive_chat;
        }
        $data['data']=$mrg_arr;
        $this->load->view("user/get_chat_list",$data);
    }

    public function fn_chat_details($rid){
        $data = array();
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");

        $chat_qry="select * from chat where (r_id=".$rid." and s_id=".$data['user_info']['id'].") or (r_id=".$data['user_info']['id']." and s_id=".$rid.") ORDER BY id";
        $data['chat_res'] = $this->Main_model->my_query($chat_qry);
        
        $data['s_id']=$data['user_info']['id'];
        $data['r_id']=$rid;
        $data['receiver']=$this->Main_model->select_record($rid,"student");
        $this->load->view("user/chat_details",$data);
    }

    public function fn_send_msg(){
        $arr = array(
            's_id'=>$_REQUEST['s_id'],
            'r_id'=>$_REQUEST['r_id'],
            'msg'=>urldecode($_REQUEST['msg'])
        );
        $this->Main_model->insert_rec($arr,"chat");
    }

    public function fn_chat_box(){
        $data = array();
        $s_id=$_REQUEST["s_id"];
        $r_id=$_REQUEST["r_id"];
        $chat_qry="select * from chat where (s_id=$s_id and r_id=$r_id) or (s_id=$r_id and r_id=$s_id)";
        $data['chat_res'] = $this->Main_model->my_query($chat_qry);
        $data['s_id']=$s_id;
        $up_status_cond = array(
            's_id'=>$r_id,
            'r_id'=>$s_id,
            'read_status'=>0
        );
        $this->Main_model->update_rec(array('read_status'=>1),"","chat",$up_status_cond);
        $this->load->view('user/get_chat_box',$data);
    }

    public function fn_chat_count(){
        $user_info=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $receive_chat_qry="select * from (select * from chat where r_id=".$user_info['id']." and read_status=0) AS tmp group by s_id order by id DESC";
        $receive_chat = $this->Main_model->my_query($receive_chat_qry);
        unset($receive_chat['blank']);
        echo count($receive_chat);
    }
}
?>
