<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
    }

	public function fn_blogs_list(){
        $data = array();
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $blogs_query = "SELECT b.*,s.name as 'cand_name',s.image as 'cand_pic' FROM `blogs` b,student s,candidate c WHERE b.cid=c.id and c.stud_id=s.id and b.status=1 and s.dept_no=".$data['user_info']['dept_no']." order by created_at DESC";
        $data['data'] = $this->Main_model->my_query($blogs_query);
        $this->load->view("user/blogs_list",$data);
    }

    public function fn_blogs_details($id){
        $data = array();
        if($this->input->post("btn_submit")){
            if($this->input->post("comment_txt")==""){
                echo "<script>alert('Please write somthing in comment...!')</script>";
            }
            else{
                $arr=array(
                    'bid'=>$id,
                    'uid'=>$this->session->userdata('user_id'),
                    'replay_id'=>$this->input->post('replay_id'),
                    'comment'=>$this->input->post('comment_txt')
                );
                $this->Main_model->insert_rec($arr,"comment");
            }
        }
        $data['blog']=$this->Main_model->select_record($id,"blogs");
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $data['candidate']=$this->Main_model->select_record($data['blog']['cid'],"candidate");
        $data['student']=$this->Main_model->select_record($data['candidate']['stud_id'],"student");
        $comment_cond = array(
            'bid'=>$id,
            'replay_id'=>0
        );
        $data['comments']=$this->Main_model->get_rec("comment","","",$comment_cond);
        $i=0;
        if(!@$data['comments']['blank']){
            foreach($data['comments'] as $comment){
                $data['comments'][$i]['student']=$this->Main_model->select_record($comment['uid'],"student");
                $data['comments'][$i]['replay']=$this->Main_model->get_rec("comment","replay_id",$comment['id']);
                $i++;
            }
        }
        $data['total_comment']=$this->Main_model->row_count('comment','bid',$id);
        $blog_cond = array(
            'id !='=>$id,
            'cid'=>$data['candidate']['id'],
            'status'=>1
        );
        $data['other_blogs'] = $this->Main_model->multitask("","blogs",$blog_cond,"created_at DESC",array(),5);
        $this->load->view("user/blogs_details",$data);
    }
}
?>