<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Election extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
        if (!$this->session->userdata('user_id')) {
            redirect('user/Login');
        }
    }

    public function fn_vote_list($eid)
    {
        $data = array();
        $data['user_info'] = $this->Main_model->select_record($this->session->userdata('user_id'), "student");
        $election = $this->Main_model->select_record($eid, "election");
        if ($this->input->post('btn_vote')) {
            if(((strtotime($election['edate']) + ($election['hours'] * 60 * 60)) - strtotime(date('Y-m-d H:i:s'))) <= 0) {
                echo "<script>alert('Sorry Election Time Is Over... :(');</script>";
            } else {
                $cid = $this->input->post('cand_id');
                $sid = $data['user_info']['id'];

                $vote_exists_qry = "select c.position from vote v,candidate c where v.eid=$eid and v.cid=c.id and v.sid=$sid";
                $vote_exists = $this->Main_model->my_query($vote_exists_qry);
                $flag = true;
                if (!@$vote_exists['blank']) {
                    foreach ($vote_exists as $v) {
                        if (@$v['position'] == $_REQUEST['cand_position']) {
                            echo "<script>alert('You Already Given Vote.');</script>";
                            $flag = false;
                        }
                    }
                }
                if ($flag) {
                    $arr = array(
                        'eid' => $eid,
                        'cid' => $cid,
                        'sid' => $sid
                    );
                    $this->Main_model->insert_rec($arr, 'vote');
                    echo "<script>alert('Thanks For Giving Vote... :)');</script>";
                }
            }
        }
        $data['is_valid_for_clg']=true;
        if($election['dept_no']==0){
            $data['is_valid_for_clg']=false;
            $position = $this->Main_model->select_record($data['user_info']['position'],"position_chart");
            if(strpos($position['position_name'],'lass') !== false){ 
                $data['is_valid_for_clg']=true;
            }
        }
        $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid and ((s.dept_no=" . $data['user_info']['dept_no'] . " and s.year=" . $data['user_info']['year'] . " and s.division=" . $data['user_info']['division'] . ") or p.position_name like '%College%') order by c.position DESC";
        $data['candidate'] = $this->Main_model->my_query($candidate_sql);
        $this->load->view("user/vote_list", $data);
    }

    public function fn_result($eid)
    {
        $data = array();
        $data['user_info'] = $this->Main_model->select_record($this->session->userdata('user_id'), "student");
        $data['election'] = $this->Main_model->select_record($eid, "election");

        if($data['election']['dept_no']!=0){
            $cal_vote_sql = "select v.*,s.id as stud_id,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=$eid and (s.dept_no=" . $data['user_info']['dept_no'] . " and s.year=" . $data['user_info']['year'] . " and s.division=" . $data['user_info']['division'] . ") group by v.cid order by total_vote desc";
        }else{
            $cal_vote_sql = "select v.eid,v.cid,s.id as stud_id,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=$eid group by v.cid order by total_vote desc";
        }
        $data['cal_vote'] = $this->Main_model->my_query($cal_vote_sql);

        $data['winner'] = array();
        if (!@$data['cal_vote']['blank']) {
            $k = 0;
            $prev_pos = "";
            for ($i = 0; $i < count($data['cal_vote']); $i++) {
                $data['cal_vote'][$i]['position_name'];
                if ($k == 0 || $data['cal_vote'][$i]['position_name'] != $prev_pos) {
                    $data['winner'][$k] = $data['cal_vote'][$i];
                    $prev_pos = $data['cal_vote'][$i]['position_name'];
                    // $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                    $k++;
                    if ($k == 2)
                        break;
                }
            }
        }
        if($data['election']['dept_no']==0){
            // update position of not winned candidate
            $win_list = array();
            $win_list = array_map(function($index){
                return $index['cid'];
            },$data['winner']);
            if(empty($win_list)){
                $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid order by s.dept_no,s.year,s.division,c.position DESC";
            }else{
                $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid and c.id NOT IN (".implode(",",$win_list).") order by s.dept_no,s.year,s.division,c.position DESC";
            }
            $not_win = $this->Main_model->my_query($not_win_sql);
            if(!@$not_win['blank']){
                $i=0;
                foreach($not_win as $n){
                    $this->Main_model->update_rec(array('position'=>$not_win[$i]['current_position']),$not_win[$i]['cand_id'],"candidate");
                    $i++;
                }
            }
        }

        $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=$eid order by s.dept_no,s.year,s.division,c.position DESC";
        $data['candidate'] = $this->Main_model->my_query($candidate_sql);
        if(!@$data['candidate']['blank']){ // order by vote
            $cal_vote_arr = array();
            if(!@$data['cal_vote']['blank']){
                $cal_vote_arr = array_map(function ($w) {
                    return $w['cid'];
                }, $data['cal_vote']);
            }
            $i=0;
            foreach ($data['candidate'] as $cand) {
                if (in_array($cand['cand_id'], $cal_vote_arr) == 1) {
                    $k = array_search($cand['cand_id'], $cal_vote_arr);
                    $data['candidate'][$i]['total_vote'] = $data['cal_vote'][$k]['total_vote'];
                } else {
                    $data['candidate'][$i]['total_vote'] = 0;
                }
                $i++;
            }
            usort($data['candidate'], function($a, $b) {
                return $a['total_vote'] <=> $b['total_vote'];
            });
            $data['candidate'] = array_reverse($data['candidate']);
        }
        //result pdf generator
        $data['result_pdf'] = $this->pdf_code($data);
        $this->load->view('user/election_result', $data);
    }

    public function pdf_code($data=array()){
        if($data['election']['dept_no']!=0){
            $dept=$this->Main_model->select_record($data['election']['dept_no'],"department");
            $dept_name = $dept['dept_name'];
        }else
            $dept_name = "College";

        $edt = $data['election']['edate'];
		$edt = str_replace(":","_",$edt);
		$edt = str_replace(" ","_",$edt);
        $fname = $_SERVER['DOCUMENT_ROOT'] .'/election/assets/result_pdf/'.$edt.'_'.$dept_name.'.pdf';
        
        // $fname = dirname(__DIR__).'/../../assets/result_pdf/'.$data['election']['edate'].'_'.$dept_name.'.pdf';
        if(!is_file($fname)){
            if($data['election']['dept_no']!=0){
                $cal_vote_sql = "select v.*,s.id as stud_id,s.year,s.dept_no,s.division,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=".$data['election']['id']." group by v.cid order by s.dept_no,s.year,s.division,total_vote desc";
                $data['cal_vote'] = $this->Main_model->my_query($cal_vote_sql);
                if (!@$data['cal_vote']['blank']) {
                    $k = 0;
                    $prev_pos = "";
                    $prev_dept = 0;
                    $prev_year = 0;
                    $prev_div = 0;
                    $change_flag = false;
                    $data['winner'] = array();
                    for ($i = 0; $i < count($data['cal_vote']); $i++) {
                        $data['cal_vote'][$i]['position_name'];
                        if($k == 0 || ($prev_dept != $data['cal_vote'][$i]['dept_no'] || $prev_year != $data['cal_vote'][$i]['year'] || $prev_div != $data['cal_vote'][$i]['division']) ){
                            $prev_dept = $data['cal_vote'][$i]['dept_no'];
                            $prev_year = $data['cal_vote'][$i]['year'];
                            $prev_div = $data['cal_vote'][$i]['division'];
                            $change_flag = true;
                        }
                        if ($change_flag || ($data['cal_vote'][$i]['position_name'] != $prev_pos && $k%2 != 0)){
                            $change_flag = false;
                            $data['winner'][$k] = $data['cal_vote'][$i];
                            $prev_pos = $data['cal_vote'][$i]['position_name'];
                            $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                            $k++;
                        }
                    }
                }
            }else{
                $cal_vote_sql = "select v.*,s.id as stud_id,p.id as position_id,p.position_name,count(*) as total_vote from vote v,student s,candidate c,position_chart p where v.cid=c.id and c.position=p.id and c.stud_id=s.id and v.eid=".$data['election']['id']." group by v.cid order by total_vote desc";
                $data['cal_vote'] = $this->Main_model->my_query($cal_vote_sql);
                if (!@$data['cal_vote']['blank']) {
                    $k = 0;
                    $prev_pos = "";
                    $data['winner'] = array();
                    for ($i = 0; $i < count($data['cal_vote']); $i++) {
                        $data['cal_vote'][$i]['position_name'];
                        if ($k == 0 || $data['cal_vote'][$i]['position_name'] != $prev_pos) {
                            $data['winner'][$k] = $data['cal_vote'][$i];
                            $prev_pos = $data['cal_vote'][$i]['position_name'];
                            $this->Main_model->update_rec(array('position' => $data['cal_vote'][$i]['position_id']), $data['cal_vote'][$i]['stud_id'], "student");
                            $k++;
                            if ($k == 2)
                                break;
                        }
                    }
                }
            }
            
            if($data['election']['dept_no']==0){
                // update position of not winned candidate
                $win_list = array_map(function($index){
                    return $index['cid'];
                },$data['winner']);
                if(empty($win_list)){
                    $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=".$data['election']['id']." order by s.dept_no,s.year,s.division,c.position DESC";
                }else{
                    $not_win_sql = "select s.position as 'current_position',c.id as 'cand_id',c.position as 'requested_position' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=".$data['election']['id']." and c.id NOT IN (".implode(",",$win_list).") order by s.dept_no,s.year,s.division,c.position DESC";
                }
                $not_win = $this->Main_model->my_query($not_win_sql);
                if(!@$not_win['blank']){
                    $i=0;
                    foreach($not_win as $n){
                        $this->Main_model->update_rec(array('position'=>$not_win[$i]['current_position']),$not_win[$i]['cand_id'],"candidate");
                        $i++;
                    }
                }
            }

            $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and c.eid=".$data['election']['id']." order by s.dept_no,s.year,s.division,c.position DESC";
            $data['candidate'] = $this->Main_model->my_query($candidate_sql);
            
            if(!@$data['candidate']['blank']){ // order by vote
                if(!@$data['cal_vote']['blank']){
                    $cal_vote_arr = array_map(function ($w) {
                        return $w['cid'];
                    }, $data['cal_vote']);
                }
                $i=0;
                foreach ($data['candidate'] as $cand) {
                    if (in_array($cand['cand_id'], $cal_vote_arr) == 1) {
                        $k = array_search($cand['cand_id'], $cal_vote_arr);
                        $data['candidate'][$i]['total_vote'] = $data['cal_vote'][$k]['total_vote'];
                    } else {
                        $data['candidate'][$i]['total_vote'] = 0;
                    }
                    $i++;
                }
                usort($data['candidate'], function($a, $b) {
                    return $a['total_vote'] <=> $b['total_vote'];
                });
                $data['candidate'] = array_reverse($data['candidate']);
            }
            
            $clg_info=$this->Main_model->select_record(1,"clg_info");
            ob_start();
            $this->load->library('Pdf');
            $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
            $pdf->SetTitle('Election Result '.$data['election']['edate'].'_'.$dept_name);
            $pdf->SetHeaderData('assets/photos/'.$clg_info['clg_logo'],
                                20, // logo height
                                $clg_info['clg_name'],
                                $clg_info['website']."\nResult Of ".$data['election']['name'],
                                array(0,64,255), array(0,64,128));
            // $pdf->setFooterData(array(0,64,0), array(0,64,128));

            // set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetTopMargin(30);
            $pdf->SetAutoPageBreak(true);
            $pdf->SetAuthor('Janak');
            $pdf->SetDisplayMode('real', 'default');
            $pdf->AddPage();
            $pdf->writeHTML($this->load->view('user/result_pdf',$data,TRUE), true, false, true, false, '');
            ob_end_clean();
            $pdf->Output($fname, 'F'); // I for open in browser, F for save in folder
        }
        return $edt.'_'.$dept_name.'.pdf';
    }
}
?>