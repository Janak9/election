<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidate extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library("form_validation");
        if(!$this->session->userdata('user_id')){
			redirect('user/Login');	
		}
    }

	public function fn_candidate_list(){
        $data = array();
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $candidate_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and c.position=p.id and c.status>0 and (s.dept_no=".$data['user_info']['dept_no']." or p.position_name like '%College%')";
        $data['candidate']=$this->Main_model->my_query($candidate_sql);
        $this->load->view("user/candidate",$data);
    }

    public function fn_details($id){
        $data = array();
        $data['user_info']=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $data['candidate']=$this->Main_model->select_record($id,"candidate");
        $data['blogs'] = $this->Main_model->get_rec("blogs","cid",$data['candidate']['id']);
        $data['student']=$this->Main_model->select_record($data['candidate']['stud_id'],"student");
        
        if($data['user_info']['id'] != $data['student']['id']){
            $data['chat']="active";
            $data['receiver_id']=$data['student']['id'];
            $data['receiver_name']=$data['student']['name'];
        }
        $this->load->view("user/candidate_details",$data);
    }

    public function fn_edit_details(){
        $data = array();
        if($this->input->post('btn_submit')){
            $fb=$this->input->post('fb_link');
            $g=$this->input->post('g_link');
            $t=$this->input->post('t_link');
            $i=$this->input->post('i_link');
            $about=$this->input->post('about');
            $arr=array(
                'fb_link'=>$fb,
                'g_link'=>$g,
                't_link'=>$t,
                'i_link'=>$i,
                'about'=>$about
            );
            $this->Main_model->update_rec($arr,"","candidate",array("stud_id"=>$this->session->userdata('user_id')));
            echo "<script>alert('Data successfull saved.');</script>";
        }
        $data['data'] = $this->Main_model->get_rec("candidate","","",array("stud_id"=>$this->session->userdata('user_id')));
        $data['data'] = $data['data'][0];
        $this->load->view("user/edit_candidate_details",$data);
    }

    public function fn_add_blogs($id = '')
	{
		$data = array();
		if (!empty($id)) {
			$data['data'] = $this->Main_model->select_record($id, "blogs");
		}
		if ($this->input->post('btn_submit')) {
            $candidate=$this->Main_model->get_rec("candidate","stud_id",$this->session->userdata('user_id'));
            $election = $this->Main_model->select_record($candidate[0]['eid'], "election");
            if ((strtotime($election['ads_end_date']) - strtotime(date('Y-m-d H:i:s'))) <= 0) {
               echo "<script>alert('Sorry Election Advertisment Time Is Over... :(');</script>";
                return $this->load->view('user/add_blogs', $data);
            }            
            $this->form_validation->set_rules('title', 'Title', 'required',array('required'=>'%s field is required'));
            $this->form_validation->set_rules('description', 'Description', 'required',array('required'=>'%s field is required'));
            if ($this->form_validation->run() == true) {
                $title=$this->input->post('title');
                $desc=$this->input->post('description');
                $status=$this->input->post('status');
				$arr=array('cid'=>$candidate[0]['id'],
                           'title'=>$title,
                           'description'=>$desc,
						   'status'=>$status
						   );
                $config['upload_path']="assets/photos/blogs/";
                $config['allowed_types']="gif|jpg|png|jpeg";
                $this->load->library("upload",$config);
                if($this->upload->do_upload('img_file')){
                    if(!empty($id)){
                        $rec=$this->Main_model->select_record($id,"blogs");
                        if(@$rec['image'] !="" && is_file("assets/photos/blogs/".@$rec['image'])){
                            unlink("assets/photos/blogs/".$rec['image']);
                        }
                    }
                    $file=array("up_img"=>$this->upload->data());
                    $arr['image']=$file['up_img']['file_name'];
                }
				//else{
				// 	$data['error']=$this->upload->display_errors();
				// }
                if(empty($id))
                    $this->Main_model->insert_rec($arr,"blogs");
                else
                    $this->Main_model->update_rec($arr, $id, "blogs");
                redirect("user/Candidate/fn_view_blogs");	
			}//if enter data is proper validation
        }
        $this->load->view('user/add_blogs', $data);
	}

	public function fn_view_blogs(){
        $data = array();
        $data['candidate']=$this->Main_model->get_rec("candidate","stud_id",$this->session->userdata('user_id'));
		$data['data'] = $this->Main_model->get_rec("blogs","cid",$data['candidate'][0]['id']);
		$this->load->view('user/view_blogs', $data);
    }
    
	public function fn_delete_blogs($id = '')
	{
        $rec = $this->Main_model->select_record($id, "blogs");
        if(@$rec['image'] !="" && is_file("assets/photos/blogs/".@$rec['image'])){
            unlink("assets/photos/blogs/".$rec['image']);
        }
		$this->Main_model->delete_rec($id, "blogs");
		redirect("user/Candidate/fn_view_blogs");
    }
    
	public function fn_manage_like($task=0){
        $tbl = $_REQUEST['tbl'];
        $id = $_REQUEST['id'];
        $user_info=$this->Main_model->select_record($this->session->userdata('user_id'),"student");
        $arr=array(
            'tbl'=>$tbl,
            'tbl_id'=>$id,
            'uid'=>$user_info['id']
        );
        if($task==0){
            $this->Main_model->insert_rec($arr,"likes");
        }else{
            $this->Main_model->delete_rec("","likes",$arr);
        }
    }
    
}
?>