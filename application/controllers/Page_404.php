<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_404 extends CI_Controller {
	
	public function index()
	{
		$this->output->set_status_header('404');
        $this->load->view('admin/404.php');
	}
}
?>