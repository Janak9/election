<?php
/*------for encrypt decrypt-------*/
declare(strict_types=1);
function url_base64_encode($str = '')
{
    return strtr(base64_encode($str), '+=/', '.-~');
}
/**
 * Decodes a base64 string that was encoded by url_base64_encode.
 * 
 * @param object $str The base64 string to decode.
 * @return string
 */
function url_base64_decode($str = '')
{
    return base64_decode(strtr($str, '.-~', '+=/'));
}

/**
 * Encrypt a message
 * 
 * @param string $message - message to encrypt
 * @param string $key - encryption key
 * @return string
 * @throws RangeException
 */
function safeEncrypt(string $message, string $key): string
{
    if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
        throw new RangeException('Key is not the correct size (must be 32 bytes).');
    }
    $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

    $cipher = base64_encode(
        $nonce.
        sodium_crypto_secretbox(
            $message,
            $nonce,
            $key
        )
    );
    sodium_memzero($message);
    sodium_memzero($key);
    return url_base64_encode($cipher);
}
/**
 * Decrypt a message
 * 
 * @param string $encrypted - message encrypted with safeEncrypt()
 * @param string $key - encryption key
 * @return string
 * @throws Exception
 */
function safeDecrypt(string $encrypted, string $key)
{   
    $decoded = base64_decode(url_base64_decode($encrypted));
    $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
    $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

    $plain = sodium_crypto_secretbox_open(
        $ciphertext,
        $nonce,
        $key
	);
    if (!is_string($plain)) {
        throw new Exception('Invalid MAC');
    }   
    sodium_memzero($ciphertext);
    sodium_memzero($key);
    return $plain;
}


function pre($value='')
{
	echo '<pre>';
	print_r($value);
	echo '</pre>';	
}
function mail_send($email='',$sub='',$msg='')
{
	$CI =& get_instance(); //CI is a use same as this and &get_instance is use for access this keyword
	$CI->load->library('email');
	$config['protocol'] = 'smtp';
	$config['smtp_host'] = 'smtp.gmail.com';
	$config['smtp_user'] = 'vaghelajanak97145@gmail.com';
	$config['smtp_pass'] = '1234654321';
	$config['smtp_crypto'] = 'ssl';
    $config['smtp_port'] = 465;
    $config['mailtype'] = 'html';
	$config['newline'] = "\r\n";
	$CI->email->initialize($config);
    $CI->email->from('vaghelajanak97145@gmail.com','Polite');
    $CI->email->to($email);
	$CI->email->subject($sub);
    $CI->email->message($msg);
    if($CI->email->send()){
        return true;
    }else{
        return false;
    }
}

function pagination($total=0,$per_page=15,$base_url='')
{
   $CI =& get_instance();
   $CI->load->library('pagination');
   $config['total_rows'] = $total;
   $config['per_page'] = $per_page;
   $config['base_url'] = $base_url;
   $config['full_tag_open'] = '<ul class="pagination pagination-sm m-t-none m-b-none">';
   $config['full_tag_close'] = '</ul>';
   $config['num_tag_open'] = '<li>';
   $config['num_tag_close'] = '</li>';
   $config['cur_tag_open'] = '<li class="active"><a>';
   $config['cur_tag_close'] = '</a></li>';
   $config['first_tag_open'] = '<li>';
   $config['first_tag_close'] = '</li>';
   $config['last_tag_open'] = '<li>';
   $config['last_tag_close'] = '</li>';
   $config['prev_tag_open'] = '<li>';
   $config['prev_tag_close'] = '</li>';
   $config['next_tag_open'] = '<li>';
   $config['next_tag_close'] = '</li>';
   $config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
   $config['next_link'] = '<i class="fa fa-chevron-right"></i>';
   $config['first_link'] = '<span aria-hidden="true">&laquo;</span>';
   $config['last_link'] = '<span aria-hidden="true">&raquo;</span>';
   $config['num_links'] = 5;
   $CI->pagination->initialize($config);
   return $CI->pagination->create_links();
}
?>
