<?php $this->load->view("user/header.php")?>
<section class="slider-main-area">
    <div class="main-slider an-si">
        <div class="bend niceties preview-2">
            <div id="ensign-nivoslider-2" class="slides">
                <?php
                $i=1;
                foreach($slider as $row){
                ?>
                <img src="<?php echo base_url();?>assets/photos/slider/<?php echo $row['image']; ?>" alt="" title="#slider-direction-<?php echo $i; ?>"  />
                <?php $i++; } ?>
            </div>
            <?php
            $i=1;
            foreach($slider as $row){
            ?>
            <!-- direction 1 -->
            <div id="slider-direction-<?php echo $i; ?>" class="t-cn slider-direction Builder">
                <div class="container">
                    <div class="slide-all">
                        <!-- layer 1 -->
                        <div class="layer-1">
                            <h3 class="title5"><?php echo $row['title']; ?></h3>
                        </div>
                        <!-- layer 2 -->
                        <div class="layer-2">
                            <h1 class="title6"><?php echo $row['description']; ?></h1>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>
    </div>
</section>
<section class="what-area section-margin ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center pb-30">
                <div class="what-top">
                    <div class="section-title">
                        <h1>What We Do</h1>
                        <div class="what-icon">
                            <i class="fa fa-bookmark" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p>Our mission is to create a society in which an informed and active citizenry is sovereign and makes policy decisions based on the will of the majority.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-crosshairs" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Mission</h3>
                        <p>To create a society in which an informed and active citizenry is sovereign and makes policy decisions based on the will.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all res-3">
                    <div class="offer-icon">
                        <i class="fa fa-volume-up" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Campaings</h3>
                        <p>We oppose discrimination based on race, color, spiritual belief, gender, sexual orientation, age, physical ability, and origin.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Election</h3>
                        <p>We believe that our mission calls for non-traditional approaches, and we call for every citizen to participate in upcoming elections.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row service-mrg">
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-street-view" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Join Volunteers</h3>
                        <p>Volunteers is the backbone of our movement. Find out more about volunteering with our party and opportunities available.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all res-3">
                    <div class="offer-icon">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Projects</h3>
                        <p>We will build democracy through activism and education and change the law to ensure that political processes.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Communities</h3>
                        <p>A sound and healthy state is one in which government recognizes and protects the independent responsibilities that belong.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="event-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center pb-30">
                <div class="what-top">
                    <div class="section-title">
                        <h1>Elections</h1>
                        <div class="what-iczon">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p class="up">We are pleased to announce the following Elections our movement’s representatives are going to participate in.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="">
            <?php
            if(!@$election || @$election['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                foreach($election as $ele){
                if($ele['dept_no']==$user_info['dept_no'] || $ele['dept_no']==0){
            ?>
                <div class="two-hover">
                    <div class="col-md-12 col-sm-12 col-xs-12 election_div"  style="margin-top:10px;">
                        <div class="event-text">
                            <div class="event-time" style="color:red;">
                                <center><h1>
                                        <i class="fa fa-clock-o"></i>
                                        <span id="ele_timer<?php echo $ele['id'];?>" value="<?php echo $ele['edate'];?>" ><script>start_timer("ele_timer<?php echo $ele['id'];?>",<?php echo $ele['hours'];?>)</script></span>
                                    </h1>
                                </center>
                            </div>
                            <h3><a href="<?php echo site_url("user/Candidate/fn_candidate_list");?>"><?php echo $ele['name'];?></a></h3>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                            Election Hour : <?php echo $ele['hours'];?></p>
                            <p><?php echo $ele['description'];?></p>
                            <div class="event-month">
                                <span class="published2">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    Elecation Date : <?php echo $ele['edate'];?>
                                </span>
                                <span class="published2 comment-left">
                                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                                    <?php echo $ele['level'];?> Level
                                </span>
                                <div style="float:right;">
                                    <span class="published2">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        Registration End Date : <?php echo $ele['registration_end_date'];?>
                                    </span>
                                    <span class="published2 comment-left">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        Advertisment End Date : <?php echo $ele['ads_end_date'];?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $position = $this->Main_model->select_record($user_info['position'],"position_chart");
                    $curr_cand=$this->Main_model->get_rec("candidate","stud_id",$user_info['id']);
                    if(!@$curr_cand['blank']){
                        $requested_position=$this->Main_model->select_record($curr_cand[0]['position'],"position_chart");
                        if($ele['level']=="college" && strpos($position['position_name'],'lass') !== false && ($requested_position['position_no'] <=> $position['position_no']) <=0 && ((strtotime($ele['edate']) - strtotime(date('Y-m-d H:i:s'))) > 0)){ 
                            $new_position = $this->Main_model->like_rec("position_chart",'position_name','ollege',"both","",array("status"=>1));
                        ?>
                            <center><form action="" method="post" onsubmit="return confirmation()">
                                <input type="hidden" name="eid" value="<?php echo $ele['id'];?>">
                                <button type="submit" class="btn btn-primary" name="apply_for_college_lvl" value="<?php if(($new_position[0]['position_no'] <=> $new_position[1]['position_no']) > 0){ echo $new_position[0]['id'];}else{ echo $new_position[1]['id'];}?>">Apply For College GS</button>

                                <button type="submit" class="btn btn-primary" name="apply_for_college_lvl" value="<?php  if(($new_position[0]['position_no'] <=> $new_position[1]['position_no']) < 0){ echo $new_position[0]['id'];}else{ echo $new_position[1]['id'];}?>" style = "margin-left:20px;">Apply For College LR</button>
                            </form></center>
                    <?php } } ?>
                    <h4><a class="result_link">View Result</a></h4>
                </div>
                <?php } } }?>
            </div>
        </div>
    </div>
</section>
<section class="timeline-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="what-top line">
                    <div class="section-title">
                        <h1>Political Timeline</h1>
                        <div class="what-icon">
                            <i class="fa fa-line-chart" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p class="up">Explore some of the key moments and events that have helped shape the course of New Zealand's political and constitutional development.</p>
                </div>
            </div>
            <div class="timeline-wraper fix">
                <?php 
                 if(!@$election || @$election['blank']){
                    echo "<center><h3>Upcomming...</h3></center>";
                }else{
                    $i=0;
                    foreach($election as $ele){
                        $i++;
                ?>
                <div class="sin-timeline col-md-6 col-sm-6 text-center <?php if($i%2){echo 'res-sin';} ?>">
                    <span class="timeline-date"><?php echo $ele['edate'];?></span>
                    <div class="timeline-content">
                        <p class="timeline-img"><img src="<?php echo base_url();?>assets/user/img/time-line/<?php echo $i%3+1;?>.jpg" alt=""></p>
                        <div class="timeline-text">
                            <h3><?php echo $ele['name'];?></h3>
                            <p><?php echo $ele['level'];?> Level <?php if($ele['dept_no']!=0){ $dept = $this->Main_model->select_record($ele['dept_no'],"department"); echo " For ".$dept['dept_name']; }?></p>
                            <p><?php echo $ele['description'];?></p>
                        </div>
                    </div>
                </div>
                <?php } } ?>
            </div>
        </div>
    </div>
</section>
<section class="meet-area pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="what-top pb-30">
                    <div class="section-title">
                        <h1>Meet Our Candidates</h1>
                        <div class="what-icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p class="up">Meet our Candidates, winning messaging and professional execution for political and issue-based campaigns.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="team-slider slider-active indicator-style">
            <?php
            if(!@$candidate || @$candidate['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                foreach($candidate as $cand){
            ?>
                <div class="col-md-3 text-center">
                    <div class="meet-all">
                        <div class="meet-img">
                            <a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><img src="<?php echo base_url();?>assets/photos/students/<?php echo @$cand['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" alt=""></a>
                            <div class="meet-icon-all">
                                <div class="meet-icon">
                                    <ul>
                                        <?php 
                                        $arr=array(
                                            'tbl'=>'candidate',
                                            'tbl_id'=>@$cand['cand_id'],
                                            'uid'=>$user_info['id']
                                        );
                                        $cls_active = "";
                                        if($this->Main_model->row_count("likes","","",$arr) > 0){
                                            $cls_active = "active";
                                        } ?>
                                        <li class="<?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$cand['cand_id']; ?>)">
                                            <i class="fa fa-heart"></i>
                                            <span class="total_like">
                                                <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><?php echo @$cand['name']; ?></a></h3>
                            <p>Current Position : <?php $curr_pos = $this->Main_model->select_record(@$cand['position'],'position_chart'); echo @$curr_pos['position_name']; ?></p>
                            <?php
                            $new_pos =$this->Main_model->select_record(@$cand['requested_position'],'position_chart');
                            if($curr_pos['position_name']!=$new_pos['position_name']){
                            ?>
                            <p>Requested Position : <?php echo @$new_pos['position_name']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } }?>
            </div>
        </div>
    </div>
</section>
<section class="campaign-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center pb-30">
                <div class="what-top meet campaign">
                    <div class="section-title">
                        <h1>Latest Advertisments</h1>
                        <div class="what-icon">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p class="up">The latest news and headlines from the world of politics. Get news and in-depth coverage with advertisments and photos.</p>
                </div>
            </div>
        </div>
        <div class="row">
        <?php
        if(!@$blogs || @$blogs['blank']){
            echo "<center><h3>Upcomming...</h3></center>";
        }else{
            foreach($blogs as $blog){
        ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="blog-container-inner">
                    <div class="post-thumb">
                    <a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><img class="attachment-blog-list" src="<?php echo base_url('assets/photos/blogs/'.$blog['image']);?>" alt=""></a>
                    </div>
                    <div class="visual-inner">
                        <div class="home-2-blog">
                            <h3 class="blog-title">
                                <a style="float:right;" href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><?php echo $blog['title'];?></a>
                                <div class="blog_list_round_pro_pic_thumb"><a href="<?php echo site_url('user/Candidate/fn_details/'.$blog['cid']); ?>"><img class="attachment-blog-list" src="<?php echo base_url('assets/photos/students/'.$blog['cand_pic']);?>" ></a></div>
                                <div class="blog_edit bloger_name" style="float:left;margin: 3px 0px 0px 7px;"><a href="<?php echo site_url('user/Candidate/fn_details/'.$blog['cid']); ?>"><?php echo $blog['cand_name']; ?></a></div>
                            </h3>
                            <div class="blog-content" style="height: 100px;overflow: hidden;padding: 10px;"> 
                                <p><?php echo $blog['description'];?></p> 
                            </div>
                        </div>
                        <div class="blog-meta">
                            <span class="published3">
                                <i class="fa fa-calendar" aria-hidden="true"></i><?php echo date("d M, Y",strtotime($blog['created_at']));?>
                            </span>
                            <?php 
                            $arr=array(
                                'tbl'=>'blogs',
                                'tbl_id'=>@$blog['id'],
                                'uid'=>$user_info['id']
                            );
                            $cls_active = "";
                            if($this->Main_model->row_count("likes","","",$arr) > 0){
                                $cls_active = "active";
                            } ?>
                            <span class="published2 list_heart <?php echo @$cls_active;?>" onclick="manage_like(this,'blogs',<?php echo @$blog['id']; ?>)">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                <span class="total_like">
                                    <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                </span>
                            </span>
                            <span class="published4"><i class="fa fa-comments-o" aria-hidden="true"></i><a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"> <?php echo $this->Main_model->row_count('comment','bid',$blog['id']);?> Comment </a></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-12 col-sm-12" style="margin-top:30px;">
            <center><a href="<?php echo site_url('user/Blogs/fn_blogs_list'); ?>" class="btn btn-primary">View All Advertisments</a></center>
        </div>
        <?php } ?>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php")?>