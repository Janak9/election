<?php $this->load->view("user/header.php");?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Profile Details</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-details-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content2 tab-content">
                    <div role="tabpanel" class="tab-pane active" id="one">
                        <div class="tab-img2">
                            <form method="post" action="" id="pro_pic_form" enctype="multipart/form-data" role="form">
                                <div class="edit_profile" onclick="edit_img_click()">
                                    <i class="fa fa-edit"></i>
                                </div>    
                                <input type="file" style="display:none;" id="img_file" name="img_file" onchange="loadfile(event,'pro_pic')">
                                <input type="submit" class="btn btn-default" name="btn_submit" value="Save">
                                <img id="pro_pic" src="<?php echo base_url("assets/photos/students/").@$user_info['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" alt="">
                            </form>
                        </div>
                        <script>
                            function edit_img_click(){
                                $("#img_file").trigger('click');
                            }
                        </script>
                        <div class="team-details-all fix">
                            <div class="team-details-top">
                                <div class="team-details-text">
                                    <h1><?php echo @$user_info['name']; ?></h1>
                                    <h3><?php 
                                    $position = $this->Main_model->select_record(@$user_info['position'],"position_chart");
                                    echo $position['position_name']; ?></h3>
                                    <?php if(@$position['position_name']!="student"){ ?>
                                        <p class="stone"></p>
                                    <?php } ?>
                                </div>
                                <?php if(@$position['position_name']!="student"){ ?>
                                <div class="team-icon">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <?php } ?>
                                <div class="event-list">
                                    <ul>
                                        <li><i class="fa fa-map-marker ex" aria-hidden="true"></i><span>Where: </span><?php echo @$user_info['address']; ?></li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="#"><span>Phone: </span> <?php echo @$user_info['mobile_no']; ?></a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email: </span> <?php echo @$user_info['email']; ?></a></li>
                                        <li><i class="fa fa-child" aria-hidden="true"></i><a href="#"><span>DOB: </span> <?php echo @$user_info['dob']; ?></a></li>
                                        <li><i class="fa fa-venus-mars" aria-hidden="true"></i><a href="#"><span>gender: </span> <?php echo @$user_info['gender']; ?></a></li>
                                        <li><i class="fa fa-building" aria-hidden="true"></i><a href="#"><span>Department: </span> <?php $dept=$this->Main_model->select_record(@$user_info['dept_no'],"department"); echo @$dept['dept_name']; ?></a></li>
                                        <li><i class="fa fa-calendar-check-o" aria-hidden="true"></i><a href="#"><span>Year: </span> <?php echo @$user_info['year']; ?></a></li>
                                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="#"><span>Division: </span> <?php echo @$user_info['division']; ?></a></li>
                                        <li><i class="fa fa-calendar" aria-hidden="true"></i><a href="#"><span>Semester: </span> <?php echo @$user_info['semester']; ?></a></li>
                                        <li><i class="fa fa-list-ol" aria-hidden="true"></i><a href="#"><span>Roll No: </span> <?php echo @$user_info['rno']; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>