<?php $this->load->view("user/header.php");
$user_info=$this->User_dashboard_model->get_user_info();
?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Apply For Election</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Apply For Election</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="subscribe-area ptb-80 subscribe-nn-pb" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="subscribe blog-subscribe">
                    <div class="subscribe-text">
                        <h3>Apply Now</h3>
                    </div>
                    <div class="subscribe-input">
                        <form action="" role="form" method="post" class="mc-form" enctype="multipart/form-data">
                            <?php if(@$error){echo '<div class="alert alert-danger">'.@$error.'</div>';}?>
                            <input placeholder="Sreach Proposer By Roll No" id="find_proposer_rno" name="find_proposer_rno" type="text" onkeyup="get_student_card('find_proposer_result','find_proposer_rno','find_proposer_name','find_proposer_detail')">
                            <input placeholder="Sreach Proposer By Name" id="find_proposer_name" name="find_proposer_name" type="text" onkeyup="get_student_card('find_proposer_result','find_proposer_rno','find_proposer_name','find_proposer_detail')"><br><br>
                            <div id="find_proposer_result"></div>
                            <br>
                            <div class="panel panel-default" id="find_proposer_detail"></div>
                            
                            <input placeholder="Sreach Supporter By Roll No" id="find_supporter_rno" name="find_supporter_rno" type="text" onkeyup="get_student_card('find_supporter_result','find_supporter_rno','find_supporter_name','find_supporter_detail')">
                            <input placeholder="Sreach Supporter By Name" id="find_supporter_name" name="find_supporter_name" type="text" onkeyup="get_student_card('find_supporter_result','find_supporter_rno','find_supporter_name','find_supporter_detail')"><br><br>
                            <div id="find_supporter_result"></div>
                            <br>
                            <div class="panel panel-default" id="find_supporter_detail"></div>
                            <center>
                            <div class="form-group">
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <select name="position" class="form-control" style="margin-right: -160px;width: 360px;">
                                            <option value="">Select Position</option>
                                            <?php
                                            if(@$position['blank']){
                                                echo @$position['blank'];
                                            }else{
                                                foreach($position as $row){
                                                    ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php if(@$data && $data['position']==$row['id']){echo " selected";}?>><?php echo $row['position_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div><br><br><br>
                            <b>Document : </b><input name="img_file" type="file" onchange="loadfile(event,'doc_pic')"></center><br>
                            <img src="" style="width:150px;height:200px !important;" id="doc_pic">
                            <br><br>
                            <input class="btn btn-hover btn-default" type="submit" name="btn_submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>