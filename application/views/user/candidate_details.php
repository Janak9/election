<?php $this->load->view("user/header.php");?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Candidate Details</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Candidate Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-details-area pt-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content2 tab-content">
                    <div role="tabpanel" class="tab-pane active" id="one">
                        <div class="tab-img2">
                            <img id="pro_pic" src="<?php echo base_url("assets/photos/students/").@$student['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/admin/avatar.png'">
                            <?php 
                            $arr=array(
                                'tbl'=>'candidate',
                                'tbl_id'=>@$candidate['id'],
                                'uid'=>$user_info['id']
                            );
                            $cls_active = "";
                            if($this->Main_model->row_count("likes","","",$arr) > 0){
                                $cls_active = "active";
                            } ?>
                            <div class="details_hc">
                                <div class="details_heart <?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$candidate['id']; ?>)">
                                    <i class="fa fa-heart"></i>
                                    <span class="total_like">
                                        <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                    </span>
                                </div>
                            </div>
                            <p class="stone"><?php echo $candidate['about'] ?></p>
                        </div>
                        <div class="team-details-all fix">
                            <div class="team-details-top">
                                <div class="team-details-text">
                                    <h1><?php echo @$student['name']; ?></h1>
                                    <h3><?php
                                    $position = $this->Main_model->select_record(@$student['position'],"position_chart");
                                    echo 'Current Position : '.$position['position_name']; ?></h3>
                                    <h3><?php 
                                    $cand_position = $this->Main_model->select_record(@$candidate['position'],"position_chart");
                                    if($position['position_name']!=$cand_position['position_name']){
                                    echo 'Requested Position : '.$cand_position['position_name']; 
                                    } ?></h3>
                                </div>
                                <div class="team-icon">
                                    <ul>
                                        <li><a href="<?php echo @$candidate['fb_link'];?>" target="_blank"><i class="fa fa-facebook"></a></i></a></li>
                                        <li><a href="<?php echo @$candidate['g_link'];?>" target="_blank"><i class="fa fa-google-plus"></a></i></a></li>
                                        <li><a href="<?php echo @$candidate['t_link'];?>" target="_blank"><i class="fa fa-twitter"></a></i></a></li>
                                        <li><a href="<?php echo @$candidate['i_link'];?>" target="_blank"><i class="fa fa-instagram"></a></i></a></li>
                                    </ul>
                                </div>
                                <div class="event-list">
                                    <ul>
                                        <li><i class="fa fa-map-marker ex" aria-hidden="true"></i><span>Where: </span><?php echo @$student['address']; ?></li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="#"><span>Phone: </span> <?php echo @$student['mobile_no']; ?></a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#"><span>Email: </span> <?php echo @$student['email']; ?></a></li>
                                        <li><i class="fa fa-child" aria-hidden="true"></i><a href="#"><span>DOB: </span> <?php echo @$student['dob']; ?></a></li>
                                        <li><i class="fa fa-venus-mars" aria-hidden="true"></i><a href="#"><span>gender: </span> <?php echo @$student['gender']; ?></a></li>
                                        <li><i class="fa fa-building" aria-hidden="true"></i><a href="#"><span>Department: </span> <?php $dept=$this->Main_model->select_record(@$student['dept_no'],"department"); echo @$dept['dept_name']; ?></a></li>
                                        <li><i class="fa fa-calendar-check-o" aria-hidden="true"></i><a href="#"><span>Year: </span> <?php echo @$student['year']; ?></a></li>
                                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="#"><span>Division: </span> <?php echo @$student['division']; ?></a></li>
                                        <li><i class="fa fa-calendar" aria-hidden="true"></i><a href="#"><span>Semester: </span> <?php echo @$student['semester']; ?></a></li>
                                        <li><i class="fa fa-list-ol" aria-hidden="true"></i><a href="#"><span>Roll No: </span> <?php echo @$student['rno']; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-area pb-80 campaign-two">
    <div class="container">
        <div class="row">
        <div class="col-md-12 text-center pb-30">
            <div class="what-top">
                <div class="section-title">
                    <h1>Advertisments</h1>
                    <div class="what-icon">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(!@$blogs || @$blogs['blank']){
            echo "<center><h3>Upcomming...</h3></center>";
        }else{
            foreach($blogs as $blog){
        ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="blog-container-inner">
                    <div class="post-thumb">
                    <a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><img class="attachment-blog-list" src="<?php echo base_url('assets/photos/blogs/'.$blog['image']);?>" alt=""></a>
                    </div>
                    <div class="visual-inner">
                        <div class="home-2-blog">
                            <h3 class="blog-title">
                                <a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><?php echo $blog['title'];?></a>
                                <?php if(@$user_info['id'] == @$student['id']){ ?>
                                <div class="blog_edit"><a href="<?php echo site_url('user/Candidate/fn_add_blogs/'.$blog['id']); ?>"><i class="fa fa-pencil"></i></a></div>
                                <div class="blog_edit"><a href="<?php echo site_url('user/Candidate/fn_delete_blogs/'.$blog['id']); ?>"><i class="fa fa-trash"></i></a></div>
                                <?php } ?>
                            </h3>
                            <div class="blog-content" style="height: 100px;overflow: hidden;padding: 10px;"> 
                                <p><?php echo $blog['description'];?></p> 
                            </div>
                        </div>
                        <div class="blog-meta">
                            <span class="published3">
                                <i class="fa fa-calendar" aria-hidden="true"></i><?php echo date("d M, Y",strtotime($blog['created_at']));?>
                            </span>
                            <?php 
                            $arr=array(
                                'tbl'=>'blogs',
                                'tbl_id'=>@$blog['id'],
                                'uid'=>$user_info['id']
                            );
                            $cls_active = "";
                            if($this->Main_model->row_count("likes","","",$arr) > 0){
                                $cls_active = "active";
                            } ?>
                            <span class="published2 list_heart <?php echo @$cls_active;?>" onclick="manage_like(this,'blogs',<?php echo @$blog['id']; ?>)">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                <span class="total_like">
                                    <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                </span>
                            </span>
                            <span class="published4"><i class="fa fa-comments-o" aria-hidden="true"></i><a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"> <?php echo $this->Main_model->row_count('comment','bid',$blog['id']);?> Comment </a></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } } ?>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>