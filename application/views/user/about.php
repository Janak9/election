<?php $this->load->view("user/header.php"); ?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">About</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="video-area pt-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="what-top">
                    <div class="section-title">
                        <h1>Who We Are</h1>
                        <div class="what-icon">
                            <i class="fa fa-bookmark" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p>We are an organisation that unites people from all political parties, and none, into one effective anti-EU ground campaign, which is working towards winning.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="events-details-ara ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <div class="contact-map">
                    <input type="hidden" name="lat" id="lat" value="<?php echo @$clg_info['lat'];?>">
                    <input type="hidden" name="lng" id="lng" value="<?php echo @$clg_info['lng'];?>">
                    <div id="map"></div>
                </div>
                <div class="events-details-text">
                    <h1><?php echo $clg_info['clg_name']; ?></h1>
                    <p><?php echo $clg_info['about_clg']; ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="event-details-sidebar">
                    <div class="enother-event-details">
                        <div class="event-list">
                            <ul>
                                <li><i class="fa fa-map-marker ex" aria-hidden="true"></i><span>Where: </span><?php echo $clg_info['address']; ?></li>
                                <li><i class="fa fa-ship" aria-hidden="true"></i><span>Principal: </span><?php echo $clg_info['principal']; ?></li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i><span>Phone: </span> <?php echo $clg_info['clg_contact_no']; ?></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i><span>Email: </span> <?php echo $clg_info['email']; ?></li>
                                <li><i class="fa fa-chrome" aria-hidden="true"></i><span>Website: </span><a href="<?php echo $clg_info['website'];?>" target="blank"><?php echo $clg_info['website']; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="event-details-slider">
                        <div class="slider-active6">
                            <img src="<?php echo base_url();?>assets/photos/<?php echo $clg_info['clg_logo']; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="what-area section-margin ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center pb-30">
                <div class="what-top">
                    <div class="section-title">
                        <h1>What We Do</h1>
                        <div class="what-icon">
                            <i class="fa fa-bookmark" aria-hidden="true"></i>
                        </div>
                    </div>
                    <p>Our mission is to create a society in which an informed and active citizenry is sovereign and makes policy decisions based on the will of the majority.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-crosshairs" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Mission</h3>
                        <p>To create a society in which an informed and active citizenry is sovereign and makes policy decisions based on the will.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all res-3">
                    <div class="offer-icon">
                        <i class="fa fa-volume-up" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Campaings</h3>
                        <p>We oppose discrimination based on race, color, spiritual belief, gender, sexual orientation, age, physical ability, and origin.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Election</h3>
                        <p>We believe that our mission calls for non-traditional approaches, and we call for every citizen to participate in upcoming elections.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row service-mrg">
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-street-view" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Join Volunteers</h3>
                        <p>Volunteers is the backbone of our movement. Find out more about volunteering with our party and opportunities available.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-area-all res-3">
                    <div class="offer-icon">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Our Projects</h3>
                        <p>We will build democracy through activism and education and change the law to ensure that political processes.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-sm">
                <div class="service-area-all">
                    <div class="offer-icon">
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <div class="offer-text">
                        <h3>Communities</h3>
                        <p>A sound and healthy state is one in which government recognizes and protects the independent responsibilities that belong.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("user/footer.php"); ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAD_xWz2gBZ4u2eShoeDYb64wzGM62BdLA&libraries=places&callback=initAutocomplete">
</script>