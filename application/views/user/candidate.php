<?php $this->load->view("user/header.php")?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Candidate List</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Candidate List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="team-all">
            <?php
            if(!@$candidate || @$candidate['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                $i=-1;
                foreach($candidate as $cand){
                    $i++;
            ?>
                <div class="col-md-3 text-center" <?php if($i%4==0){ echo "style='clear:both;'"; } ?>>
                    <div class="meet-all">
                        <div class="meet-img">
                            <a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><img src="<?php echo base_url();?>assets/photos/students/<?php echo @$cand['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" alt=""></a>
                            <div class="meet-icon-all">
                                <div class="meet-icon">
                                    <ul>
                                        <?php 
                                        $arr=array(
                                            'tbl'=>'candidate',
                                            'tbl_id'=>@$cand['cand_id'],
                                            'uid'=>$user_info['id']
                                        );
                                        $cls_active = "";
                                        if($this->Main_model->row_count("likes","","",$arr) > 0){
                                            $cls_active = "active";
                                        } ?>
                                        <li class="<?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$cand['cand_id']; ?>)">
                                            <i class="fa fa-heart"></i>
                                            <span class="total_like">
                                                <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><?php echo @$cand['name']; ?></a></h3>
                            <p>Current Position : <?php $curr_pos = $this->Main_model->select_record(@$cand['position'],'position_chart'); echo @$curr_pos['position_name']; ?></p>
                            <?php $new_pos = $this->Main_model->select_record(@$cand['requested_position'],'position_chart');
                            if($curr_pos['position_name']!=$new_pos['position_name']){
                            ?>
                            <p>Requested Position : <?php  echo @$new_pos['position_name']; ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } }?>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php")?>