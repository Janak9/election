<html>
<head>
    <title>Polite</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/election.css">
    <style type="text/css" media="all">
        td {
            padding: 5px;

        }
        table {
            width: 100%;
        }
        .bgcolor {
            background-color: #dbdde0 !important;
        }
        .tcenter {
            text-align: center;
        }
        body {
            -webkit-print-color-adjust: exact;
        }

        @page {
            size: auto;
            /* auto is the initial value */
            margin: 0;
            /* this affects the margin in the printer settings */
        }
        .btnprint {
            margin: 10px;
            padding: 10px;
        }

        table {}

        .border {
            border: 1px solid #a6a7a8;
        }

        .ucase {
            text-transform: uppercase;
        }
    </style>

    <style type="text/css" media="print">
        .btnprint {
            display: none;
        }
    </style>
</head>

<body>
    <table class="tmain">
        <tr>
            <th style="text-align:center;font-size:24px;">Winners</th>
        </tr>
        <tr>
            <td width="100%">
                <?php
                if(@$winner && !@$winner['blank']){
                    foreach (@$winner as $row)
                    { 
                        // pre($row);
                        $stud=$this->Main_model->select_record($row['stud_id'],"student");
                        $dept=$this->Main_model->select_record($stud['dept_no'],"department");
                ?>
                <table style="padding:10px 5px;" border="1">
                    <tr>
                        <td width="20%">
                            <?php 
                                if(@$stud['image']!=''){$imgname = @$stud['image'];}else{ $imgname = 'avatar.png';}
                            ?>
                            <img src="<?php echo base_url('assets/photos/students/'.$imgname); ?>"  onerror="this.src='<?php echo base_url();?>assets/photos/admin/avatar.png'" height="100px;">
                        </td>
                        <td width="50%" style="border-right:0px solid #FFF;">
                            <?php echo "Name : ".@$stud['name'];
                            echo "<br>&nbsp;&nbsp;Position : ".@$row['position_name'];
                            echo "<br>&nbsp;&nbsp;Email : ".@$stud['email'];
                            echo "<br>&nbsp;&nbsp;Mobile : ".@$stud['mobile_no'];?>
                        </td>
                        <td width="30%" style="border-left:0px solid #FFF;"><?php echo "Dept.:".$dept['dept_name'];
                            echo "<br>Year:".$stud['year'];
                            echo "<br>Division:".$stud['division'];
                            echo "<br>Votes : ".@$row['total_vote'];
                            ?></td>
                    </tr>
                </table>
                <?php } } ?>
            </td>
        </tr>
    </table>
    <br><br><br><br>
    <table class="tmain">
        <tr>
            <th style="text-align:center;font-size:24px;">All Over Result</th>
        </tr>
        <tr>
            <td width="100%">
                <?php
                if(@$candidate && !@$candidate['blank']){
                    foreach (@$candidate as $row)
                    { 
                        // pre($row);
                        // $stud=$this->Main_model->select_record($row['stud_id'],"student");
                        $dept=$this->Main_model->select_record($row['dept_no'],"department");
                        $position=$this->Main_model->select_record($row['position'],"position_chart");
                ?>
                    <table style="padding:10px 5px;" border="1">
                        <tr>
                            <td width="20%">
                                <?php 
                                    if(@$row['image']!=''){$imgname = @$row['image'];}else{ $imgname = 'avatar.png';}
                                ?>
                                <img src="<?php echo base_url('assets/photos/students/'.$imgname); ?>"  onerror="this.src='<?php echo base_url();?>assets/photos/admin/avatar.png'" height="100px;">
                            </td>
                            <td width="50%" style="border-right:0px solid #FFF;">
                                <?php echo "Name : ".@$row['name'];
                                echo "<br>&nbsp;&nbsp;Position : ".@$position['position_name'];
                                echo "<br>&nbsp;&nbsp;Email : ".@$row['email'];
                                echo "<br>&nbsp;&nbsp;Mobile : ".@$row['mobile_no'];?>
                            </td>
                            <td width="30%" style="border-left:0px solid #FFF;"><?php echo "Dept.:".$dept['dept_name'];
                                echo "<br>Year:".$row['year'];
                                echo "<br>Division:".$row['division'];
                                echo "<br>Votes : ".@$row['total_vote'];
                                ?></td>
                        </tr>
                    </table>
                <?php } } ?>
            </td>
        </tr>
    </table>
</body>
</html>