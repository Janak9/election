<?php
$user_info = $this->User_dashboard_model->get_user_info();
$is_candidate=false;
if($this->Main_model->row_count('candidate','stud_id',$user_info['id'],array("status > " => 0)) > 0){
    $is_candidate=true;
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Election</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/user/img/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/meanmenu.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/lib/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/lib/css/preview.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/chosen.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/user/css/election.css">
    <script src="<?php echo base_url();?>assets/user/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/user/js/jquery.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/user/js/user.js"></script>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- Add your site or application content here -->
    <div class="wrapper">
        <header class="header-area">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-xs-12">
                            <div class="header-top-info">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            Hotline (+123) 45 67 89 21
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            devitmes@gmail.com
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-xs">
                            <div class="header-social floatright">
                                <ul>
                                    <li><a href="<?php echo site_url('admin'); ?>">Admin</a></li>
                                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a class="instagram" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom stick-h2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="logo floatleft">
                                <a href="<?php echo site_url();?>">
                                    <img alt="" style="height:36px !important;width:145px !important;" src="<?php echo base_url();?>assets/user/img/logo/logo.png">
                                </a>
                            </div>
                            <?php if(!@$_SESSION['user_id']){ ?>
                            <div class="header-search floatright">
                                <div class="header-button floatright">
                                    <a href="<?php echo site_url('user/Login/'); ?>">Login</a>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="main-menu floatright">
                                <nav>
                                    <ul>
                                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                                        <li><a href="<?php echo site_url("user/Home/fn_about"); ?>"> About</a></li>
                                        <li><a href="<?php echo site_url("user/Candidate/fn_candidate_list"); ?>"> Candidates</a></li>
                                        <li><a href="<?php echo site_url("user/Blogs/fn_blogs_list"); ?>"> Advertisments</a></li>
                                        <?php if(@$_SESSION['user_id']){ ?>
                                        <li>
                                            <a href="<?php echo site_url("user/Chat/fn_chat_list"); ?>"> Chat <span class="badge" id="chat_count"></span></a>
                                            <script type="text/javascript">
                                                chat_count();
                                                setInterval(function(){chat_count()},1000);
                                            </script>
                                        </li>
                                        <li><a href="#"> <?php echo @$user_info['name']; ?> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                            <ul class="submenu-mainmenu">
                                                <li>
                                                    <a href="<?php echo site_url("user/Dashboard/fn_profile"); ?>">Profile</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url("user/Dashboard/fn_change_pwd"); ?>">Change Password</a>
                                                </li>
                                                <?php if(!@$is_candidate){?>
                                                <li>
                                                    <a href="<?php echo site_url("user/Dashboard/fn_apply_for_election"); ?>">Apply For Election</a>
                                                </li>
                                                <?php }else{
                                                $candidate = $this->Main_model->get_rec('candidate','stud_id',$user_info['id']);?>
                                                <li>
                                                    <a href="<?php echo site_url("user/Candidate/fn_details/".$candidate[0]['id']); ?>">Profile As Candidate</a>
                                                </li>
                                                
                                                <li>
                                                    <a href="<?php echo site_url("user/Candidate/fn_edit_details"); ?>">Edit Details</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url("user/Candidate/fn_add_blogs"); ?>">Add Advertisments</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url("user/Candidate/fn_view_blogs"); ?>">My Advertisments</a>
                                                </li>
                                                <?php } ?>
                                                <li>
                                                    <a href="<?php echo site_url("user/Login/fn_logout"); ?>">Logout</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-menu-area hidden-lg hidden-md">
            <div class="container">
                <div class="col-md-12">
                    <div class="mobile-menu-area">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul>
                                            <li><a class="menu-icon" href="index.html">Home<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                <ul class="submenu-mainmenu">
                                                    <li>
                                                        <a href="index.html">home version one</a>
                                                    </li>
                                                    <li>
                                                        <a href="index-2.html">home version two</a>
                                                    </li>
                                                    <li>
                                                        <a href="index-3.html">home version three</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="about.html"> About</a></li>
                                            <li><a href="blog.html"> blog <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                <ul class="submenu-mainmenu">
                                                    <li>
                                                        <a href="blog.html">blog</a>
                                                    </li>
                                                    <li>
                                                        <a href="blog-two.html">blog sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="blog-two-details.html">blog details</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="#"> Pages <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                                <ul class="submenu-mainmenu">
                                                    <li>
                                                        <a href="about.html">about us</a>
                                                    </li>
                                                    <li>
                                                        <a href="donate.html">donate</a>
                                                    </li>
                                                    <li>
                                                        <a href="team.html">team</a>
                                                    </li>
                                                    <li>
                                                        <a href="team-details.html">team details</a>
                                                    </li>
                                                    <li>
                                                        <a href="events.html">event</a>
                                                    </li>
                                                    <li>
                                                        <a href="events-details.html">event details</a>
                                                    </li>
                                                    <li>
                                                        <a href="contact.html">contact</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact </a></li>
                                        </ul>
                                    </nav>
                                </div>					
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-area end -->