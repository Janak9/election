<?php $this->load->view("user/header.php")?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Vote List</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Vote List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-area ptb-80">
    <div class="container">
        <center><h4 style="color:red;">Every one can give total 2 vote.<?php if($is_valid_for_clg){echo "Only Class CR/LR Can Give Vote.";} ?></h4></center>
        <div class="row">
            <div class="team-all">
            <?php
            if(!@$candidate || @$candidate['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                $old_position=-1;
                foreach($candidate as $cand){
                    $curr_pos = $this->Main_model->select_record(@$cand['position'],'position_chart');
                    $new_pos = $this->Main_model->select_record(@$cand['requested_position'],'position_chart');
                    if($old_position==-1){
                        echo '<center><h3>'.@$new_pos['position_name'].'</h3></center>';
                    }
                    if($old_position!=-1 && $cand['requested_position']!=$old_position){
                        echo '</div></div><div class="row"><div class="team-all"><center><h3>'.@$new_pos['position_name'].'</h3></center>';
                    }
                    $old_position=$cand['requested_position'];
            ?>
                <div class="col-md-3 text-center mb-30">
                    <div class="meet-all">
                        <div class="meet-img">
                            <a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><img src="<?php echo base_url();?>assets/photos/students/<?php echo @$cand['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" alt=""></a>
                            <div class="meet-icon-all">
                                <div class="meet-icon">
                                    <ul>
                                        <?php 
                                        $arr=array(
                                            'tbl'=>'candidate',
                                            'tbl_id'=>@$cand['cand_id'],
                                            'uid'=>$user_info['id']
                                        );
                                        $cls_active = "";
                                        if($this->Main_model->row_count("likes","","",$arr) > 0){
                                            $cls_active = "active";
                                        } ?>
                                        <li class="<?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$cand['cand_id']; ?>)">
                                            <i class="fa fa-heart"></i>
                                            <span class="total_like">
                                                <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><?php echo @$cand['name']; ?></a></h3>
                            <p>Current Position : <?php echo @$curr_pos['position_name']; ?></p>
                            <?php
                            if($curr_pos['position_name']!=$new_pos['position_name']){
                            ?>
                            <p>Requested Position : <?php echo @$new_pos['position_name']; ?></p>
                            <?php }
                            if($is_valid_for_clg){?>
                            <form action="" method="post" onsubmit="return confirmation()">
                                <input type="hidden" name="cand_id" value="<?php echo @$cand['cand_id']; ?>">
                                <input type="hidden" name="cand_position" value="<?php echo @$cand['requested_position']; ?>">
                                <input type="submit" name="btn_vote" value="Vote" class="btn btn-primary">
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } }?>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php")?>