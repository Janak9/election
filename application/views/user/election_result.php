<?php $this->load->view("user/header.php");?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Result Of <?php echo @$election['name']; ?></h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Result</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team-area pt-80">
    <div class="container">
        <div class="row">
            <center><h2>Winners</h2></center><br>
            <div class="team-all">
            <?php
            if(!@$winner || @$winner['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                foreach($winner as $win){
                    $cand_sql = "select s.*,c.id as 'cand_id',c.proposer_id,c.supporter_id,c.eid,c.position as 'requested_position',c.status as 'candidate_status' from student s,candidate c,position_chart p where c.stud_id=s.id and s.position=p.id and c.id=".$win['cid'];
                    $cand=$this->Main_model->my_query($cand_sql);
                    $cand=$cand[0];
            ?>
                <div class="col-md-3 text-center mb-30">
                    <div class="meet-all">
                        <div class="meet-img">
                            <a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>">
                                <img src="<?php echo base_url();?>assets/photos/students/<?php echo @$cand['image'];?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'">
                                <img class="win_img" src="<?php echo base_url();?>assets/photos/win2.png">
                            </a>
                            <div class="meet-icon-all">
                                <div class="meet-icon">
                                    <ul>
                                        <?php 
                                        $arr=array(
                                            'tbl'=>'candidate',
                                            'tbl_id'=>@$cand['cand_id'],
                                            'uid'=>$user_info['id']
                                        );
                                        $cls_active = "";
                                        if($this->Main_model->row_count("likes","","",$arr) > 0){
                                            $cls_active = "active";
                                        } ?>
                                        <li class="<?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$cand['cand_id']; ?>)">
                                            <i class="fa fa-heart"></i>
                                            <span class="total_like">
                                                <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><?php echo @$cand['name']; ?></a></h3>
                            <p>Current Position : <?php echo @$win['position_name']; ?></p>
                            <p style="color:red;"><strong>Win By <?php echo @$win['total_vote']; ?> Votes</strong></p>
                        </div>
                    </div>
                </div>
            <?php } }?>
            </div>
        </div>
    </div>
</section>
<section class="team-area pb-80">
    <div class="container">
        <div class="row">
            <a target="_blank" href="<?php echo base_url('assets/result_pdf/').$result_pdf;?>"><center><h2>View Over All Result</h2></center><br></a>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php"); ?>