<?php $this->load->view("user/header.php");?>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Advertisment details</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Advertisment details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-two-details-area ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-container-inner">
                    <div class="post-for2">
                        <img src="<?php echo base_url('assets/photos/blogs/'.$blog['image']);?>" alt="">
                    </div>
                    <div class="visual-inner">
                        <h3 class="blog-title">
                            <?php echo $blog['title']; ?>
                        </h3>
                        <div class="blog-meta">
                            <span class="published3">
                                <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo date("d M, Y",strtotime($blog['created_at']));?>
                            </span>
                            <?php 
                            $arr=array(
                                'tbl'=>'blogs',
                                'tbl_id'=>@$blog['id'],
                                'uid'=>$user_info['id']
                            );
                            $cls_active = "";
                            if($this->Main_model->row_count("likes","","",$arr) > 0){
                                $cls_active = "active";
                            } ?>
                            <span class="published2 list_heart <?php echo @$cls_active;?>" onclick="manage_like(this,'blogs',<?php echo @$blog['id']; ?>)">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                <span class="total_like">
                                    <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                </span>
                            </span>
                            <span class="published4"><i class="fa fa-comments-o" aria-hidden="true"></i> <?php echo $total_comment;?> Comment </span>
                        </div>
                        <div class="blog-content"><?php echo $blog['description'];?></div>
                    </div>
                    <div class="blog-tag">
                        <p style="float:left;"><i class="fa fa-tags" aria-hidden="true"></i></p>
                        <div class="round_pro_pic_thumb"><a href="<?php echo site_url("user/Candidate/fn_details/".$candidate['id']); ?>"><img src="<?php echo base_url('assets/photos/students/'.$student['image']);?>"></a></div>
                        <p class="student_name"><strong><a href="<?php echo site_url("user/Candidate/fn_details/".$candidate['id']); ?>"><?php echo $student['name'];?></a></strong></p>
                    </div>
                    <div class="blog-comment">
                        <p><i class="fa fa-comments" aria-hidden="true"></i>Write Comment:</p>
                        <?php if(@$comments && !@$comments['blank']){
                            foreach($comments as $comment){
                        ?>
                        <div class="blog-reply-all2">
                            <div class="blog-top">
                                
                                <img class="comment_pic_thumb" src="<?php echo base_url('assets/photos/students/'.$comment['student']['image']);?>" alt="">
                                <div class="blog-img-details">
                                    <div class="blog-title">
                                        <h3><?php echo $comment['student']['name']; ?></h3>
                                        <a onclick="set_replay(<?php echo $comment['id']; ?>,'<?php echo $comment['student']['name']; ?>')">Reply</a>
                                    </div>
                                    <span><?php echo date("d M, Y g:i a",strtotime($comment['created_at'])); ?></span>
                                    <div class="p-border"><?php echo $comment['comment']; ?></div>
                                </div>
                            </div>
                            <?php
                            if(!@$comment['replay']['blank']){
                                foreach($comment['replay'] as $replay){
                                    $replay_stud = $this->Main_model->select_record($replay['uid'],"student");
                            ?>
                            <div class="blog-top blog-top-mrg">
                                <img class="comment_pic_thumb" src="<?php echo base_url('assets/photos/students/'.$replay_stud['image']);?>" alt="">
                                <div class="blog-img-details">
                                    <div class="blog-title">
                                        <h3><?php echo $replay_stud['name']; ?></h3>
                                        <a onclick="set_replay(<?php echo $comment['id']; ?>,'<?php echo $replay_stud['name']; ?>')">Reply</a>
                                    </div>
                                    <span><?php echo date("d M, Y g:i a",strtotime($replay['created_at'])); ?></span>
                                    <div><?php echo $replay['comment']; ?></div>
                                </div>
                            </div>
                            <?php } }?>
                        </div>
                        <?php } }?>
                    </div>
                    <div class="blog-input">
                        <p><i class="fa fa-reply" aria-hidden="true"></i>Leave A Reply:</p>
                        <form action="" method="post">
                            <div class="row">
                                <div class="blog-from">
                                    <div class="col-md-12">
                                        <input type="hidden" name="replay_id" id="replay_id" value="0">
                                        <textarea id="comment_txt" name="comment_txt" class="form-control" placeholder="Comment" required></textarea>
                                        <script>
                                            CKEDITOR.replace('comment_txt');
                                        </script>
                                        <!-- <input type="submit" name="btn_submit"> -->
                                        <button class="submit" name="btn_submit" value="btn_submit" type="submit">
                                            Post Comment
                                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="issues-categoris issues-top3 blog-two2">
                    <h3>Other Advertisments</h3>
                    <div class="event-sidebar-2">
                        <div class="categori-list1">
                            <?php
                            if(!@$other_blogs || @$other_blogs['blank']){
                                echo "<center><h3>Upcomming...</h3></center>";
                            }else{
                                foreach($other_blogs as $b){
                            ?>
                            <div class="categori-list-one">
                                <div class="categori-list-img">
                                    <a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$b['id']); ?>"><img src="<?php echo base_url('assets/photos/blogs/'.$b['image']);?>"></a>
                                </div>
                                <div class="post-details">
                                    <h3><a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$b['id']); ?>"><?php echo $b['title'];?></a></h3>
                                    <p><?php echo substr($b['description'],0,70);?>...</p>
                                </div>
                            </div>
                            <?php } }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>
<script>
    function set_replay(rid,nm){
        $('#replay_id').val(rid);
        CKEDITOR.instances['comment_txt'].insertHtml('&nbsp;<strong>@'+nm+'</strong>&nbsp;');
        CKEDITOR.instances['comment_txt'].focus();
    }
</script>