<?php $this->load->view("user/header.php");?>
<section class="chat_list">
    <div class="chat_list_pic">
        <img src="<?php echo base_url('assets/photos/students/'.$receiver['image']); ?>" onerror="this.src='<?php echo base_url();?>assets/photos/admin/avatar.png'">
    </div>
    <div class="chat_list_info">
        <center><h1 style="font-size:43px;color:#ea000d;" ><?php echo $receiver['name']; ?></h1></center>
    </div>
</section>
<section class="blog-area campaign-two">
    <div class="row chat_details">
        <div id="messages" class="messages">
        <ul>
            <?php 
            if(!@$chat_res['blank']){
            foreach($chat_res as $m) {
                if($m['s_id']==$s_id){ ?>
                <li>
                    <span class="right"><?php echo @$m['msg']; ?><hr style="margin:4px 0px;"><p style="color:#ccc;"><?php echo @$m['created_at'];?></p></span>
                    <div class="clear"></div>
                </li> 
                <?php  }else{ ?>
                <li>
                    <span class="left"><?php  echo @$m['msg']; ?><hr style="margin:4px 0px;"><p style="color:#ccc;"><?php echo @$m['created_at'];?></p></span>
                    <div class="clear"></div>
                </li> 
            <?php } } }?>
        </ul>
        <div class="clear"></div>
        </div>
        <div class="input-box chat_details_input">
            <input type="hidden" id="s_id" value="<?php echo $s_id;?>">
            <input type="hidden" id="r_id" value="<?php echo $r_id;?>">
            <input type="text" name="mess" id="mess" class="form-control" onKeydown="Javascript: if (event.keyCode==13) send_msg(<?php echo @$s_id;?>,<?php echo @$r_id;?>,this.value);" placeholder="Enter message">
        </div>
        <script type="text/javascript">
        var elem = document.getElementById('messages');
        elem.scrollTop = elem.scrollHeight;
        $(document).ready(function(){
            setInterval(function(){chat_box()},1000);
        });
        </script>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>