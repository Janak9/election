<div class="panel-heading"><?php echo @$type; ?> Details</div>
<div class="panel-body">
    <input id="<?php echo @$type; ?>_id" name="<?php echo @$type; ?>_id" type="hidden" value="<?php echo @$student['id']; ?>">
    Roll No : <input placeholder="Roll No" id="<?php echo @$type; ?>_rno" name="<?php echo @$type; ?>_rno" type="text" value="<?php echo @$student['rno']; ?>" disabled>
    Name : <input placeholder="Name" id="<?php echo @$type; ?>_name" name="<?php echo @$type; ?>_name" type="text" value="<?php echo @$student['name']; ?>" disabled>
    <br><br>
    Department : <input placeholder="Department" id="<?php echo @$type; ?>_dept" name="<?php echo @$type; ?>_dept" type="text" value="<?php $dept=$this->Main_model->select_record($student['dept_no'],'department'); echo @$dept['dept_name']; ?>" disabled>
    Year :<input placeholder="Year" id="<?php echo @$type; ?>_year" name="<?php echo @$type; ?>_year" type="text" value="<?php echo @$student['year']; ?>" disabled>
    <br><br>
    Division : <input placeholder="Division" id="<?php echo @$type; ?>_div" name="<?php echo @$type; ?>_div" type="text" value="<?php echo @$student['division']; ?>" disabled>
    Email : <input placeholder="Email" id="<?php echo @$type; ?>_email" name="<?php echo @$type; ?>_email" type="text" value="<?php echo @$student['email']; ?>" disabled>
    <br><br>
    Mobile No : <input placeholder="Mobile No" id="<?php echo @$type; ?>_phno" name="<?php echo @$type; ?>_phno" type="text" value="<?php echo @$student['mobile_no']; ?>" disabled>
    <img src="<?php echo base_url('assets/photos/students/').$student['image']; ?>" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" style="width:100px;height:100px !important;" >
</div>
