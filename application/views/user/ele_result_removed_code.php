<section class="team-area pb-80">
    <div class="container">
        <div class="row">
            <center><h2>Result</h2></center><br>
            <div class="team-all">
            <?php
            if(!@$candidate || @$candidate['blank']){
                echo "<center><h3>Upcomming...</h3></center>";
            }else{
                foreach($candidate as $cand){
            ?>
                <div class="col-md-3 text-center mb-30">
                    <div class="meet-all">
                        <div class="meet-img">
                            <a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>">
                                <img src="<?php echo base_url();?>assets/photos/students/<?php echo @$cand['image'];?>">
                            </a>
                            <div class="meet-icon-all">
                                <div class="meet-icon">
                                    <ul>
                                        <?php 
                                        $arr=array(
                                            'tbl'=>'candidate',
                                            'tbl_id'=>@$cand['cand_id'],
                                            'uid'=>$user_info['id']
                                        );
                                        $cls_active = "";
                                        if($this->Main_model->row_count("likes","","",$arr) > 0){
                                            $cls_active = "active";
                                        } ?>
                                        <li class="<?php echo @$cls_active;?>" onclick="manage_like(this,'candidate',<?php echo @$cand['cand_id']; ?>)">
                                            <i class="fa fa-heart"></i>
                                            <span class="total_like">
                                                <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <h3><a href="<?php echo site_url("user/Candidate/fn_details/".$cand['cand_id']); ?>"><?php echo @$cand['name']; ?></a></h3>
                            <?php $curr_pos = $this->Main_model->select_record(@$cand['position'],'position_chart');?>
                            <p>Current Position : <?php echo @$curr_pos['position_name']; ?></p>
                            <p style="color:red;"><strong>Total Votes <?php echo $cand['total_vote']; ?></strong></p>
                        </div>
                    </div>
                </div>
            <?php } }?>
            </div>
        </div>
    </div>
</section>
