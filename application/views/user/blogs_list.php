<?php $this->load->view("user/header.php");?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Advertisments</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Advertisments</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-area ptb-80 campaign-two">
    <div class="container">
        <div class="row">
        <?php
        if(!@$data || @$data['blank']){
            echo "<center><h3>Upcomming...</h3></center>";
        }else{
            foreach($data as $blog){
        ?>
            <div class="col-md-4 col-sm-6 col-xs-12 mb-40">
                <div class="blog-container-inner">
                    <div class="post-thumb">
                    <a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><img class="attachment-blog-list" src="<?php echo base_url('assets/photos/blogs/'.$blog['image']);?>" alt=""></a>
                    </div>
                    <div class="visual-inner">
                        <div class="home-2-blog">
                            <h3 class="blog-title">
                                <a style="float:right;" href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"><?php echo $blog['title'];?></a>
                                <div class="blog_list_round_pro_pic_thumb">
                                    <a href="<?php echo site_url('user/Candidate/fn_details/'.$blog['cid']); ?>">
                                        <img class="attachment-blog-list" src="<?php echo base_url('assets/photos/students/'.$blog['cand_pic']);?>" >
                                    </a>
                                </div>
                                <div class="blog_edit bloger_name" style="float:left;margin: 3px 0px 0px 7px;">
                                    <a href="<?php echo site_url('user/Candidate/fn_details/'.$blog['cid']); ?>"><?php echo $blog['cand_name']; ?></a>
                                </div>
                            </h3>
                            <div class="blog-content" style="height: 100px;overflow: hidden;padding: 10px;"> 
                                <p><?php echo $blog['description'];?></p> 
                            </div>
                        </div>
                        <div class="blog-meta">
                            <span class="published3">
                                <i class="fa fa-calendar" aria-hidden="true"></i><?php echo date("d M, Y",strtotime($blog['created_at']));?>
                            </span>
                            <?php 
                            $arr=array(
                                'tbl'=>'blogs',
                                'tbl_id'=>@$blog['id'],
                                'uid'=>$user_info['id']
                            );
                            $cls_active = "";
                            if($this->Main_model->row_count("likes","","",$arr) > 0){
                                $cls_active = "active";
                            } ?>
                            <span class="published2 list_heart <?php echo @$cls_active;?>" onclick="manage_like(this,'blogs',<?php echo @$blog['id']; ?>)">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                <span class="total_like">
                                    <?php unset($arr['uid']); echo $this->Main_model->row_count('likes','','',$arr);?>
                                </span>
                            </span>
                            <span class="published4"><i class="fa fa-comments-o" aria-hidden="true"></i><a href="<?php echo site_url('user/Blogs/fn_blogs_details/'.$blog['id']); ?>"> <?php echo $this->Main_model->row_count('comment','bid',$blog['id']);?> Comment </a></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } } ?>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>