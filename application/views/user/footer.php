            <footer id="footer" class="social-area">
                <div class="footer-middle ptb-80">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 col-sm-6">
                                <div class="middle-text">
                                    <div class="footer-icon">
                                        <a href="<?php echo site_url();?>">
                                            <img style="height:36px !important;width:145px !important;" src="<?php echo base_url();?>assets/user/img/logo/logo2.png" alt="">
                                        </a>
                                    </div>
                                    <div class="middle-mgn">
                                        <p>This is a simple, safe and secure method that takes minimum of time. We proceed our project with the assumption that each voter has a voter ID storing his/her unique identity including data.</p>
                                        <p>Integrity of the results is guaranteed. Preventing the chance of false voting. Generally voting has to be performing by user by going to the voting center</p>
                                        <div class="footer-social">
                                            <ul>
                                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a class="instagram" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-6">
                                <div class="middle-text">
                                    <h3>CONTACT US</h3>
                                    <div class="middle-mgn">
                                        <div class="footer-top-left">
                                            <p><i class="fa fa-paper-plane"></i></p>
                                            <a href="# ">+88 225 88 154 263</a>
                                        </div>
                                        <div class="footer-middle-right">
                                            <p><i class="fa fa-phone"></i></p>
                                            <a href="# ">rayed@email.com </a>
                                        </div>
                                        <div class="footer-bottom-right">
                                            <p><i class="fa fa-envelope" aria-hidden="true"></i></p>
                                            <a href="# ">msnmistiri@email.com </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="footer-bottom-all">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="copyright-text text-center">
                                        <p>Copyright© 2018, All Right Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- start scrollUp
            ============================================ -->
            <div id="toTop">
                <i class="fa fa-chevron-up"></i>
            </div>
        </div>
<?php if(@$chat=="active"){?>
    <div class="chat-box">
        <div class="header">
            <?php 
            $s_id=@$user_info['id'];
            $r_id=@$receiver_id;
            echo @$receiver_name;
            ?>
            <div class="mini">-</div>
        </div>
        <?php 
            $chat_qry="select * from chat where (s_id=$s_id and r_id=$r_id) or (s_id=$r_id and r_id=$s_id)";
            $chat_res = $this->Main_model->my_query($chat_qry);
        ?>
        <div id="messages" class="messages">
            <ul>
                <?php 
                if(!@$chat_res['blank']){
                foreach($chat_res as $m) {
                    if($m['s_id']==$s_id){ ?>
                    <li>
                        <span class="right"><?php echo @$m['msg']; ?> </span>
                        <div class="clear"></div>
                    </li> 
                    <?php  }else{ ?>
                    <li>
                        <span class="left"><?php  echo @$m['msg']; ?></span>
                        <div class="clear"></div>
                    </li> 
                <?php } } }?>
            </ul>
            <div class="clear"></div>
        </div>
        <div class="input-box">
            <input type="hidden" id="s_id" value="<?php echo @$s_id;?>">
            <input type="hidden" id="r_id" value="<?php echo @$r_id;?>">
            <input type="text" name="mess" id="mess" onKeydown="Javascript: if (event.keyCode==13) send_msg(<?php echo @$s_id;?>,<?php echo @$r_id;?>,this.value);" placeholder="Enter message">
        </div>
    </div>
    <div class="def-btn btn-bg-blue mini_chat"><i class="fa fa-comments-o"></i></div>
    <script type="text/javascript">
        var elem = document.getElementById('messages');
        elem.scrollTop = elem.scrollHeight;
        $(document).ready(function(){
        setInterval(function(){chat_box()},1000);
        $('.mini').on('click',function() {
            $('.chat-box').hide();
            $('.mini_chat').show();
        });
        $('.mini_chat').on('click',function() {
            $('.chat-box').show();
            $('.mini_chat').hide();
        });
    });
    </script>
<?php }//chat is active end?>        
		<script src="<?php echo base_url();?>assets/user/js/vendor/jquery-1.12.4.min.js"></script>
		<script src="<?php echo base_url();?>assets/user/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/user/js/ajax-mail.js"></script>
		<script src="<?php echo base_url();?>assets/user/js/wow.min.js"></script>
		<script src="<?php echo base_url();?>assets/user/lib/js/jquery.nivo.slider.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/user/lib/home.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/user/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/user/js/main.js"></script>
        <script src="<?php echo base_url();?>assets/admin/js/custom-file-input.js"></script>

    </body>
</html>
