<?php $this->load->view("user/header.php");?>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<style>
.form-control{
    height: 40px;
}
</style>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Edit Details</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Edit Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="subscribe-area ptb-80 subscribe-nn-pb" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="subscribe blog-subscribe">
                    <div class="subscribe-text">
                        <h3>Edit Details</h3>
                    </div>
                    <div class="subscribe-input">
                        <form action="" role="form" method="post" class="form-horizontal">
                            <?php if(@$error){echo '<div class="alert alert-danger">'.@$error.'</div>';}?>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Facebook Link</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-facebook-f"></i></span>
                                        <input class="form-control" placeholder="Facebook Link" name="fb_link" type="text" value="<?php if(set_value('fb_link')){echo set_value('fb_link');}else{echo @$data['fb_link'];}?>"><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Google+ Link</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-google-plus"></i></span>
                                        <input class="form-control" placeholder="Google+ Link" name="g_link" type="text" value="<?php if(set_value('g_link')){echo set_value('g_link');}else{echo @$data['g_link'];}?>"><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Twitter Link</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-twitter"></i></span>
                                        <input class="form-control" placeholder="Twitter Link" name="t_link" type="text" value="<?php if(set_value('t_link')){echo set_value('t_link');}else{echo @$data['t_link'];}?>"><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Instagram Link</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-instagram"></i></span>
                                        <input class="form-control" placeholder="Instagram Link" name="i_link" type="text" value="<?php if(set_value('i_link')){echo set_value('i_link');}else{echo @$data['i_link'];}?>"><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">About</label>
                                <div class="col-lg-10">
                                    <div class="compose-editor">
                                        <textarea name="about" id="about" class="form-control" rows="6" placeholder="About You"><?php if(set_value('about')){echo set_value('about');}else{echo @$data['about'];}?></textarea>
                                    </div>
                                    <script>
                                    CKEDITOR.replace('about');
                                    </script>
                                </div>
                            </div>
                            <input class="btn btn-primary" type="submit" name="btn_submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>