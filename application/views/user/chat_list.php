<?php $this->load->view("user/header.php");?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Chat List</h2>
                    <ul>
                        <li><a href="<?php echo site_url();?>">Home</a></li>
                        <li>Chat List</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-area ptb-80 campaign-two">
    <div class="container">
        <div class="row" id="main_chat_list">
        
        </div>
    </div>
    <script type="text/javascript">
        chat_list();
        setInterval(function(){chat_list()},3000);
    </script>
</section>
<?php $this->load->view("user/footer.php");?>