<?php $this->load->view("user/header.php");
$user_info=$this->User_dashboard_model->get_user_info();
?>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title">Change Password</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li>Change Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="subscribe-area ptb-80 subscribe-nn-pb" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="subscribe blog-subscribe">
                    <div class="subscribe-text">
                        <h3>Change Password</h3>
                    </div>
                    <div class="subscribe-input">
                        <form action="" role="form" method="post" class="mc-form">
                            <?php if(@$error){echo '<p class="alert alert-danger">'.@$error.'</p>';}?>
                            <input placeholder="Old Password" name="old_pwd" type="password" required><br><br>
                            <input placeholder="New Password" name="new_pwd" type="password" required><br><br>
                            <input placeholder="Confirm New Password" name="cnew_pwd" type="password" required><br><br>
                            <input class="submit" type="submit" name="btn_submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>