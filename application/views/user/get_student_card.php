Select Student That You Want<br>
<?php 
if(@$student['blank']){
    echo $student['blank'];
    return;
}
?>
<div class="row">
<?php foreach ($student as $row) { ?>
    <div class="col-sm-3" onclick="set_student_card_details('<?php echo $detail_id; ?>',<?php echo $row['id']; ?>)" style="cursor:pointer;" >
        <div class="card">
            <canvas class="header-bg" width="250" height="70" id="header-blur"></canvas>
            <div class="avatar">
                <img src="" onerror="this.src='<?php echo base_url();?>assets/photos/students/avatar.png'" />
            </div>
            <div class="content">
                <p style="color:white;">Rno : <?php echo $row['rno']; ?>&nbsp;&nbsp;&nbsp;&nbsp; Name : <?php echo $row['name']; ?> <br>
                    Email : <?php echo $row['email']; ?><br>
                    Mobile No : <?php echo $row['mobile_no']; ?></p>
                <!-- <p><button type="button" class="btn btn-default">Contact</button></p> -->
            </div>
        </div>
    </div>
    <img class="src-image" src="<?php echo base_url('assets/photos/students/').$row['image']; ?>"/>
<?php } ?>
</div>
<script src="<?php echo base_url();?>assets/user/js/card_design.js"></script>