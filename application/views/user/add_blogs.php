<?php $this->load->view("user/header.php");?>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<link href="<?php echo base_url();?>assets/admin/css/component.css" rel="stylesheet" type="text/css">
<style>
.form-control{
    height: 40px;
}
</style>
<section class="breadcrumbs-area ptb-140 about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs">
                    <h2 class="page-title"><?php if(@$data){ echo 'Edit';}else{echo 'Add';} ?> Advertisments</h2>
                    <ul>
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li><?php if(@$data){ echo 'Edit';}else{echo 'Add';} ?> Advertisments</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="subscribe-area ptb-80 subscribe-nn-pb" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="subscribe blog-subscribe">
                    <div class="subscribe-text">
                        <h3><?php if(@$data){ echo 'Edit';}else{echo 'Add';} ?> Advertisments</h3>
                    </div>
                    <div class="subscribe-input">
                        <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php if(@$error){echo '<div class="alert alert-danger">'.@$error.'</div>';}?>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Title</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-header"></i></span>
                                        <input class="form-control" placeholder="Title" name="title" type="text" value="<?php if(set_value('title')){echo set_value('title');}else{echo @$data['title'];}?>"><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Description</label>
                                <div class="col-lg-10">
                                    <div class="compose-editor">
                                        <textarea name="description" id="description" class="form-control" rows="6" placeholder="Description"><?php if(set_value('description')){echo set_value('description');}else{echo @$data['description'];}?></textarea>
                                    </div>
                                    <script>
                                    CKEDITOR.replace('description');
                                    </script>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Image</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                    <div class="box">
                                        <img id="pic" style="height:150px !important;width:150px !important;margin-bottom: 10px;" src="<?php echo base_url('assets/photos/blogs/'); if(!empty(@$data['image'])){ echo @$data['image']; }?>">
                                        
                                        <input type="file" id="file-0" onchange="loadfile(event,'pic');" name="img_file" class="inputfile inputfile-2 btn_browse" multiple />
                                        <label for="file-0" class="form-control">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                width="20" height="17" viewBox="0 0 20 17">
                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                            <span id="span0" class="span_file">Choose a file&hellip;</span>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                <div class="col-lg-10">
                                    <div class="input-group m-bot15">
                                        <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1" <?php if(@$data && $data['status']==1){echo " selected";}?>>Active</option>
                                            <option value="0" <?php if(@$data && $data['status']==0){echo " selected";}?>>Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input class="btn btn-primary" type="submit" name="btn_submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("user/footer.php");?>