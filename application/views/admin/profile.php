<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">Profile</header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Name</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
                                                <input type="text" name="name" class="form-control" placeholder="Name" value="<?php if(set_value('name')){echo set_value('name');}else{echo @$data['name'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Profile pic</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <div class="box">
                                                    <img id="pic" style="height:150px;width:150px;margin-bottom: 10px;" src="<?php echo base_url('assets/photos/admin/'); if(!empty(@$data['image'])){ echo @$data['image']; }else{ echo "avatar.png";}?>">
                                                    <input type="file" onchange="loadfile(event,this);" name="img_file" id="file-0" class="inputfile inputfile-2 btn_browse" multiple />
                                                    <label for="file-0" class="form-control">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            width="20" height="17" viewBox="0 0 20 17">
                                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                        <span id="span0" class="span_file">Choose a file&hellip;</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
<?php $this->load->view("admin/footer.php");?>