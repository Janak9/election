<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                                <?php if(@$data){echo "Edit";}else{echo "Add";} ?>
                                Position Chart
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Position Name</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-heading"></i></span>
                                                <input type="text" name="name" class="form-control" placeholder="Position Name" value="<?php if(set_value('name')){echo set_value('name');}else{echo @$data['position_name'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Position No</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-chart-line"></i></span>
                                                <input type="text" name="no" class="form-control" placeholder="Position No" value="<?php if(set_value('no')){echo set_value('no');}else{echo @$data['position_no'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" <?php if(@$data && $data['status']==1){echo " selected";}?>>Active</option>
                                                    <option value="0" <?php if(@$data && $data['status']==0){echo " selected";}?>>Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
    <?php $this->load->view("admin/footer.php");?>