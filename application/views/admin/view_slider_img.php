<?php $this->load->view("admin/header.php"); ?>
<section id="main-content">
    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading"> View Slider Image</div>
                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <form method="post" onSubmit="return del()">
                        <thead>
                            <tr>
                                <th style="width:85px;">
                                    <input type="checkbox" name="checkall" id="checkall" onclick="chkall();">
                                    <button type="submit" class="btn btn-default btn-xs" name="btdel"><i class="far fa-trash-alt" style="width: 25px;font-size: 20px;"></i></button>
                                </th>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (@$data['blank']) {
                                echo "<div class='err_div alert alert-warning'>" . $data['blank'] . "</div>";
                            } else {
                                $i = $this->uri->segment(4) + 1;

                                foreach ($data as $row) { ?>
                                <tr>
                                <td><input type="checkbox" name="chk[]" value="<?php echo $row['id']; ?>" onclick="select_chk()" ></td>
                                <td><?php echo $i;
                                    $i++; ?></td>
                                <td><?php echo $row['title']; ?></td>
                                <td><?php echo $row['description']; ?></td>
                                <td><img class="view_pic" src="<?php echo base_url('assets/photos/slider/').$row['image']; ?>" ></td>
                                <td>
                                    <div style="width: 90px;">
                                    <div class="colorful-switch">
                                    <input type="checkbox" class="colorful-switch__checkbox" id="chk<?php echo $row['id']; ?>" <?php if ($row['status'] == 1) { echo "checked";} ?>/>
                                    <label class="colorful-switch__label" for="colorful-switch-cb" onclick="status_toggle('chk<?php echo $row['id']; ?>','slider','<?php echo $row['id']; ?>')">
                                        <span class="colorful-switch__bg"></span>
                                        <span class="colorful-switch__dot"></span>
                                        <span class="colorful-switch__on">
                                        <span class="colorful-switch__on__inner"></span>
                                        </span>
                                        <span class="colorful-switch__off"></span>
                                    </label>
                                    </div></div>
                                    </td>
                                <td class="text-center">
                                    <a href="<?php echo site_url('admin/Clg_info/fn_add_slider_img/' . $row['id']); ?>" class="btn btn-default btn-xs"><i style="color:#FC0;" class="fa fa-pencil-alt"></i> Edit</a>
                                    <a href="<?php echo site_url('admin/Clg_info/fn_delete_slider_img/' . $row['id']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                                </tr>
                                <?php 
                            }
                            } ?>
                        </tbody>  
                        </form> 
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 text-right text-center-xs">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </section>
    <?php $this->load->view("admin/footer.php"); ?>