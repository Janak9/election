<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content">
    <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">Home Slider Images
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <?php
                                    $cnt=0; 
                                    if(@$data)
                                    {
                                    $imgs=explode(',',$data['slider_images']);
                                    for($i=0;$i<count($imgs);$i++) {  
                                        $pic="pic".$cnt;
                                        $span="span".$cnt;
                                        $div_id="file_up_div".$cnt;
                                    ?>
                                    <div id="<?php echo $div_id;?>" class="form-group">
                                        <button type="button" class="close" onclick="remove_img('#<?php echo $div_id;?>');">&times;</button>
                                        <label for="img_file" class="col-lg-2 col-sm-2 control-label">Picture
                                            <?php echo $cnt+1;?></label>
                                        <div class="col-lg-10">
                                            <img id="<?php echo $pic;?>" src="<?php if(@$data){echo base_url('assets/photos/slider/'.$imgs[$i]);}?>"
                                                height="150px" width="150px">
                                            <div class="box">
                                                <input type="file" onchange="loadfile1(event,'<?php echo $pic;?>','<?php echo $span;?>')"
                                                    name="img_file<?php echo $cnt;?>" id="file-<?php echo $cnt;?>"
                                                    class="inputfile inputfile-2 btn_browse" multiple />
                                                <label for="file-<?php echo $cnt;?>" class="form-control"><svg xmlns="http://www.w3.org/2000/svg"
                                                        width="20" height="17" viewBox="0 0 20 17">
                                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                    <span id="<?php echo $span;?>" class="span_file">
                                                        <?php if(@$data){echo $imgs[$i];}else{echo "Choose a file";}?>&hellip;</span></label>
                                            </div>
                                        </div>
                                        <?php $cnt++; ?>
                                    </div>
                                    <?php  }
                                    }
                                    else{
                                    ?>
                                    <div id="file_up_div0" class="form-group">
                                        <button type="button" class="close" onclick="remove_img('#file_up_div0');">&times;</button>
                                        <label for="img_file" class="col-lg-2 col-sm-2 control-label">Picture 1</label>
                                        <div class="col-lg-10">
                                            <img id="pic0" src="" height="150px" width="150px">
                                            <div class="box">
                                                <input type="file" onchange="loadfile1(event,'pic0','span0')" name="img_file0" id="file-0" class="inputfile inputfile-2 btn_browse" multiple />
                                                <label for="file-0" class="form-control"><svg xmlns="http://www.w3.org/2000/svg"
                                                        width="20" height="17" viewBox="0 0 20 17">
                                                        <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                    <span id="span0" class="span_file">Choose a file&hellip;</span></label>
                                            </div>
                                        </div>
                                        <?php $cnt++;?>
                                    </div>
                                    <?php  } ?>
                                    <div class="form-group more_img_div" id="<?php echo 'img'.$cnt;?>">
                                    </div>
                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-8">
                                            <input type="submit" name="btn_submit" value="submit" class="btn btn-default">
                                        </div>
                                    </div>
                                </form>
                                <div id="add_img_btn">
                                    <button name="more_img" id="more_img" class="form-control icon-plus btn btn-primary" onclick="get_image(<?php echo $cnt;?>)"> Add more image</button>
                                </div>
                            </div>
                        </div>
                </div>
    </section>

    </div>
    </div>
    <!-- page end-->
    </div>
</section>
<?php $this->load->view("admin/footer.php");?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8L-CqR8B5VdiESOJPMlVgGag88yataaE&libraries=places&callback=initAutocomplete">
</script>