<?php
$admin_info=$this->Dashboard_model->get_admin_info();
?>
<!DOCTYPE html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/admin/css/css.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/admin/css/font.css" type="text/css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/admin/fontawesome/css/all.css" rel="stylesheet"> 
    <!-- custome file input -->
    <link href="<?php echo base_url();?>assets/admin/css/component.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/admin/css/demo.css" rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/admin/css/normalize.css" rel='stylesheet' type='text/css'>
    <!-- <link href="<?php // echo base_url();?>assets/admin/css/morris.css" type="text/css" rel="stylesheet"/> -->
    <!-- calendar -->
    <link href="<?php echo base_url();?>assets/admin/css/monthly.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style_admin.css" type="text/css" rel="stylesheet"/>
    
    <script src="<?php echo base_url();?>assets/admin/js/jquery2.0.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/js/raphael-min.js"></script>
    <script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
    <!-- <script src="<?php //echo base_url();?>assets/admin/js/morris.js"></script> -->
    <script src="<?php echo base_url();?>assets/admin/js/admin.js"></script>
</head>
<body>
<div id="loader"></div>
<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">
    <a href="" class="logo">
    Election
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li>
            <a href="<?php echo site_url(); ?>"><i class="fa fa-home" style="padding: 3px;font-size: 26px;padding-left: 9px;"></i></a>
        </li>
        <li>
            <input type="text" class="form-control search" placeholder=" Search">
        </li>
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img onerror="this.src='<?php echo base_url();?>assets/photos/admin/avatar.png'" class="admin_profile" src="<?php echo base_url();?>assets/photos/admin/<?php echo $admin_info['image'];?>">
                <span class="username"><?php echo $admin_info['name'];?></span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="<?php echo site_url("admin/Profile/fn_edit_profile");?>"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a href="<?php echo site_url("admin/Profile/fn_change_email");?>"><i class=" far fa-envelope"></i>Change Email</a></li>
                <li><a href="<?php echo site_url("admin/Profile/fn_change_password");?>"><i class=" fa fa-key"></i>Change Password</a></li>
                <li><a href="<?php echo site_url("admin/Profile/fn_forgot_password");?>"><i class=" fa fa-key"></i>Forgot Password</a></li>
                <li><a href="<?php echo site_url('admin/Login/fn_logout');?>"><i class="fa fa-sign-out-alt"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="<?php if($menu=='dashboard'){echo 'active';} ?>" href="<?php echo site_url("admin/"); ?>">
                        <i class="fa fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php if(@$admin_info['type']==0){ ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='sub_admin'){echo 'active';} ?>">
                        <i class="fa fa-user"></i>
                        <span>Sub Admin</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Sub_admin/fn_add_sub_admin');?>">Add Sub Admin</a></li>
						<li><a href="<?php echo site_url('admin/Sub_admin/fn_view_sub_admin');?>">View Sub Admin</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='clg_info'){echo 'active';} ?>">
                        <i class="fas fa-university"></i>
                        <span>College Info</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url("admin/Clg_info/fn_edit_clg_info"); ?>">View/Edit College Info</a></li>
                        <li><a href="<?php echo site_url('admin/Clg_info/fn_add_slider_img');?>">Add Slider Images</a></li>
                        <li><a href="<?php echo site_url('admin/Clg_info/fn_view_slider_img');?>">View Slider Images</a></li>
                    </ul>
                </li>
                <?php } ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='department'){echo 'active';} ?>">
                        <i class="fa fa-building"></i>
                        <span>Department</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Department/fn_add_department');?>">Add Department</a></li>
						<li><a href="<?php echo site_url('admin/Department/fn_view_department');?>">View Department</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='division'){echo 'active';} ?>">
                        <i class="fa fa-home"></i>
                        <span>Divisions</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Division/fn_add_division');?>">Add Division</a></li>
						<li><a href="<?php echo site_url('admin/Division/fn_view_division');?>">View Division</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='position_chart'){echo 'active';} ?>">
                        <i class="fas fa-chart-line"></i>
                        <span>Position Chart</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Position_chart/fn_add_position_chart');?>">Add Position Chart</a></li>
						<li><a href="<?php echo site_url('admin/Position_chart/fn_view_position_chart');?>">View Position Chart</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='student'){echo 'active';} ?>">
                        <i class="fa fa-users"></i>
                        <span>Student</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Student/fn_add_student');?>">Add Student</a></li>
						<li><a href="<?php echo site_url('admin/Student/fn_view_student');?>">View Student</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='election'){echo 'active';} ?>">
                    <i class="fas fa-poll-h"></i>
                        <span>Election</span>
                    </a>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Election/fn_add_election');?>">Add Election</a></li>
						<li><a href="<?php echo site_url('admin/Election/fn_view_election');?>">View Election</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($menu=='candidate'){echo 'active';} ?>">
                        <i class="far fa-user"></i>
                        <span>Candidate</span>
                        <span class="badge badge-primary" style="margin-left:6px;" id="applied_candidate_count"></span>
                    </a>
                    <script type="text/javascript">
                        applied_candidate_count();
                        setInterval(function(){applied_candidate_count()},1000);
                    </script>
                    <ul class="sub">
						<li><a href="<?php echo site_url('admin/Candidate/fn_applied_candidate');?>">Applied Candidate</a></li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="<?php echo site_url('admin/Election/fn_all_results');?>" class="<?php if($menu=='ele_result'){echo 'active';} ?>">
                        <i class="fas fa-file"></i>
                        <span>All Election Results</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('admin/Login/fn_logout');?>">
                        <i class="fa fa-sign-out-alt"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>            </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->