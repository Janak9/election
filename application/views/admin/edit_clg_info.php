<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content">
    <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">College Information
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Name</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
                                                <input type="text" name="name" class="form-control" placeholder="Name"
                                                    value="<?php if(set_value('name')){echo set_value('name');}else{echo @$data['clg_name'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Email</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="far fa-envelope"></i></span>
                                                <input type="email" name="email" class="form-control" placeholder="Enter Email"
                                                    value="<?php if(set_value('email')){echo set_value('email');}else{echo @$data['email'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        $no=explode(",",$data['clg_contact_no']);
                                    ?>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Contact Number 1</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="contact1" class="form-control" placeholder="Enter Contact Number"
                                                    value="<?php if(set_value('contact1')){echo set_value('contact1');}else{echo @$no[0];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Contact Number 2(optional)</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="contact2" class="form-control" placeholder="Enter Contact Number"
                                                    value="<?php if(set_value('contact2')){echo set_value('contact2');}else{echo @$no[1];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Contact Numbers 3(optional)</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="contact3" class="form-control" placeholder="Enter Contact Number"
                                                    value="<?php if(set_value('contact3')){echo set_value('contact3');}else{echo @$no[2];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Contact Numbers 4(optional)</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="contact4" class="form-control" placeholder="Enter Contact Number"
                                                    value="<?php if(set_value('contact4')){echo set_value('contact4');}else{echo @$no[3];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Address</label>
                                        <div class="col-lg-10">
                                            <div class="compose-editor">
                                                <textarea name="addr" class="form-control" rows="5" placeholder="Enter College Address"><?php if(set_value('addr')){echo set_value('addr');}else{echo @$data['address'];}?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">About</label>
                                        <div class="col-lg-10">
                                            <div class="compose-editor">
                                                <textarea name="about" id="about" class="form-control" rows="6" placeholder="About Your College"><?php if(set_value('about')){echo set_value('about');}else{echo @$data['about_clg'];}?></textarea>
                                            </div>
                                            <script>
                                            CKEDITOR.replace('about');
                                            </script>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Principal</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
                                                <input type="text" name="principal" class="form-control" placeholder="Principal Name"
                                                    value="<?php if(set_value('principal')){echo set_value('principal');}else{echo @$data['principal'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">College Website</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-link"></i></span>
                                                <input type="text" name="website" class="form-control" placeholder="Website"
                                                    value="<?php if(set_value('website')){echo set_value('website');}else{echo @$data['website'];}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Logo</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">        
                                                <div class="box">
                                                    <img id="pic" style="height:150px;width:150px;margin-bottom: 10px;" src="<?php echo base_url('assets/photos/'); if(!empty(@$data['clg_logo'])){ echo @$data['clg_logo']; }else{ echo "def_clg_logo.png";}?>">
                                                    <input type="file" onchange="loadfile(event,this);" name="img_file" id="file-0" class="inputfile inputfile-2 btn_browse" multiple />
                                                    <label for="file-0" class="form-control">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            width="20" height="17" viewBox="0 0 20 17">
                                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                        <span id="span0" class="span_file">Choose a file&hellip;</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Location On Map:</label>
                                        <div class="col-md-8">
                                            <input type="hidden" name="lat" id="lat" value="<?php if(set_value('lat')){echo set_value('lat');}else{echo @$data['lat'];}?>">
                                            <input type="hidden" name="lng" id="lng" value="<?php if(set_value('lng')){echo set_value('lng');}else{echo @$data['lng'];}?>">
                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                            <div id="map"></div>
                                            <input type='button' value='Delete' class="btn btn-deafault" onclick='delete_marker()'/>
                                            <input type='button' value='Save' class="btn btn-deafault" onclick='saveData()' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" <?php if(@$data && $data['status']==1){echo
                                                        " selected" ;}?>>Active</option>
                                                    <option value="0" <?php if(@$data && $data['status']==0){echo
                                                        " selected" ;}?>>Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
<?php $this->load->view("admin/footer.php");?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIE92HcJNtPloQ6FNTTu-ctyDzf1rkzWw&libraries=places&callback=initAutocomplete">
</script>
