<?php $this->load->view("admin/header.php"); ?>
<section id="main-content">
    <section class="wrapper">
        <div class="table-agile-info">
            <div class="panel panel-default">
                <div class="panel-heading"> View Candidate </div>
                <div class="table-responsive">
                    <p class="alert-warning" >*After accept or denied the request it take sometime because email is sending to candidate</p>
                    <table class="table table-striped b-t b-light">
                        <form method="post" onSubmit="return del()">
                        <thead>
                            <tr>
                                <th style="width:85px;">
                                    <input type="checkbox" name="checkall" id="checkall" onclick="chkall();">
                                    <button type="submit" class="btn btn-default btn-xs" name="btdel"><i class="far fa-trash-alt" style="width: 25px;font-size: 20px;"></i></button>
                                </th>
                                <th>Id</th>
                                <th>Student Name</th>
                                <th>Roll No.</th>
                                <th>Semester</th>
                                <th>Division</th>
                                <th>Year</th>
                                <th>Department</th>
                                <th>Position</th>
                                <th>Applied For Position</th>
                                <th>Proposer</th>
                                <th>Supporter</th>
                                <th>Status</th>
                                <th>Document</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (@$data['blank']) {
                                echo "<div class='err_div alert alert-warning'>" . $data['blank'] . "</div>";
                            } else {
                                $i = $this->uri->segment(4) + 1;

                                foreach ($data as $row) { ?>
                                <tr>
                                <td><input type="checkbox" name="chk[]" value="<?php echo $row['id']; ?>" onclick="select_chk()" ></td>
                                <td><?php echo $i;
                                    $i++; ?></td>
                                <?php $stud = $this->Main_model->select_record($row['stud_id'],"student");?>
                                <td><?php echo $stud['name']; ?></td>
                                <td><?php echo $stud['rno']; ?></td>
                                <td><?php echo $stud['semester']; ?></td>
                                <td><?php echo $stud['division']; ?></td>
                                <td><?php echo $stud['year']; ?></td>
                                <td><?php 
                                    $dept = $this->Main_model->select_record($stud['dept_no'],"department");
                                    echo $dept['dept_name']; ?></td>
                                <td><?php 
                                    $position = $this->Main_model->select_record($stud['position'],"position_chart");
                                    echo $position['position_name']; ?></td>
                                <td><?php 
                                    $position = $this->Main_model->select_record($row['position'],"position_chart");
                                    echo $position['position_name']; ?></td>
                                <?php $stud_pro = $this->Main_model->select_record($row['proposer_id'],"student");?>
                                <td><a target="_blank" href="<?php echo site_url('admin/Student/fn_add_student/' . $stud_pro['id']); ?>"><?php echo $stud_pro['name']; ?></a></td>
                                <?php $stud_sup = $this->Main_model->select_record($row['supporter_id'],"student");?>
                                <td><a target="_blank" href="<?php echo site_url('admin/Student/fn_add_student/' . $stud_sup['id']); ?>"><?php echo $stud_sup['name']; ?></a></td>
                                <td>
                                    <div style="width: 90px;">
                                    <div class="colorful-switch">
                                    <input type="checkbox" class="colorful-switch__checkbox" id="chk<?php echo $row['id']; ?>" <?php if ($row['status'] == 1) { echo "checked";} ?>/>
                                    <label class="colorful-switch__label" for="colorful-switch-cb" onclick="status_change('<?php echo $stud['id']; ?>',status_toggle,['chk<?php echo $row['id']; ?>','candidate','<?php echo $row['id']; ?>'])">
                                        <span class="colorful-switch__bg"></span>
                                        <span class="colorful-switch__dot"></span>
                                        <span class="colorful-switch__on">
                                        <span class="colorful-switch__on__inner"></span>
                                        </span>
                                        <span class="colorful-switch__off"></span>
                                    </label>
                                    </div></div>
                                </td>
                                <td><a target="_blank" href="<?php echo base_url('assets/photos/candidate_docs/').$row['image']; ?>"><img class="view_pic" src="<?php echo base_url('assets/photos/candidate_docs/').$row['image']; ?>" ></a></td>
                                <td></td>
                                <td class="text-center">
                                    <a href="<?php echo site_url('admin/Candidate/fn_delete_candidate/' . $row['id']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                                </tr>
                                <?php 
                            }
                            } ?>
                        </tbody>  
                        </form> 
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 text-right text-center-xs">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </section>
    <?php $this->load->view("admin/footer.php"); ?>
    <script>
        function status_change(sid,callback,args) {
            var t = document.getElementById(args[0]).checked;
            callback.apply(this,args);
            var msg="";
            if(t == true){ //value is taken before change so msg is oposite to checkbox value
                msg="Your request for candidate is denied by administrator";
            }else{
                msg="Your request for candidate is accept by administrator";
            }
            $.ajax({
                type:"get",
                url:"http://localhost/election/index.php/admin/Candidate/fn_notify/"+sid+"/"+msg
            });
        }    
    </script>