<?php
	$pic="pic".$cnt;
	$span="span".$cnt;
	$div_id="file_up_div".$cnt;
?>
<div id="<?php echo $div_id;?>" class="form-group">
	<button type="button" class="close" onclick="remove_img('#<?php echo $div_id;?>');">&times;</button>
    <label for="img_file" class="col-lg-2 col-sm-2 control-label">Picture <?php echo $cnt+1;?></label>
    <div class="col-lg-10">
      <img id="<?php echo $pic;?>" height="150px" width="150px">
      <div class="box">
        <input type="file" onchange="loadfile1(event,'<?php echo $pic;?>','<?php echo $span;?>')" name="img_file<?php echo $cnt;?>" id="file-<?php echo $cnt;?>" class="inputfile inputfile-2 btn_browse" multiple />
        <label for="file-<?php echo $cnt;?>" class="form-control"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span id="<?php echo $span;?>" class="span_file">Choose a file&hellip;</span></label>
      </div>
    </div>
</div>
<div class='form-group more_img_div' id="img<?php echo $cnt+1;?>"  style="margin-bottom: 0;">
</div>