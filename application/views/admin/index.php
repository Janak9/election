<?php $this->load->view("admin/header.php") ?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<!-- //market-->
		<div class="market-updates">
			<div class="col-md-3 market-update-gd">
				<div class="market-update-block clr-block-2">
					<div class="col-md-4 market-update-right">
						<i class="fas fa-poll-h dashboard_icon"> </i>
					</div>
					<div class="col-md-8 market-update-left">
						<h4>Elections</h4>
						<h3><?php echo $this->Main_model->row_count('election'); ?></h3>
						<p>All Election That Are Conducted Till Now</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-3 market-update-gd">
				<div class="market-update-block clr-block-1">
					<div class="col-md-4 market-update-right">
						<i class="fa fa-users dashboard_icon"></i>
					</div>
					<div class="col-md-8 market-update-left">
						<h4>Students</h4>
						<h3><?php echo $this->Main_model->row_count('student'); ?></h3>
						<p>Students That Are Involve In College</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-3 market-update-gd">
				<div class="market-update-block clr-block-3">
					<div class="col-md-4 market-update-right">
						<i class="far fa-user dashboard_icon"></i>
					</div>
					<div class="col-md-8 market-update-left">
						<h4>Candidate</h4>
						<h3><?php echo $this->Main_model->row_count('candidate'); ?></h3>
						<p>Students That Are Participate In Elections</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<?php $this->load->view("admin/footer.php") ?>