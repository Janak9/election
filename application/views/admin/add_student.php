<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <?php if(@$data){echo "Edit";}else{echo "Add";} ?>
                            Student
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <center>
                                    <input type="radio" name="excel_view" value="is_excel" id="excel_id" onchange="swith_to('is_excel','is_not_excel')" <?php if(!@$data){ echo 'checked'; }?>><label for="excel_id">Upload Excel</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="excel_view" value="is_not_excel" id="not_excel_id" onchange="swith_to('is_not_excel','is_excel')" <?php if(@$data){ echo 'checked'; }?>><label for="not_excel_id">Enter Manual Recorde</label>
                                </center><br>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="show_loader()">
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Department</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="dept" id="dept" class="form-control" onchange="get_division('dept','year','division')" required>
                                                    <?php
                                                    if(@$dept['blank']){
                                                        echo @$dept['blank'];
                                                    }else{
                                                        foreach($dept as $row){
                                                            ?>
                                                            <option value="<?php echo $row['id']; ?>" <?php if(@$data && $data['dept_no']==$row['id']){echo " selected";}?>><?php echo $row['dept_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Year</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="year" id="year" class="form-control" onchange="get_division('dept','year','division')" required>
                                                    <option value="">Select Year</option>
                                                    <option value="1" <?php if(@$data && $data['year']==1){echo " selected";}?>>1</option>
                                                    <option value="2" <?php if(@$data && $data['year']==2){echo " selected";}?>>2</option>
                                                    <option value="3" <?php if(@$data && $data['year']==3){echo " selected";}?>>3</option>
                                                    <option value="4" <?php if(@$data && $data['year']==4){echo " selected";}?>>4</option>
                                                    <option value="5" <?php if(@$data && $data['year']==5){echo " selected";}?>>5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Division</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="division" id="division" class="form-control" required>
                                                    <option value="">Select Division</option>
                                                    <?php
                                                    if(@$data && @$division){
                                                        foreach($division as $row){
                                                            if($row['dept_no']==$data['dept_no'] && $row['year']==$data['year']){
                                                                for($i=1;$i<=$row['no_of_div'];$i++){
                                                                ?>
                                                                <option value="<?php echo $i;?>" <?php if(@$data && $data['division']==$i){echo " selected";}?>><?php echo $i;?></option>
                                                                <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Semester</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="semester" id="semester" class="form-control" required>
                                                    <option value="">Select Semester</option>
                                                    <?php
                                                    if(@$data){
                                                    ?>
                                                    <option value="<?php echo $data['year']*2-1;?>" <?php if(@$data && ($data['year']*2-1)==$data['semester']){echo " selected";}?>><?php echo $data['year']*2-1;?></option>
                                                    <option value="<?php echo $data['year']*2;?>" <?php if(@$data && ($data['year']*2)==$data['semester']){echo " selected";}?>><?php echo $data['year']*2;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="is_not_excel" <?php if(!@$data){ echo 'style="display:none;"'; }?> >
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Roll No.</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <input type="number" name="rno" class="form-control" placeholder="Roll Number" value="<?php if(set_value('rno')){echo set_value('rno');}else{echo @$data['rno'];}?>" min="1" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Student Name</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
                                                    <input type="text" name="sname" class="form-control" placeholder="Student Name" value="<?php if(set_value('sname')){echo set_value('sname');}else{echo @$data['name'];}?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Mobile Number</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="fa fa-phone"></i></span>
                                                    <input type="text" name="phno" class="form-control" placeholder="Mobile Number" value="<?php if(set_value('phno')){echo set_value('phno');}else{echo @$data['mobile_no'];}?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Email</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="far fa-envelope"></i></span>
                                                    <input type="email" name="email" class="form-control" placeholder="Email" value="<?php if(set_value('email')){echo set_value('email');}else{echo @$data['email'];}?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">DOB</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="fa fa-calendar"></i></span>
                                                    <input type="date" name="dob" class="form-control" placeholder="Date Of Birth" value="<?php if(set_value('dob')){echo set_value('dob');}else{echo @$data['dob'];}?>" onchange="calc_age(this,'age')" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Age</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <input type="number" name="age" id="age" class="form-control" placeholder="Age" value="<?php if(set_value('age')){echo set_value('age');}else{echo date_diff(date_create(@$data['dob']), date_create('today'))->y;}?>" min="1" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Gender</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="fa fa-venus-mars"></i></span>
                                                    <select name="gender" class="form-control" > 
                                                        <option value="male" <?php if(@$data && $data['gender']=="male"){echo " selected";}?>>Male</option>
                                                        <option value="female" <?php if(@$data && $data['gender']=="female"){echo " selected";}?>>Female</option>
                                                        <option value="other" <?php if(@$data && $data['gender']=="other"){echo " selected";}?>>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Address</label>
                                            <div class="col-lg-10">
                                                <div class="compose-editor">
                                                    <textarea name="addr" class="form-control" rows="5" placeholder="Enter Address" ><?php if(set_value('addr')){echo set_value('addr');}else{echo @$data['address'];}?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Picture</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">        
                                                    <div class="box">
                                                        <img id="pic" style="height:150px;width:150px;margin-bottom: 10px;" src="<?php echo base_url('assets/photos/students/'); if(!empty(@$data['image'])){ echo @$data['image']; }else{ echo "avatar.png";}?>">
                                                        <input type="file" onchange="loadfile(event,this);" name="img_file" id="file-0" class="inputfile inputfile-2 btn_browse" multiple />
                                                        <label for="file-0" class="form-control">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                width="20" height="17" viewBox="0 0 20 17">
                                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                            <span id="span0" class="span_file">Choose a file&hellip;</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Position</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <select name="position" id="position" class="form-control" >
                                                        <?php
                                                        if(@$position['blank']){
                                                            echo @$position['blank'];
                                                        }else{
                                                            foreach($position as $row){
                                                                ?>
                                                                <option value="<?php echo $row['id']; ?>" <?php if(@$data && $data['position']==$row['id']){echo " selected";}?>><?php echo $row['position_name']; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">
                                                    <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="1" <?php if(@$data && $data['status']==1){echo " selected";}?>>Active</option>
                                                        <option value="0" <?php if(@$data && $data['status']==0){echo " selected";}?>>Deactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="is_excel" <?php if(@$data){ echo 'style="display:none;"'; }?>>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Select Excel File</label>
                                            <div class="col-lg-10">
                                                <div class="input-group m-bot15">        
                                                    <div class="box">
                                                        <input type="file" name="excel_file" id="file-1" class="inputfile inputfile-2 btn_browse" multiple />
                                                        <label for="file-1" class="form-control">
                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                width="20" height="17" viewBox="0 0 20 17">
                                                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg>
                                                            <span id="span1" class="span_file">Choose a file&hellip;</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <p style="color:red;">*It may take time to send email to all students</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                                <script>
                                    function show_loader(){
                                        $("#loader").css('display','block');
                                    }
                                </script>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
    <?php $this->load->view("admin/footer.php");?>
<script>
function swith_to(to,from){
    $("#"+to).show();
    $("#"+from).hide();
}
</script>