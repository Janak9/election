<!DOCTYPE html>
<head>
	<title>Admin Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />
	<link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet"/>
	<link href="<?php echo base_url();?>assets/admin/css/css.css" rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url();?>assets/admin/css/font.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>assets/admin/fontawesome/css/all.css" rel="stylesheet"> 
	<script src="<?php echo base_url();?>assets/admin/js/jquery2.0.3.min.js"></script>
</head>
<body>
<label><a href="<?php echo site_url();?>" style="color:#FFF;right: 25px;top:20px;position: absolute;">Home</a></label>
<div class="log-w3">
<div class="w3layouts-main">
	<h2><i class="fas fa-user-cog"></i> Sign In Now</h2>
		<form method="post">
			<input type="email" name="uname" class="ggg" placeholder="Email" required>
			<input type="password" name="pwd" class="ggg" placeholder="Password" required>
			<h6><a href="<?php echo site_url("admin/Profile/fn_forgot_password"); ?>">Forgot Password?</a></h6>
			<div class="clearfix"></div>
				<br>
              <?php  
              if(@$error)
                  echo "<div class='err_div alert alert-danger'>".$error."</div>";
              ?>
				<input type="submit" value="Sign In"  name="btn_submit">
		</form>
		<!-- <p>Don't Have an Account ?<a href="#">Create an account</a></p> -->
</div>
</div>
<script src="<?php echo base_url();?>assets/admin/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="<?php echo base_url();?>assets/admin/js/jquery.scrollTo.js"></script>
</body>
</html>
