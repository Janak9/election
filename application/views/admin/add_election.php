<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                                <?php if(@$data){echo "Edit";}else{echo "Add";} ?>
                                Election
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Level</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-chart-line"></i></span>
                                                <select name="level" id="level" class="form-control" onchange="check_level()"> <!-- check_level() code is in end -->
                                                    <option value="class" <?php if(@$data && $data['level']=="class"){echo " selected";}?>>Class</option>
                                                    <option value="college" <?php if(@$data && $data['level']=="college"){echo " selected";}?>>College</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="dept_div">
                                        <label class="col-lg-2 col-sm-2 control-label">Department</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-building"></i></span>
                                                <select name="dept" class="form-control">
                                                    <?php
                                                    if(@$dept['blank']){
                                                        echo @$dept['blank'];
                                                    }else{
                                                        foreach($dept as $row){
                                                            ?>
                                                            <option value="<?php echo $row['id']; ?>" <?php if(@$data && $data['dept_no']==$row['id']){echo " selected";}?>><?php echo $row['dept_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Name</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-heading"></i></span>
                                                <input type="text" name="name" class="form-control" placeholder="Enter Election Name" value="<?php if(set_value('name')){echo set_value('name');}else{echo @$data['name'];}?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Description</label>
                                        <div class="col-lg-10">
                                            <div class="compose-editor">
                                                <textarea name="description" id="description" class="form-control" rows="6" placeholder="About Your College"><?php if(set_value('description')){echo set_value('description');}else{echo @$data['description'];}?></textarea>
                                            </div>
                                            <script>
                                            CKEDITOR.replace('description');
                                            </script>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Election Date</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-clock"></i></span>
                                                <input type="datetime-local" name="edate" class="form-control" value="<?php if(set_value('edate')){echo set_value('edate');}elseif(@$data){echo date("Y-m-d\TH:i", strtotime(@$data['edate']));}else{echo date("Y-m-d\TH:i");}?>">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Registration End Date</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-clock"></i></span>
                                                <input type="datetime-local" name="registration_end_date" class="form-control" value="<?php if(set_value('registration_end_date')){echo set_value('registration_end_date');}elseif(@$data){echo date("Y-m-d\TH:i", strtotime(@$data['registration_end_date']));}else{echo date("Y-m-d\TH:i");}?>">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Advertisment End Date</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-clock"></i></span>
                                                <input type="datetime-local" name="ads_end_date" class="form-control" value="<?php if(set_value('ads_end_date')){echo set_value('ads_end_date');}elseif(@$data){echo date("Y-m-d\TH:i", strtotime(@$data['ads_end_date']));}else{echo date("Y-m-d\TH:i");}?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Election Hours</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-heading"></i></span>
                                                <input type="number" name="hours" class="form-control" placeholder="Enter Election Hours" value="<?php if(set_value('hours')){echo set_value('hours');}else{echo @$data['hours'];}?>" min="1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" <?php if(@$data && $data['status']==1){echo " selected";}?>>Active</option>
                                                    <option value="0" <?php if(@$data && $data['status']==0){echo " selected";}?>>Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
    <?php $this->load->view("admin/footer.php");?>
    <script>
        check_level(); // call one for update time check status
        function check_level(){
            if($('#level').val() == "college"){
                $('#dept_div').hide();
            }else{
                $('#dept_div').show();
            }
        }
    </script>