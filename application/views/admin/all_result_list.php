<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
<section class="wrapper">
    <div class="form-w3layouts">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">All Election Result</header>
                    <div class="panel-body">
                        <div class="position-center">
                            <?php 
                            if(empty($files)){
                                echo "No Results Are Available";
                            }else{
                                $i = 0;
                                foreach($files as $file){ 
                                $i++;
                                ?>
                                    <div style="margin:10px 0px;">
                                        <a href="<?php echo site_url('admin/Election/delete_result_pdf/'.$file); ?>" style="float:left;"><i class="fas fa-trash-alt" style="margin-right:20px;font-size:19px;"></i></a>
                                        <a target="_blank" href="<?php echo base_url('assets/result_pdf/'.$file); ?>" ><h4><?php echo $i.") ".$file; ?><i class="fas fa-file" style="margin-left:10px;"></i></h4></a>
                                    </div>
                            <?php } } ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view("admin/footer.php");?>