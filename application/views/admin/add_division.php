<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                                <?php if(@$data){echo "Edit";}else{echo "Add";} ?>
                                Division
                        </header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Department</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="dept" class="form-control">
                                                    <?php
                                                    if(@$dept['blank']){
                                                        echo @$dept['blank'];
                                                    }else{
                                                        foreach($dept as $row){
                                                            ?>
                                                            <option value="<?php echo $row['id']; ?>" <?php if(@$data && $data['dept_no']==$row['id']){echo " selected";}?>><?php echo $row['dept_name']; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Year</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <select name="year" id="year" class="form-control">
                                                    <option value="1" <?php if(@$data && $data['year']==1){echo " selected";}?>>1</option>
                                                    <option value="2" <?php if(@$data && $data['year']==2){echo " selected";}?>>2</option>
                                                    <option value="3" <?php if(@$data && $data['year']==3){echo " selected";}?>>3</option>
                                                    <option value="4" <?php if(@$data && $data['year']==4){echo " selected";}?>>4</option>
                                                    <option value="5" <?php if(@$data && $data['year']==5){echo " selected";}?>>5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Number Of Division</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <input type="number" name="no_of_div" class="form-control" placeholder="Enter Number Of Division" value="<?php if(set_value('no_of_div')){echo set_value('no_of_div');}else{echo @$data['no_of_div'];}?>" min="1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Status</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-check"></i></span>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" <?php if(@$data && $data['status']==1){echo " selected";}?>>Active</option>
                                                    <option value="0" <?php if(@$data && $data['status']==0){echo " selected";}?>>Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
    <?php $this->load->view("admin/footer.php");?>