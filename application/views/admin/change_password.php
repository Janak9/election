<?php $this->load->view("admin/header.php"); ?>
<!--main content start-->
<section id="main-content"> <!--main content end in footer-->
    <section class="wrapper">
        <div class="form-w3layouts">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">Change Password</header>
                        <div class="panel-body">
                            <div class="position-center">
                                <?php
                                    if(!empty(validation_errors())) //form errors only
                                        echo "<div class='err_div alert alert-danger'>".validation_errors()."</div>";
                                    else if(@$error) // database or other manual error
                                        echo "<div class='err_div alert alert-danger'>".$error."</div>";
                                ?>
                                <form action="" role="form" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Old Password</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-key"></i></span>
                                                <input type="password" name="old_pwd" class="form-control" placeholder="Old Password" value="<?php if(set_value('old_pwd')){echo set_value('old_pwd');}?>" maxlength="8" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">New Password</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-key"></i></span>
                                                <input type="password" name="pwd" class="form-control" placeholder="New Password" value="<?php if(set_value('pwd')){echo set_value('pwd');}?>" maxlength="8" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 col-sm-2 control-label">Confirm Password</label>
                                        <div class="col-lg-10">
                                            <div class="input-group m-bot15">
                                                <span class="input-group-addon btn-white"><i class="fa fa-key"></i></span>
                                                <input type="password" name="cpwd" class="form-control" placeholder="Confirm Password" value="<?php if(set_value('cpwd')){echo set_value('cpwd');}?>" maxlength="8" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <input type="submit" name="btn_submit" value="Submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <!-- page end-->
        </div>
    </section>
<?php $this->load->view("admin/footer.php");?>