<!DOCTYPE html>
<head>
<title>404-Page Not Found</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" >
<link href="<?php echo base_url();?>assets/admin/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet"/>
<link href='<?php echo base_url();?>assets/admin/css/css.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/font.css" type="text/css"/>
<link href="<?php echo base_url();?>assets/admin/css/font-awesome.css" rel="stylesheet"> 
<script src="<?php echo base_url();?>assets/admin/js/jquery2.0.3.min.js"></script>
</head>
<body>
<!--main content start-->
<div class="eror-w3">
	<div class="agile-info">
		<h3>SORRY</h3>
		<h2>404</h2>
		<h3>Page Not Found</h3>
		<p>An error Occurred in the Application And Your Page could not be Served.</p>
		<a onclick="go_back(1)">go Back</a>
		<a href="<?php echo site_url();?>">go home</a>
	</div>
</div>
<script>
function go_back(page) {
  window.history.back(page);
}
</script>
<script src="<?php echo base_url();?>assets/admin/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/admin/js/flot-chart/excanvas.min.js"></script><![endif]-->
</body>
</html>