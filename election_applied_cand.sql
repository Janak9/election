-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 13, 2018 at 09:57 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.13-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `election`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(8) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `type`, `status`) VALUES
(1, 'janak v', 'vaghelajanak97145@gmail.com', '123123', '2.jpg', 0, 1),
(2, 'sudeep', 'sudeepyadav5@gmail.com', '123123', 'p2.jpg', 0, 1),
(3, 'dhaval', 'variyadhaval758@gmail.com', '123123', 'p3.jpg', 0, 1),
(4, 'smita', 'smitapatel9537@gmail.com', '123123', 'IMG-20160229-WA00172.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `cid`, `title`, `description`, `created_at`, `image`, `status`) VALUES
(3, 7, 'Welcome', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal. Dramatically cultivate viral paradigms after team building e-services. Energistically network worldwide systems after highly efficient innovation.Dramatically leverage other&amp;rsquo;s cross-unit expertise vis-a-vis cross-platform ideas. Uniquely disintermediate cross-media synergy without fully researched internal or &amp;ldquo;organic&amp;rdquo; sources. Monotonectally empower competitive innovation after alternative core competencies. Professionally optimize turnkey deliverables after enterprise bandwidth. Assertively foster inexpensive &amp;ldquo;outside the box&amp;rdquo; thinking rather than intermandated niche markets.</p>\r\n\r\n<p>Interactively implement intermandated architectures without vertical methods of empowerment. Enthusiastically streamline multidisciplinary e-business after frictionless schemas. Phosfluorescently fabricate intuitive architectures through empowered manufactured products. Professionally repurpose dynamic products whereas team driven process improvements. Authoritatively grow leading-edge process improvements vis-a-vis cooperative internal or &amp;ldquo;organic&amp;rdquo; sources.</p>\r\n', '2018-12-13 20:50:51', '3.jpg', 1),
(4, 7, 'New Announcement ', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal. Dramatically cultivate viral paradigms after team building e-services. Energistically network worldwide systems after highly efficient innovation.Dramatically leverage other&amp;rsquo;s cross-unit expertise vis-a-vis cross-platform ideas. Uniquely disintermediate cross-media synergy without fully researched internal or &amp;ldquo;organic&amp;rdquo; sources. Monotonectally empower competitive innovation after alternative core competencies. Professionally optimize turnkey deliverables after enterprise bandwidth. Assertively foster inexpensive &amp;ldquo;outside the box&amp;rdquo; thinking rather than intermandated niche markets.</p>\r\n\r\n<p>Interactively implement intermandated architectures without vertical methods of empowerment. Enthusiastically streamline multidisciplinary e-business after frictionless schemas. Phosfluorescently fabricate intuitive architectures through empowered manufactured products. Professionally repurpose dynamic products whereas team driven process improvements. Authoritatively grow leading-edge process improvements vis-a-vis cooperative internal or &amp;ldquo;organic&amp;rdquo; sources.<br />\r\n&nbsp;</p>\r\n', '2018-12-13 20:55:12', '', 1),
(5, 8, 'Hello, I\'m Gaurav', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\r\n', '2018-12-13 21:30:37', '9.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `id` int(11) NOT NULL,
  `stud_id` int(11) NOT NULL,
  `proposer_id` int(11) NOT NULL,
  `supporter_id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `position` int(11) NOT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  `g_link` varchar(255) DEFAULT NULL,
  `t_link` varchar(255) DEFAULT NULL,
  `i_link` varchar(255) DEFAULT NULL,
  `about` text,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`id`, `stud_id`, `proposer_id`, `supporter_id`, `eid`, `image`, `position`, `fb_link`, `g_link`, `t_link`, `i_link`, `about`, `status`) VALUES
(7, 4, 54, 55, 17, 'sem6.jpeg', 2, 'https://www.facebook.com/janak.vaghela.97', 'https://plus.google.com/u/0/108013413762526383953', 'https://twitter.com/vaghelajanak9?lang=en', 'https://www.instagram.com/janak__vaghela/', '<p>I am specialise in building a website for small and large business. Whatever purpose you want include in your site then i am here to setup your all needs. And not only website i can also develop a software for any business solution.</p>\r\n\r\n<p>My Awesome Skills :- HTML, CSS , JavaScript , PHP , AngularJs, React.js, Python, Django, Java, Mysqli, SQL, jQuery, Codeigniter , Working with API , Project Management.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>All works will completed by me.So your Work will not be outsourced to someone else. I would like to make the project as per the requirement of the client, and i will never complete a project until client is 100% satisfied.</p>\r\n', 1),
(8, 5, 16, 94, 17, 'sem1.jpeg', 3, 'https://www.facebook.com/gaurav.vanani.9?__tn__=%2CdlC-R-R&eid=ARCx_laOKgvACg9tDuiTvhTtPRbuSm8njt_Kff5BGXKpfbaZ3TbOfD12deqOZK7hiVbf33wiRkHCgEHP&hc_ref=ARR2U8emtlFezfBuKH0kD0EJNuPGmyIp2WdtkKKiryVFtpV2pcnlBXpb-L7Sv5G8Tn0', 'https://plus.google.com/u/0/+GauravVanani', 'https://twitter.com/GVanani?lang=en', 'https://www.instagram.com/gauravvanani/', '<p>I am specialise in building a website for small and large business. Whatever purpose you want include in your site then i am here to setup your all needs. And not only website i can also develop a software for any business solution.</p>\r\n\r\n<p>My Awesome Skills :- HTML, CSS , JavaScript , PHP , AngularJs, React.js, Python, Django, Java, Mysqli, SQL, jQuery, Codeigniter , Working with API , Project Management.</p>\r\n\r\n<p>all works will completed by me.So your Work will not be outsourced to someone else. I would like to make the project as per the requirement of the client, and i will never complete a project until client is 100% satisfied.</p>\r\n', 1),
(9, 7, 55, 57, 17, 'sem4.jpeg', 3, NULL, NULL, NULL, NULL, NULL, 1),
(10, 54, 55, 71, 17, 'sem41.jpeg', 2, NULL, NULL, NULL, NULL, NULL, 1),
(11, 55, 58, 62, 17, 'S_sem6.jpeg', 2, NULL, NULL, NULL, NULL, NULL, 1),
(12, 57, 59, 61, 17, 'S_sem61.jpeg', 3, NULL, NULL, NULL, NULL, NULL, 1),
(13, 75, 79, 78, 18, 'sem42.jpeg', 2, NULL, NULL, NULL, NULL, NULL, 1),
(14, 80, 81, 88, 18, 'sem2.jpeg', 3, NULL, NULL, NULL, NULL, NULL, 1),
(15, 6, 52, 48, 19, 'S_sem62.jpeg', 2, NULL, NULL, NULL, NULL, NULL, 1),
(16, 22, 51, 50, 19, 'sem5.jpeg', 3, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `r_id` int(11) NOT NULL,
  `msg` text NOT NULL,
  `read_status` int(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `s_id`, `r_id`, `msg`, `read_status`, `created_at`) VALUES
(1, 5, 4, 'hii', 1, '2018-11-25 19:24:45'),
(2, 5, 4, 'how ar you?', 1, '2018-11-25 19:25:45'),
(3, 5, 4, 'hello', 1, '2018-11-26 00:50:40'),
(4, 5, 4, 'h', 1, '2018-11-26 00:50:53'),
(5, 5, 4, 's', 1, '2018-11-26 00:50:54'),
(6, 5, 4, 'd', 1, '2018-11-26 00:50:55'),
(7, 5, 4, 'f', 1, '2018-11-26 00:50:56'),
(8, 5, 4, 'f', 1, '2018-11-26 00:50:56'),
(9, 5, 4, 'v', 1, '2018-11-26 00:50:57'),
(10, 5, 4, 'v', 1, '2018-11-26 00:50:58'),
(11, 5, 4, 'v', 1, '2018-11-26 00:50:58'),
(12, 5, 4, 'v', 1, '2018-11-26 00:50:59'),
(13, 5, 4, 's', 1, '2018-11-26 00:50:59'),
(14, 5, 4, 'fg', 1, '2018-11-26 00:51:00'),
(15, 5, 4, 'dd', 1, '2018-11-26 00:51:02'),
(19, 4, 60, 'hii bro', 1, '2018-11-27 21:29:11'),
(20, 4, 60, 'j', 1, '2018-11-27 21:45:56'),
(21, 4, 5, 'hello', 1, '2018-11-27 22:02:59'),
(22, 4, 5, 'how are you', 1, '2018-11-27 22:03:06'),
(23, 60, 4, 'hii dii', 1, '2018-11-27 22:24:28'),
(24, 4, 6, 'hii', 1, '2018-12-08 16:37:19'),
(25, 4, 6, 'how are you?', 1, '2018-12-08 16:37:26'),
(26, 52, 60, 'sdfs', 0, '2018-12-09 23:12:45'),
(27, 5, 4, 'hii', 1, '2018-12-13 12:17:15'),
(28, 6, 4, 'fsjdf', 0, '2018-12-13 14:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `clg_info`
--

CREATE TABLE `clg_info` (
  `id` int(11) NOT NULL,
  `clg_name` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `clg_logo` varchar(80) NOT NULL,
  `clg_contact_no` varchar(43) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` double(10,6) DEFAULT NULL,
  `lng` double(10,6) DEFAULT NULL,
  `about_clg` text NOT NULL,
  `principal` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clg_info`
--

INSERT INTO `clg_info` (`id`, `clg_name`, `email`, `clg_logo`, `clg_contact_no`, `address`, `lat`, `lng`, `about_clg`, `principal`, `website`, `status`) VALUES
(1, 'scet', 'scet@gmail.com', 'clg_logo.jpeg', '9876543210,9988775566', 'athvaline', 21.181796, 72.808966, '<p>Single-minded devotion of the pioneers of the society, inspired a series of selfless workers and public spirited men to volunteer to dedicated themselves to the service of Sarvajanik Education Society, which is now the largest philanthropic society in the whole of country.</p>\r\n\r\n<p>Established in 1912, and registered under Society&rsquo;s Registration Act XXI OF 1860, Sarvajanik Education Society imparts high quality education in multiple disciplines to 33,000 students in 33 institutions from pre primary to post graduate level.</p>\r\n\r\n<p>SES, managed by eminent citizens of surat from different cross sections of the society, is purely a philanthropic society sustained only through public donations.</p>\r\n\r\n<p>SES runs in a fully democratic manner, functioning in accordance with the guidelines set up unanimously by the founders, the government, university and the All India Council of technical Education (AICTE).</p>\r\n\r\n<p>The organizational structure of SES comprises of the Managing Committee and the Executive Committee. The latter looks after the Finance, Building, Academic, legal, Purchase, Project &amp; Administration matters as per the policies laid down by the Managing Committee.</p>\r\n', 'prof. jayatri kapadiya', 'http://www.scet.ac.in', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `replay_id` int(11) DEFAULT '0',
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `bid`, `uid`, `replay_id`, `comment`, `created_at`) VALUES
(4, 3, 4, 0, '<p>Any Questions Then Comment or massege me.&nbsp;</p>\r\n', '2018-12-13 20:51:48'),
(5, 3, 7, 0, '<p>i like it</p>\r\n', '2018-12-13 21:17:21'),
(6, 4, 7, 0, '<p>nice.. :)</p>\r\n', '2018-12-13 21:17:49'),
(7, 3, 4, 5, '<p>&nbsp;<strong>@venish</strong>&nbsp;thanks</p>\r\n', '2018-12-13 21:20:16');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_name`, `status`) VALUES
(1, 'MCA', 1),
(2, 'MBA', 1),
(4, 'MSC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `no_of_div` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `dept_no`, `year`, `no_of_div`, `status`) VALUES
(2, 1, 1, 3, 1),
(1, 1, 2, 2, 1),
(5, 2, 1, 2, 1),
(4, 4, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `election`
--

CREATE TABLE `election` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `dept_no` int(11) NOT NULL,
  `edate` datetime NOT NULL,
  `registration_end_date` datetime NOT NULL,
  `ads_end_date` datetime NOT NULL,
  `hours` int(11) NOT NULL DEFAULT '1',
  `level` varchar(20) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election`
--

INSERT INTO `election` (`id`, `name`, `description`, `dept_no`, `edate`, `registration_end_date`, `ads_end_date`, `hours`, `level`, `status`) VALUES
(17, 'MCA election', '<p>this is for mca department. all student can participate on this whose <strong>attendance is more </strong><strong>then</strong><strong> 70</strong><strong>%</strong> <strong>,</strong><strong> don&#39;t have any ATKT and previous year result is above 70</strong><strong>%</strong> .</p>\r\n', 1, '2018-12-30 15:56:00', '2018-12-14 11:56:00', '2018-12-14 13:56:00', 1, 'class', 1),
(18, 'MBA Election', '<p>this is for mca department. all student can participate on this whose <strong>attendance is more</strong> <strong>then</strong><strong> 70</strong><strong>% &nbsp;,</strong><strong> don&#39;t have any ATKT and previous year result is above 70</strong><strong>% .</strong></p>\r\n', 2, '2018-12-30 15:12:00', '2018-12-14 11:10:00', '2018-12-14 13:12:00', 1, 'class', 1),
(19, 'MSC Election', '<p>this is for mca department. all student can participate on this whose <strong>attendance is more </strong><strong>then</strong><strong> 70</strong><strong>% &nbsp;,</strong><strong> don&#39;t have any ATKT and previous year result is above 70</strong><strong>% .</strong></p>\r\n', 4, '2018-12-30 15:16:00', '2018-12-14 11:14:00', '2018-12-14 13:14:00', 1, 'class', 1),
(20, 'College Election', '<p>All candidate can apply whose win in class election.</p>\r\n', 0, '2018-12-30 16:17:00', '2018-12-14 12:15:00', '2018-12-14 14:15:00', 1, 'college', 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `tbl` varchar(30) NOT NULL,
  `tbl_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `tbl`, `tbl_id`, `uid`) VALUES
(55, 'candidate', 8, 52),
(56, 'candidate', 14, 52),
(57, 'candidate', 15, 52),
(58, 'candidate', 16, 52),
(59, 'candidate', 17, 52),
(62, 'candidate', 9, 4),
(65, 'candidate', 13, 75),
(66, 'candidate', 12, 75),
(67, 'candidate', 11, 75),
(68, 'candidate', 9, 52),
(72, 'candidate', 3, 53),
(73, 'candidate', 2, 53),
(75, 'candidate', 7, 4),
(76, 'candidate', 7, 5),
(78, 'blogs', 4, 4),
(80, 'blogs', 3, 4),
(81, 'candidate', 7, 7),
(82, 'blogs', 3, 7),
(83, 'blogs', 4, 7),
(84, 'candidate', 8, 4),
(85, 'blogs', 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `position_chart`
--

CREATE TABLE `position_chart` (
  `id` int(11) NOT NULL,
  `position_name` varchar(50) NOT NULL,
  `position_no` int(11) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position_chart`
--

INSERT INTO `position_chart` (`id`, `position_name`, `position_no`, `status`) VALUES
(1, 'student', 2, 1),
(2, 'Class LR', 5, 1),
(3, 'Class CR', 7, 1),
(4, 'College LR', 10, 1),
(5, 'College GS', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `image`, `status`) VALUES
(1, 'HEY! WE ARE POLITE', 'Election Day Is Coming\r\nUnited for Progress', 'def_31.jpg', 1),
(2, 'HEY! WE ARE POLITE', 'Your Vote for Progress\r\nWe Make History', 'def_21.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `rno` int(11) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(250) NOT NULL,
  `image` varchar(60) NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dept_no`, `year`, `division`, `semester`, `rno`, `mobile_no`, `email`, `password`, `dob`, `gender`, `address`, `image`, `position`, `status`) VALUES
(4, 'smita', 1, 2, 1, 3, 1, 9876543210, 'smita@gmail.com', 'SS123', '1998-01-27', 'female', 'uyuyguh ihh ih iuh', 'g5.jpg', 1, 1),
(7, 'venish', 1, 2, 1, 3, 3, 7894561232, 'venishsiyani@gmail.com', 'EJ6090', '1997-03-07', 'male', 'sdas as321d as1d 32s ', 'polish-words-v.jpg', 1, 1),
(53, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 2, 9427157100, 'abk123@gmail.com', 'HM2594', '0000-00-00', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(54, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 1, 4, 5, 9712473353, 'hamita97@gmail.com', 'LU3728', '0000-00-00', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(55, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 1, 4, 8, 7226057145, 'ranjan3544@gmail.com', 'OD8693', '0000-00-00', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(56, 'DESAI RUHI MUKESHKUMAR', 1, 2, 1, 4, 10, 9624059581, 'rushi@gmail.com', 'UT8684', '0000-00-00', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(57, 'PAWAR ROHAN RAJENDRA', 1, 2, 1, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', 'WC7688', '0000-00-00', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(58, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 1, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'TK6378', '0000-00-00', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(59, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 1, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'PI1675', '0000-00-00', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(60, 'VAGHELA JANAK KANUBHAI', 1, 2, 1, 4, 20, 9662727082, 'vaghelajanak97145@gmail.com', '123123', '0000-00-00', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '2.jpg', 1, 1),
(61, 'LAD HARDIK JAGDISHBHAI', 1, 2, 1, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'KZ3195', '0000-00-00', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(62, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 1, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'NE5631', '0000-00-00', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(63, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'CE6726', '0000-00-00', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(64, 'BHATT RUCHA JAYESHBHAI', 1, 2, 1, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'ZE3956', '0000-00-00', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(65, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 1, 4, 32, 8128603405, 'httgabk123@gmail.com', 'DC7851', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(66, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 1, 4, 34, 7046118204, 'mirhystal45@gmail.com', 'CL2992', '0000-00-00', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(67, 'GUPTA MANISH KRISHNA', 1, 2, 1, 4, 37, 7096869810, 'abk12457453@gmail.com', 'VV6882', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(68, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 1, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'TY3651', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(69, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 1, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'SH8283', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(70, 'ATLANI TARUNA MUKESH', 1, 2, 1, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'SX9134', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(71, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 1, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'AJ6572', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(5, 'gaurav', 1, 2, 2, 3, 2, 9966332255, 'gaurav.vanani@gmail.com', 'GV123', '1997-12-24', 'male', '25,jjndjnj jnfjdnfjndj ,f j ', 'crop.jpg', 1, 1),
(8, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 2, 9427157100, 'abk123@gmail.com', '123123', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(9, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 2, 4, 5, 9712473353, 'hamita97@gmail.com', '123123', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(10, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 2, 4, 8, 7226057145, 'ranjan3544@gmail.com', '123123', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(11, 'DESAI RUHI MUKESHKUMAR', 1, 2, 2, 4, 10, 9624059581, 'rushi@gmail.com', '123123', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(12, 'PAWAR ROHAN RAJENDRA', 1, 2, 2, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', '123123', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(13, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 2, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', '123123', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(14, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 2, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', '123123', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(15, 'VAGHELA JANAK KANUBHAI', 1, 2, 2, 4, 20, 9662727082, 'vaghelajanak97@gmail.com', '123123', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(16, 'LAD HARDIK JAGDISHBHAI', 1, 2, 2, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', '123123', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(17, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 2, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', '123123', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(18, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', '123123', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(19, 'BHATT RUCHA JAYESHBHAI', 1, 2, 2, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', '123123', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(92, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', 'BM7949', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(21, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 2, 4, 34, 7046118204, 'mirhystal45@gmail.com', '123123', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(93, 'GUPTA MANISH KRISHNA', 1, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', 'UF3846', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(94, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'CE3464', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(95, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'FV2625', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(96, 'ATLANI TARUNA MUKESH', 1, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'RW2730', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(97, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'BJ8861', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(98, 'JOSHI JAYKUMAR KETANBHAI', 1, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'VL4347', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(72, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 2, 9427157100, 'abk123@gmail.com', 'PI5128', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(73, 'BARAIYA HASMITA DANJIBHAI', 2, 1, 2, 2, 5, 9712473353, 'hamita97@gmail.com', 'CC5088', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(74, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 2, 1, 2, 2, 8, 7226057145, 'ranjan3544@gmail.com', 'OQ8146', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(75, 'DESAI RUHI MUKESHKUMAR', 2, 1, 2, 2, 10, 9624059581, 'rushi@gmail.com', 'VR9179', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(76, 'PAWAR ROHAN RAJENDRA', 2, 1, 2, 2, 13, 9824914133, 'ranjahsvn34@gmail.com', 'CN4783', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(77, 'PATEL DRASHTIBEN RASIKBHAI', 2, 1, 2, 2, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'BP9024', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(78, 'PRAJAPATI NEHA HARKISHAN', 2, 1, 2, 2, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'KY4966', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(79, 'VAGHELA JANAK KANUBHAI', 2, 1, 2, 2, 20, 9662727082, 'rushvhzsvbvzi@gmail.com', 'KW4951', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(80, 'LAD HARDIK JAGDISHBHAI', 2, 1, 2, 2, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'XV1485', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(81, 'PATEL AYUSH VIJAYKUMAR', 2, 1, 2, 2, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'FV2613', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(82, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'JU2168', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(83, 'BHATT RUCHA JAYESHBHAI', 2, 1, 2, 2, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'WZ9302', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(84, 'PARAJAPATI DARSHANA BHARATKUMAR', 2, 1, 2, 2, 32, 8128603405, 'httgabk123@gmail.com', 'YY3381', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(85, 'PIPALIYA MIRAL KISHORBHAI', 2, 1, 2, 2, 34, 7046118204, 'mirhystal45@gmail.com', 'ZF5616', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(86, 'GUPTA MANISH KRISHNA', 2, 1, 2, 2, 37, 7096869810, 'abk12457453@gmail.com', 'HK3500', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(87, 'PATEL SUNNYKUMAR ARUNBHAI', 2, 1, 2, 2, 39, 7405859953, 'ruchggha441221@gmail.com', 'QD4079', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(88, 'AMIPARA NIRALIBEN RAMESHBHAI', 2, 1, 2, 2, 42, 8238165078, 'httgabk1234544@gmail.com', 'PF2719', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(89, 'ATLANI TARUNA MUKESH', 2, 1, 2, 2, 44, 7567699564, 'mirhystal42145@gmail.com', 'FF2277', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(90, 'GOLWALA MONIKA PRAKASHCHANDRA', 2, 1, 2, 2, 47, 9724373235, 'kanjothgghxi@gmail.com', 'EX6165', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(91, 'JOSHI JAYKUMAR KETANBHAI', 2, 1, 2, 2, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'MS9670', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(6, 'nirti', 4, 2, 2, 4, 1, 9856321478, 'nirtipatel@gmail.com', 'NP123', '1998-12-28', 'female', 'asdsadd sd,sdaa f,rrrt,yr', 'the_letter_n_by_nvn5719-d46l9ld.jpg', 1, 1),
(20, 'PARAJAPATI DARSHANA BHARATKUMAR', 4, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', '123123', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(22, 'GUPTA MANISH KRISHNA', 4, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', '123123', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(48, 'PATEL SUNNYKUMAR ARUNBHAI', 4, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'LA2110', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(49, 'AMIPARA NIRALIBEN RAMESHBHAI', 4, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'WK5961', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(50, 'ATLANI TARUNA MUKESH', 4, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'JD6080', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(51, 'GOLWALA MONIKA PRAKASHCHANDRA', 4, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'JW2041', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(52, 'JOSHI JAYKUMAR KETANBHAI', 4, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'YR7914', '0000-00-00', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '4971432001.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stud_id` (`stud_id`),
  ADD KEY `proposer_id` (`proposer_id`),
  ADD KEY `supporter_id` (`supporter_id`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `s_id` (`s_id`),
  ADD KEY `r_id` (`r_id`);

--
-- Indexes for table `clg_info`
--
ALTER TABLE `clg_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bid` (`bid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`dept_no`,`year`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `election`
--
ALTER TABLE `election`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dept_no` (`dept_no`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`tbl`,`tbl_id`,`uid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `unique_id` (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `position_chart`
--
ALTER TABLE `position_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`dept_no`,`year`,`division`,`semester`,`rno`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`,`cid`,`sid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `sid` (`sid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `clg_info`
--
ALTER TABLE `clg_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `election`
--
ALTER TABLE `election`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `position_chart`
--
ALTER TABLE `position_chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `candidate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `candidate`
--
ALTER TABLE `candidate`
  ADD CONSTRAINT `candidate_ibfk_1` FOREIGN KEY (`stud_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_2` FOREIGN KEY (`proposer_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_3` FOREIGN KEY (`supporter_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `candidate_ibfk_4` FOREIGN KEY (`eid`) REFERENCES `election` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`s_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`r_id`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`bid`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `division`
--
ALTER TABLE `division`
  ADD CONSTRAINT `division_ibfk_1` FOREIGN KEY (`dept_no`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vote`
--
ALTER TABLE `vote`
  ADD CONSTRAINT `vote_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `election` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vote_ibfk_2` FOREIGN KEY (`cid`) REFERENCES `candidate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vote_ibfk_3` FOREIGN KEY (`sid`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
