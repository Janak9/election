-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 03, 2018 at 07:41 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.12-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `election`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(8) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `type`, `status`) VALUES
(1, 'janak v', 'vaghelajanak97145@gmail.com', '123123', '2.jpg', 0, 1),
(2, 'sudeep', 'sudeepyadav5@gmail.com', '123123', 'p2.jpg', 0, 1),
(3, 'dhaval', 'variyadhaval758@gmail.com', '123123', 'p3.jpg', 0, 1),
(4, 'smita', 'smitapatel9537@gmail.com', 'IB8225', 'IMG-20160229-WA00172.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `cid`, `title`, `description`, `created_at`, `image`, `status`) VALUES
(1, 2, 'welcome', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal. Dramatically cultivate viral paradigms after team building e-services. Energistically network worldwide systems after highly efficient innovation.Dramatically leverage other&rsquo;s cross-unit expertise vis-a-vis cross-platform ideas. Uniquely disintermediate cross-media synergy without fully researched internal or &ldquo;organic&rdquo; sources. Monotonectally empower competitive innovation after alternative core competencies. Professionally optimize turnkey deliverables after enterprise bandwidth. Assertively foster inexpensive &ldquo;outside the box&rdquo; thinking rather than intermandated niche markets.</p>\r\n\r\n<p>Interactively implement intermandated architectures without vertical methods of empowerment. Enthusiastically streamline multidisciplinary e-business after frictionless schemas. Phosfluorescently fabricate intuitive architectures through empowered manufactured products. Professionally repurpose dynamic products whereas team driven process improvements. Authoritatively grow leading-edge process improvements vis-a-vis cooperative internal or &ldquo;organic&rdquo; sources.</p>\r\n', '2018-11-23 19:04:53', 'def_41.jpg', 1),
(2, 2, 'sdfds.s l l', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>\r\n', '2018-11-23 22:44:31', '6.jpg', 1),
(3, 3, 'hello', '<p>I am specialise in building a website for small and large business. Whatever purpose you want include in your site then i am here to setup your all needs. And not only website i can also develop a software for any business solution.</p>\r\n\r\n<p>My Awesome Skills :- HTML, CSS , JavaScript , PHP , AngularJs, React.js, Python, Django, Java, Mysqli, SQL, jQuery, Codeigniter , Working with API , Project Management.</p>\r\n\r\n<p>All works will completed by me.So your Work will not be outsourced to someone else. I would like to make the project as per the requirement of the client, and i will never complete a project until client is 100% satisfied.</p>\r\n', '2018-11-25 17:12:47', '3.jpg', 1),
(4, 4, 'welcome blog', '<p>asdnk, c&nbsp; &nbsp;sa clakm ll klaskl lk samclk m llaslnllal lalmcacma almac, slmvk</p>\r\n', '2018-12-01 23:09:39', '9.jpg', 1),
(5, 5, 'hello guy\'s', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal. Dramatically cultivate viral paradigms after team building e-services. Energistically network worldwide systems after highly efficient innovation.Dramatically leverage other&rsquo;s cross-unit expertise vis-a-vis cross-platform ideas. Uniquely disintermediate cross-media synergy without fully researched internal or &ldquo;organic&rdquo; sources. Monotonectally empower competitive innovation after alternative core competencies. Professionally optimize turnkey deliverables after enterprise bandwidth. Assertively foster inexpensive &ldquo;outside the box&rdquo; thinking rather than intermandated niche markets.</p>\r\n\r\n<p>Interactively implement intermandated architectures without vertical methods of empowerment. Enthusiastically streamline multidisciplinary e-business after frictionless schemas. Phosfluorescently fabricate intuitive architectures through empowered manufactured products. Professionally repurpose dynamic products whereas team driven process improvements. Authoritatively grow leading-edge process improvements vis-a-vis cooperative internal or &ldquo;organic&rdquo; sources.</p>\r\n', '2018-12-02 00:00:26', '23.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `id` int(11) NOT NULL,
  `stud_id` int(11) NOT NULL,
  `proposer_id` int(11) NOT NULL,
  `supporter_id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `position` int(11) NOT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  `g_link` varchar(255) DEFAULT NULL,
  `t_link` varchar(255) DEFAULT NULL,
  `i_link` varchar(255) DEFAULT NULL,
  `about` text,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`id`, `stud_id`, `proposer_id`, `supporter_id`, `eid`, `image`, `position`, `fb_link`, `g_link`, `t_link`, `i_link`, `about`, `status`) VALUES
(2, 4, 7, 5, 3, '12936504-alphabet-letter-made-from-water-and-bubble-letter-s1.jpg', 4, 'https://www.facebook.com/janak.vaghela.97', 'https://plus.google.com/u/0/108013413762526383953', 'https://twitter.com/vaghelajanak9?lang=en', 'https://www.instagram.com/janak__vaghela/', '<p>lmadklma llka l clk kl k askv kk laskl lsl lklk llk lk ls jv k kj k&nbsp; ak&nbsp; ak l lkas&nbsp;<br />\r\n<br />\r\nlsakncj as ka&nbsp; lkl&nbsp; ll j&nbsp; &nbsp; &nbsp; &nbsp;slmlsknjksdnlccslm<br />\r\nasldllsmadkm;</p>\r\n\r\n<p>asdmlm<br />\r\n<br />\r\n&nbsp;</p>\r\n', 1),
(3, 5, 60, 7, 3, 'WhatsApp_Image_2018-09-28_at_10_17_11_PM_(1).jpeg', 5, 'https://www.facebook.com/gaurav.vanani.9?__tn__=%2CdlC-R-R&eid=ARCx_laOKgvACg9tDuiTvhTtPRbuSm8njt_Kff5BGXKpfbaZ3TbOfD12deqOZK7hiVbf33wiRkHCgEHP&hc_ref=ARR2U8emtlFezfBuKH0kD0EJNuPGmyIp2WdtkKKiryVFtpV2pcnlBXpb-L7Sv5G8Tn0', 'https://plus.google.com/u/0/+GauravVanani', 'https://twitter.com/GVanani?lang=en', 'https://www.instagram.com/gauravvanani/', '<p>I am specialise in building a website for small and large business. Whatever purpose you want include in your site then i am here to setup your all needs. And not only website i can also develop a software for any business solution.</p>\r\n\r\n<p>My Awesome Skills :- HTML, CSS , JavaScript , PHP , AngularJs, React.js, Python, Django, Java, Mysqli, SQL, jQuery, Codeigniter , Working with API , Project Management.</p>\r\n\r\n<p>All works will completed by me.So your Work will not be outsourced to someone else. I would like to make the project as per the requirement of the client, and i will never complete a project until client is 100% satisfied.</p>\r\n', 1),
(4, 6, 22, 51, 3, 'Screenshot_from_2018-12-01_21-20-35.png', 4, NULL, NULL, NULL, NULL, NULL, 1),
(5, 60, 55, 71, 1, '52.jpg', 3, 'https://www.facebook.com/janak.vaghela.97', 'https://plus.google.com/u/0/108013413762526383953', 'https://twitter.com/vaghelajanak9', 'https://www.instagram.com/janak__vaghela/?hl=en', '<p>A web and software developer with 1+ years experience in back-end development using Python/Django, PHP/Codeigniter, PHP/Laravel and front-end development using ReactJS/Redux, HTML5, CSS3, Bootstrap.<br />\r\n<br />\r\nMy aim is to facilitate clients achieve their business goals through web design and development solutions. Based on your business needs and branding goals, I will work with you to create a plan for a unique and exclusive technological solution.<br />\r\n<br />\r\nThe major factor behind my success is that I will not just focus on delivering a Web Application or Website but I will deliver a project that works for your business.&nbsp;<br />\r\n<br />\r\nFor&nbsp;more about me click this link&nbsp;<a href=\"https://janakvaghela.site123.me/\" target=\"_blank\">janakVaghela</a></p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `r_id` int(11) NOT NULL,
  `msg` text NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `s_id`, `r_id`, `msg`, `read_status`, `created_at`) VALUES
(1, 5, 4, 'hii', 1, '2018-11-25 19:24:45'),
(2, 5, 4, 'how ar you?', 1, '2018-11-25 19:25:45'),
(3, 5, 4, 'hello', 1, '2018-11-26 00:50:40'),
(4, 5, 4, 'h', 1, '2018-11-26 00:50:53'),
(5, 5, 4, 's', 1, '2018-11-26 00:50:54'),
(6, 5, 4, 'd', 1, '2018-11-26 00:50:55'),
(7, 5, 4, 'f', 1, '2018-11-26 00:50:56'),
(8, 5, 4, 'f', 1, '2018-11-26 00:50:56'),
(9, 5, 4, 'v', 1, '2018-11-26 00:50:57'),
(10, 5, 4, 'v', 1, '2018-11-26 00:50:58'),
(11, 5, 4, 'v', 1, '2018-11-26 00:50:58'),
(12, 5, 4, 'v', 1, '2018-11-26 00:50:59'),
(13, 5, 4, 's', 1, '2018-11-26 00:50:59'),
(14, 5, 4, 'fg', 1, '2018-11-26 00:51:00'),
(15, 5, 4, 'dd', 1, '2018-11-26 00:51:02'),
(19, 4, 60, 'hii bro', 1, '2018-11-27 21:29:11'),
(20, 4, 60, 'j', 1, '2018-11-27 21:45:56'),
(21, 4, 5, 'hello', 0, '2018-11-27 22:02:59'),
(22, 4, 5, 'how are you', 0, '2018-11-27 22:03:06'),
(23, 60, 4, 'hii dii', 1, '2018-11-27 22:24:28');

-- --------------------------------------------------------

--
-- Table structure for table `clg_info`
--

CREATE TABLE `clg_info` (
  `id` int(11) NOT NULL,
  `clg_name` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `clg_logo` varchar(80) NOT NULL,
  `clg_contact_no` varchar(43) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` double(10,6) DEFAULT NULL,
  `lng` double(10,6) DEFAULT NULL,
  `about_clg` text NOT NULL,
  `principal` varchar(50) NOT NULL,
  `website` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clg_info`
--

INSERT INTO `clg_info` (`id`, `clg_name`, `email`, `clg_logo`, `clg_contact_no`, `address`, `lat`, `lng`, `about_clg`, `principal`, `website`, `status`) VALUES
(1, 'scet', 'scet@gmail.com', 'clg_logo.jpeg', '9876543210,9988775566', 'athvaline', 21.181796, 72.808966, '<p>Single-minded devotion of the pioneers of the society, inspired a series of selfless workers and public spirited men to volunteer to dedicated themselves to the service of Sarvajanik Education Society, which is now the largest philanthropic society in the whole of country.</p>\r\n\r\n<p>Established in 1912, and registered under Society&rsquo;s Registration Act XXI OF 1860, Sarvajanik Education Society imparts high quality education in multiple disciplines to 33,000 students in 33 institutions from pre primary to post graduate level.</p>\r\n\r\n<p>SES, managed by eminent citizens of surat from different cross sections of the society, is purely a philanthropic society sustained only through public donations.</p>\r\n\r\n<p>SES runs in a fully democratic manner, functioning in accordance with the guidelines set up unanimously by the founders, the government, university and the All India Council of technical Education (AICTE).</p>\r\n\r\n<p>The organizational structure of SES comprises of the Managing Committee and the Executive Committee. The latter looks after the Finance, Building, Academic, legal, Purchase, Project &amp; Administration matters as per the policies laid down by the Managing Committee.</p>\r\n', 'prof. jayatri kapadiya', 'http://www.scet.ac.in', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `replay_id` int(11) DEFAULT '0',
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `bid`, `uid`, `replay_id`, `comment`, `created_at`) VALUES
(1, 2, 4, 0, '<p>Ne erat velit invidunt his. Eum in dicta veniam interesset, harum fuisset te nam, ea cum lupta definitionem. Vocibus suscipit prodesset vim ei, equidem perpetua eu per.</p>\r\n', '2018-11-24 19:13:29'),
(2, 2, 4, 1, '<p>fsdfd&nbsp;<strong>@smita</strong>&nbsp;sdsdfsdfs,bdkldsm</p>\r\n', '2018-11-24 23:25:07'),
(3, 2, 5, 0, '<p>nice blog... :)</p>\r\n', '2018-11-25 16:46:09'),
(4, 1, 5, 0, '<p>awsome ..&nbsp;;)</p>\r\n', '2018-11-25 16:46:48'),
(5, 3, 4, 0, '<p>dfgfdgfd</p>\r\n', '2018-11-29 08:52:25'),
(6, 4, 6, 0, '<p>hello guy&#39;s</p>\r\n', '2018-12-01 23:15:47'),
(7, 2, 60, 0, '<p>nice one... :)</p>\r\n', '2018-12-02 00:14:31'),
(8, 2, 60, 1, '<p>&nbsp;<strong>@smita</strong>&nbsp;yaa right</p>\r\n', '2018-12-02 00:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `dept_name`, `status`) VALUES
(1, 'MCA', 1),
(2, 'MBA', 1),
(4, 'MSC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `no_of_div` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `dept_no`, `year`, `no_of_div`, `status`) VALUES
(2, 1, 1, 3, 1),
(1, 1, 2, 2, 1),
(5, 2, 1, 2, 1),
(4, 4, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `election`
--

CREATE TABLE `election` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `dept_no` int(11) NOT NULL,
  `edate` datetime NOT NULL,
  `registration_end_date` datetime NOT NULL,
  `ads_end_date` datetime NOT NULL,
  `hours` int(11) NOT NULL DEFAULT '1',
  `level` varchar(20) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election`
--

INSERT INTO `election` (`id`, `name`, `description`, `dept_no`, `edate`, `registration_end_date`, `ads_end_date`, `hours`, `level`, `status`) VALUES
(1, 'safs', '<p>However, he noted ISIS terrorists in Syria still have &quot;the ability to appeal to troubled souls&quot; online and said the number of open</p>\r\n', 1, '2018-12-01 00:01:00', '2018-11-25 23:48:00', '2018-11-27 23:48:00', 1, 'class', 1),
(2, 'dffgdfsdsavsd', '<p>smdfn kf ke ff ke few fk kje fjk kfd lkdlk d</p>\r\n', 4, '2018-12-02 22:25:00', '2018-11-07 23:48:00', '2018-11-13 23:48:00', 1, 'class', 1),
(3, 'college election', '<p>l&nbsp; L L CL&nbsp; LKLNADMOPPOMCSVSSC&nbsp; PDPOMCSC MM;MCC MLKCMC; M LKCLM ;LM</p>\r\n', 0, '2018-12-03 22:25:00', '2018-11-25 10:01:00', '2018-11-28 11:50:00', 1, 'college', 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `tbl` varchar(30) NOT NULL,
  `tbl_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `tbl`, `tbl_id`, `uid`) VALUES
(8, 'blogs', 1, 4),
(21, 'blogs', 2, 4),
(22, 'blogs', 2, 5),
(23, 'blogs', 1, 5),
(26, 'blogs', 3, 5),
(27, 'candidate', 3, 5),
(38, 'candidate', 2, 5),
(39, 'candidate', 2, 4),
(40, 'candidate', 3, 4),
(41, 'candidate', 4, 6),
(42, 'blogs', 4, 6),
(43, 'candidate', 5, 60),
(44, 'blogs', 5, 60),
(45, 'blogs', 3, 60),
(46, 'blogs', 2, 60),
(47, 'blogs', 1, 60),
(48, 'candidate', 3, 60),
(49, 'candidate', 2, 60),
(50, 'candidate', 5, 4),
(51, 'candidate', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `position_chart`
--

CREATE TABLE `position_chart` (
  `id` int(11) NOT NULL,
  `position_name` varchar(50) NOT NULL,
  `position_no` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position_chart`
--

INSERT INTO `position_chart` (`id`, `position_name`, `position_no`, `status`) VALUES
(1, 'student', 2, 1),
(2, 'Class LR', 5, 1),
(3, 'Class CR', 7, 1),
(4, 'College LR', 10, 1),
(5, 'College GS', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `description`, `image`, `status`) VALUES
(1, 'HEY! WE ARE POLITE', 'Election Day Is Coming\r\nUnited for Progress', 'def_31.jpg', 1),
(2, 'HEY! WE ARE POLITE', 'Your Vote for Progress\r\nWe Make History', 'def_21.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dept_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `rno` int(11) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(250) NOT NULL,
  `image` varchar(60) NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dept_no`, `year`, `division`, `semester`, `rno`, `mobile_no`, `email`, `password`, `dob`, `gender`, `address`, `image`, `position`, `status`) VALUES
(4, 'smita', 1, 2, 1, 3, 1, 9876543210, 'smita@gmail.com', 'SS123', '1998-01-27', 'female', 'uyuyguh ihh ih iuh', 'g5.jpg', 4, 1),
(5, 'gaurav', 1, 2, 1, 3, 2, 9966332255, 'gaurav.vanani@gmail.com', 'GV123', '1997-12-24', 'male', '25,jjndjnj jnfjdnfjndj ,f j ', 'crop.jpg', 5, 1),
(7, 'venish', 1, 2, 1, 3, 3, 7894561232, 'venishsiyani@gmail.com', 'EJ6090', '1997-03-07', 'male', 'sdas as321d as1d 32s ', 'polish-words-v.jpg', 1, 1),
(53, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 2, 9427157100, 'abk123@gmail.com', 'HM2594', '0000-00-00', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(54, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 1, 4, 5, 9712473353, 'hamita97@gmail.com', 'LU3728', '0000-00-00', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(55, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 1, 4, 8, 7226057145, 'ranjan3544@gmail.com', 'OD8693', '0000-00-00', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(56, 'DESAI RUHI MUKESHKUMAR', 1, 2, 1, 4, 10, 9624059581, 'rushi@gmail.com', 'UT8684', '0000-00-00', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(57, 'PAWAR ROHAN RAJENDRA', 1, 2, 1, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', 'WC7688', '0000-00-00', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(58, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 1, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'TK6378', '0000-00-00', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(59, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 1, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'PI1675', '0000-00-00', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(60, 'VAGHELA JANAK KANUBHAI', 1, 2, 1, 4, 20, 9662727082, 'vaghelajanak97145@gmail.com', '123123', '0000-00-00', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '2.jpg', 1, 1),
(61, 'LAD HARDIK JAGDISHBHAI', 1, 2, 1, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'KZ3195', '0000-00-00', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(62, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 1, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'NE5631', '0000-00-00', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(63, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 1, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'CE6726', '0000-00-00', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(64, 'BHATT RUCHA JAYESHBHAI', 1, 2, 1, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'ZE3956', '0000-00-00', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(65, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 1, 4, 32, 8128603405, 'httgabk123@gmail.com', 'DC7851', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(66, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 1, 4, 34, 7046118204, 'mirhystal45@gmail.com', 'CL2992', '0000-00-00', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(67, 'GUPTA MANISH KRISHNA', 1, 2, 1, 4, 37, 7096869810, 'abk12457453@gmail.com', 'VV6882', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(68, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 1, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'TY3651', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(69, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 1, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'SH8283', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(70, 'ATLANI TARUNA MUKESH', 1, 2, 1, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'SX9134', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(71, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 1, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'AJ6572', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(8, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 2, 9427157100, 'abk123@gmail.com', '123123', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(9, 'BARAIYA HASMITA DANJIBHAI', 1, 2, 2, 4, 5, 9712473353, 'hamita97@gmail.com', '123123', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(10, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 1, 2, 2, 4, 8, 7226057145, 'ranjan3544@gmail.com', '123123', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(11, 'DESAI RUHI MUKESHKUMAR', 1, 2, 2, 4, 10, 9624059581, 'rushi@gmail.com', '123123', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(12, 'PAWAR ROHAN RAJENDRA', 1, 2, 2, 4, 13, 9824914133, 'ranjahsvn34@gmail.com', '123123', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(13, 'PATEL DRASHTIBEN RASIKBHAI', 1, 2, 2, 4, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', '123123', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(14, 'PRAJAPATI NEHA HARKISHAN', 1, 2, 2, 4, 18, 9712154400, 'ranjakjhn3544@gmail.com', '123123', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(15, 'VAGHELA JANAK KANUBHAI', 1, 2, 2, 4, 20, 9662727082, 'rushvhzsvbvzi@gmail.com', '123123', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(16, 'LAD HARDIK JAGDISHBHAI', 1, 2, 2, 4, 22, 9824862187, 'ranjahsvn34444@gmail.com', '123123', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(17, 'PATEL AYUSH VIJAYKUMAR', 1, 2, 2, 4, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', '123123', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(18, 'MOHATA ABHISHEK KARNIDAN', 1, 2, 2, 4, 27, 7878885379, 'ranjakjhn35144444@gmail.com', '123123', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(19, 'BHATT RUCHA JAYESHBHAI', 1, 2, 2, 4, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', '123123', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(92, 'PARAJAPATI DARSHANA BHARATKUMAR', 1, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', 'BM7949', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(21, 'PIPALIYA MIRAL KISHORBHAI', 1, 2, 2, 4, 34, 7046118204, 'mirhystal45@gmail.com', '123123', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(93, 'GUPTA MANISH KRISHNA', 1, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', 'UF3846', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(94, 'PATEL SUNNYKUMAR ARUNBHAI', 1, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'CE3464', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(95, 'AMIPARA NIRALIBEN RAMESHBHAI', 1, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'FV2625', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(96, 'ATLANI TARUNA MUKESH', 1, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'RW2730', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(97, 'GOLWALA MONIKA PRAKASHCHANDRA', 1, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'BJ8861', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(98, 'JOSHI JAYKUMAR KETANBHAI', 1, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'VL4347', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(72, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 2, 9427157100, 'abk123@gmail.com', 'PI5128', '1996-10-25', 'male', '44-45, AMBIKA VIJAY SOC., VARACHHA ROAD, SURAT', '', 1, 1),
(73, 'BARAIYA HASMITA DANJIBHAI', 2, 1, 2, 2, 5, 9712473353, 'hamita97@gmail.com', 'CC5088', '1996-02-25', 'female', 'A-701,SHALEEN RESIDENCY,OPP.BHARTI RESIDENCY,NEAR RAJ CORNER,PAL,SURAT-394510.', '', 1, 1),
(74, 'CHAUDHARI SEJALKUMARI KISHORBHAI', 2, 1, 2, 2, 8, 7226057145, 'ranjan3544@gmail.com', 'OQ8146', '1996-10-25', 'female', '156, SHERI NO-2, KRUSHNAKUNJ SOC, PALANPUR PATIYA, RANDER ROAD, SURAT', '', 1, 1),
(75, 'DESAI RUHI MUKESHKUMAR', 2, 1, 2, 2, 10, 9624059581, 'rushi@gmail.com', 'VR9179', '1996-02-25', 'female', 'C-1, SAHYOG APPT, NEAR TIRUMALA COMPLEX, B/H SNEH SANKUL WADI, ANAND MAHAL ROAD, SURAT', '', 1, 1),
(76, 'PAWAR ROHAN RAJENDRA', 2, 1, 2, 2, 13, 9824914133, 'ranjahsvn34@gmail.com', 'CN4783', '1996-10-25', 'male', '2/3976-77, FLAT NO-302, AMBER PALACE, CHOGAN SHERI, SAGRAMPURA, SURAT', '', 1, 1),
(77, 'PATEL DRASHTIBEN RASIKBHAI', 2, 1, 2, 2, 15, 8690695969, 'hamhgsfvxvbita97@gmail.com', 'BP9024', '1996-02-25', 'female', '26, MANGALDHAM APT., BHATAR TENAMENT, ALTHAN BHATAR ROAD, SURAT', '', 1, 1),
(78, 'PRAJAPATI NEHA HARKISHAN', 2, 1, 2, 2, 18, 9712154400, 'ranjakjhn3544@gmail.com', 'KY4966', '1996-10-25', 'female', '2/91, SAMRAT BAND, SADAK FADIYA, A.K. ROAD, FULPADA, SURAT', '', 1, 1),
(79, 'VAGHELA JANAK KANUBHAI', 2, 1, 2, 2, 20, 9662727082, 'rushvhzsvbvzi@gmail.com', 'KW4951', '1996-02-25', 'male', 'B-5, MAHUVA SUGAR COLONY, BAMANIYA SITE, MAHUVA, BARDOLI', '', 1, 1),
(80, 'LAD HARDIK JAGDISHBHAI', 2, 1, 2, 2, 22, 9824862187, 'ranjahsvn34444@gmail.com', 'XV1485', '1996-10-25', 'male', '2/2868, MALIWAD, SAGRAMPURA, SURAT', '', 1, 1),
(81, 'PATEL AYUSH VIJAYKUMAR', 2, 1, 2, 2, 24, 9104847682, 'hamhgsfvxvbita92227@gmail.com', 'FV2613', '1996-02-25', 'male', 'A-403, SURYAM SKY, B/H RAJ WORLD SHOPPING COMPLEX, PALANPUR GAM ROAD, SURAT', '', 1, 1),
(82, 'MOHATA ABHISHEK KARNIDAN', 2, 1, 2, 2, 27, 7878885379, 'ranjakjhn35144444@gmail.com', 'JU2168', '1996-10-25', 'male', '80, TIRUPATI RESIDENCY,NEAR SANJIVNI HOSPITAL, KADODARA, DIST. SURAT-394305', '', 1, 1),
(83, 'BHATT RUCHA JAYESHBHAI', 2, 1, 2, 2, 29, 7984240699, 'rushvhzsvbvzi124@gmail.com', 'WZ9302', '1996-02-25', 'female', '7/881, RAMPURA NOORI MOHALLAH, SURAT-395003', '', 1, 1),
(84, 'PARAJAPATI DARSHANA BHARATKUMAR', 2, 1, 2, 2, 32, 8128603405, 'httgabk123@gmail.com', 'YY3381', '1996-10-25', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(85, 'PIPALIYA MIRAL KISHORBHAI', 2, 1, 2, 2, 34, 7046118204, 'mirhystal45@gmail.com', 'ZF5616', '1996-02-25', 'female', '38, MANINAGAR SOCIETY, B/H NARAYAN NAGAR, KATARGAM, SURAT-395004', '', 1, 1),
(86, 'GUPTA MANISH KRISHNA', 2, 1, 2, 2, 37, 7096869810, 'abk12457453@gmail.com', 'HK3500', '1996-10-25', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(87, 'PATEL SUNNYKUMAR ARUNBHAI', 2, 1, 2, 2, 39, 7405859953, 'ruchggha441221@gmail.com', 'QD4079', '1996-02-25', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(88, 'AMIPARA NIRALIBEN RAMESHBHAI', 2, 1, 2, 2, 42, 8238165078, 'httgabk1234544@gmail.com', 'PF2719', '1996-10-25', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(89, 'ATLANI TARUNA MUKESH', 2, 1, 2, 2, 44, 7567699564, 'mirhystal42145@gmail.com', 'FF2277', '1996-02-25', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(90, 'GOLWALA MONIKA PRAKASHCHANDRA', 2, 1, 2, 2, 47, 9724373235, 'kanjothgghxi@gmail.com', 'EX6165', '1996-10-25', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(91, 'JOSHI JAYKUMAR KETANBHAI', 2, 1, 2, 2, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'MS9670', '1996-02-25', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1),
(6, 'nirti', 4, 2, 2, 4, 1, 9856321478, 'nirtipatel@gmail.com', 'NP123', '1998-12-28', 'female', 'asdsadd sd,sdaa f,rrrt,yr', 'the_letter_n_by_nvn5719-d46l9ld.jpg', 2, 1),
(20, 'PARAJAPATI DARSHANA BHARATKUMAR', 4, 2, 2, 4, 32, 8128603405, 'httgabk123@gmail.com', '123123', '0000-00-00', 'female', 'B-1 204 SAI TIRTH RESIDENCY, NEAR PALANPUR GAM ROAD, SURAT-395009', '', 1, 1),
(22, 'GUPTA MANISH KRISHNA', 4, 2, 2, 4, 37, 7096869810, 'abk12457453@gmail.com', '123123', '0000-00-00', 'male', 'CH 1/37, PRAM COLONY , SUGAR FACTORY, SAYAN, SURAT-394130', '', 1, 1),
(48, 'PATEL SUNNYKUMAR ARUNBHAI', 4, 2, 2, 4, 39, 7405859953, 'ruchggha441221@gmail.com', 'LA2110', '0000-00-00', 'male', 'B-1/301, CHARBHUJA ARCADE, NEW KOSAD ROAD, AMROLI, SURAT-394107', '', 1, 1),
(49, 'AMIPARA NIRALIBEN RAMESHBHAI', 4, 2, 2, 4, 42, 8238165078, 'httgabk1234544@gmail.com', 'WK5961', '0000-00-00', 'female', '6/102, RIVERA TOWER, NR. SWEAMINARAYAN TEMPLE, ADAJAN, SURAT-395009', '', 1, 1),
(50, 'ATLANI TARUNA MUKESH', 4, 2, 2, 4, 44, 7567699564, 'mirhystal42145@gmail.com', 'JD6080', '0000-00-00', 'female', '80, VALLBHA NAGAR SOCIETY, OPP. BARODA PRISTAGE, VARACHHA ROAD, SURAT-395006', '', 1, 1),
(51, 'GOLWALA MONIKA PRAKASHCHANDRA', 4, 2, 2, 4, 47, 9724373235, 'kanjothgghxi@gmail.com', 'JW2041', '0000-00-00', 'female', '203, TRILOK SOCIETY, OPP, AKHAND ANAND COLLEGE, VED ROAD, SURAT-395004', '', 1, 1),
(52, 'JOSHI JAYKUMAR KETANBHAI', 4, 2, 2, 4, 49, 8490968863, 'ranjakjhn3544@gmail.com', 'YR7914', '0000-00-00', 'male', 'A/474, SITARAM SOCIETY, AAI MATA ROAD, SURAT-395009', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE `vote` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vote`
--

INSERT INTO `vote` (`id`, `eid`, `cid`, `sid`, `date`) VALUES
(3, 1, 3, 4, '2018-12-01 00:43:27'),
(4, 1, 2, 4, '2018-12-01 00:46:10'),
(5, 1, 2, 60, '2018-12-02 15:16:08'),
(6, 2, 4, 55, '2018-12-02 15:31:28'),
(7, 3, 3, 4, '2018-12-03 01:27:25'),
(8, 3, 2, 4, '2018-12-03 01:27:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clg_info`
--
ALTER TABLE `clg_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`dept_no`,`year`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `election`
--
ALTER TABLE `election`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`tbl`,`tbl_id`,`uid`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `unique_id` (`id`);

--
-- Indexes for table `position_chart`
--
ALTER TABLE `position_chart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`dept_no`,`year`,`division`,`semester`,`rno`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `clg_info`
--
ALTER TABLE `clg_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `election`
--
ALTER TABLE `election`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `position_chart`
--
ALTER TABLE `position_chart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
